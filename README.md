# Scala Study Group

Using [Essential Scala](https://gumroad.com/l/essential-scala#)
Some notes [Rignisms](https://jrigney.gitlab.io/rignisms/)

## Practice

### Sum type
1. create a sum type
1. implement fold on the sum type
1. implement map on the sum type
1. implement flatMap on the sum type

### Product type
1. create a product type
1. implement fold on the product type
1. implement map on the product type
1. implement flatMap on the product type

### Linked List
1. create a linked list type
1. implement fold on the linked list type
1. implement map on the linked list type

### Tree
1. create a tree type
1. implement fold using inorder traversal
1. implement fold using postorder traversal
1. implement map

##Graphvis
dot -Tpdf graph1.dot -o graph1.pdf
