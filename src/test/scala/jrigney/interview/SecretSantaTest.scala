package jrigney.interview

import org.scalatest.{ Matchers, WordSpec }

/**
 * Programming Challenge: Secret Santa
 *
 * There are many many different ways to solve this problem. What we are
 * lookng for is simply good code and good architecture. It's up to you
 * to decide what that is.
 *
 * We want your solution to be simple enough to complete in a few hours
 * (it doesn't necessarily have to be "perfect" if there is such a
 * thing). Find a good medium, write the best code you can. If you don't
 * have time to finish send what you have; just explain what you planned
 * on doing.
 *
 * If you get done and decide you could have done something better, feel
 * free to add commentary on what you would have done differently if you
 * were to do it again. Any commentary is welcomed (it will help us
 * understand your thought process).
 *
 * ## Part one:
 *
 * Imagine that every year your extended family does a "Secret Santa"
 * gift exchange. For this gift exchange, each person draws another
 * person at random and then gets a gift for them. Write a program that
 * will choose a Secret Santa for everyone given a list of all the
 * members of your extended family. Obviously, a person cannot be their
 * own Secret Santa.
 *
 * ## Part two:
 *
 * After the third year of having the Secret Santa gift exchange, you've
 * heard complaints of having the same Secret Santa year after year.
 * Modify your program so that a family member can only have the same
 * Secret Santa once every 3 years.
 *
 * ## Part three:
 * *
 * As your extended family has grown, members have gotten married and/or
 * had children. Families usually get gifts for members of their
 * immediate family, so it doesn't make a lot of sense for anyone to be a
 * Secret Santa for a member of their immediate family (spouse, parents,
 * or children). Modify your program to take this constraint into
 * consideration when choosing Secret Santas.
 */
class SecretSantaTest extends WordSpec with Matchers {

  import SecretSantaTest._
  //  val family5 = Vector("Jere", "Dominga", "Yvone", "Michell", "Sam")
  //  val expected5 = Vector(NoRecipient("Jere"), Paired("Yvone","Dominga"), Paired("Michell","Yvone"), Paired("Jere","Michell"), Paired("Dominga","Sam"))

  val jere = Player("Jere", List.empty)
  val dominga = Player("Dominga", List.empty)
  val michell = Player("Michell", List.empty)
  val yvonne = Player("Yvone", List.empty)
  val sam = Player("Sam", List.empty)

  val family4 = Vector(jere, dominga, yvonne, michell)
  val expected4 = Vector(Paired(michell, yvonne), Paired(yvonne, michell), Paired(dominga, jere), Paired(jere, dominga))

  val family5 = Vector(jere, dominga, yvonne, michell, sam)
  val expected5 = Vector(NoRecipient(sam), Paired(michell, yvonne), Paired(yvonne, michell), Paired(dominga, jere), Paired(jere, dominga))

  "Secret Santa" can {
    "family of 4" should {
      "part1 returns matches between people" in {
        SecretSanta.pair(family4, family4, SecretSanta.part1Pairing _) shouldBe expected4
      }
      "part2 returns matches between people" in {
        val map = scala.collection.mutable.HashMap[String, Player](
          (jere.name -> jere),
          (dominga.name -> dominga),
          (michell.name -> michell),
          (yvonne.name -> yvonne))

        val expectedYear1 = Vector(
          Paired(addPastRecipient(michell, yvonne, map), yvonne),
          Paired(addPastRecipient(yvonne, michell, map), michell),
          Paired(addPastRecipient(dominga, jere, map), jere),
          Paired(addPastRecipient(jere, dominga, map), dominga))

        val year1Result = SecretSanta.pair(family4, family4, SecretSanta.part2Pairing _)
        year1Result shouldBe expectedYear1

        val expectedYear2: Vector[SecretPair] = Vector(
          Paired(addPastRecipient(michell, jere, map), jere),
          Paired(addPastRecipient(yvonne, dominga, map), dominga),
          Paired(addPastRecipient(dominga, yvonne, map), yvonne),
          Paired(addPastRecipient(jere, michell, map), michell))

        //        println(s" 2 ######################### ")
        val year2Family = nextFamily(year1Result)
        val year2Result = SecretSanta.pair(year2Family, family4, SecretSanta.part2Pairing _)
        year2Result.sorted shouldBe expectedYear2.sorted

        val expectedYear3: Vector[SecretPair] = Vector(
          Paired(addPastRecipient(michell, dominga, map), dominga),
          Paired(addPastRecipient(yvonne, jere, map), jere),
          Paired(addPastRecipient(dominga, michell, map), michell),
          Paired(addPastRecipient(jere, yvonne, map), yvonne))

        //        println(s" 3 ######################### ")
        val year3Family = nextFamily(year2Result)
        val year3Result = SecretSanta.pair(year3Family, family4, SecretSanta.part2Pairing _)
        year3Result.sorted shouldBe expectedYear3.sorted

      }
    }
    "family of 5" should {
      "part1 returns one extra" in {
        SecretSanta.pair(family5, family5, SecretSanta.part1Pairing _) shouldBe expected5
      }
    }
  }
}

object SecretSantaTest {
  def addPastRecipient(santa: Player, recipient: Player, players: scala.collection.mutable.HashMap[String, Player]): Player = {
    val player = players(santa.name)
    val updatedRecipients = recipient +: player.pastRecipients
    val updatePlayer = player.copy(pastRecipients = updatedRecipients)
    players.update(santa.name, updatePlayer)
    updatePlayer
  }

  def nextFamily(result: Vector[SecretPair]): Vector[Player] = {
    result.flatMap {
      case Paired(santa, _) => Some(santa)
      case NoRecipient(_) => None
    }
  }

}
