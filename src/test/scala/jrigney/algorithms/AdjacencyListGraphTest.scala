package jrigney.algorithms

import org.scalatest.{ Matchers, WordSpec }

import scala.collection.immutable.HashMap

class AdjacencyListGraphTest extends WordSpec with Matchers {

  "BFS for an UnweightedAdjacencyGraph using a adjacency list" should {
    "for graph1 return the vertices from the start outward" in {
      val expected = List[Int](0, 1, 2, 3)
      UnweightedAdjacencyListGraph.breadthFirstSearch(0, AdjacencyListGraphTest.hashmapGraph1) should equal(expected)
    }

    "for graph2 and starting at 0 return the vertices from the start outward" in {
      val expected = List[Int](0, 2, 3, 4, 6, 7)
      UnweightedAdjacencyListGraph.breadthFirstSearch(0, AdjacencyListGraphTest.hashmapGraph2) should equal(expected)
    }

    "for graph2 and starting at 1 return the vertices from the start outward" in {
      val expected = List[Int](1, 6)
      UnweightedAdjacencyListGraph.breadthFirstSearch(1, AdjacencyListGraphTest.hashmapGraph2) should equal(expected)
    }

    "for graph2 and starting at 1 return only the vertices where x % 2 == 0" in {
      val expected = List(0, 2, 4, 6)
      val fn: Int => Boolean = x => x % 2 == 0
      UnweightedAdjacencyListGraph.bfsMod2(0, AdjacencyListGraphTest.hashmapGraph2, fn) should equal(expected)
    }
  }

  "BFS & DFS for an UnweightedGraph using an adjacency matrix" should {

    "for graph1 and starting at 0 return the vertices from the start outward" in {
      val expected = List(0, 1, 2, 3)
      UnweightedAdjacencyListGraph.breadthFirstSearch(0, AdjacencyListGraphTest.vectorVectorGraph1) should equal(expected)
    }

    "for graph3 and starting at 0 return the vertices from the start outward" in {
      val expected = List(0, 1, 5, 2, 3, 6, 4)
      UnweightedAdjacencyListGraph.breadthFirstSearch(0, AdjacencyListGraphTest.vectorVectorGraph3) should equal(expected)
    }

    "for graph1 and starting a 0 return the vertices downward" in {
      val expected = List(0, 1, 2, 3, 3, 2)
      UnweightedAdjacencyListGraph.depthFirstSearch(0, AdjacencyListGraphTest.vectorVectorGraph1) should equal(expected)
    }

    "for graph3 and starting a 0 return the vertices downward" in {
      val expected = List(0, 1, 2, 3, 4, 5, 6)
      UnweightedAdjacencyListGraph.depthFirstSearch(0, AdjacencyListGraphTest.vectorVectorGraph3) should equal(expected)
    }
  }

  "DFS for an UnweightedAdjacencyGraph using a adjacency list" should {

    "for graph1 and starting a 2 return the vertices from the start downward" in {
      val expected = List[Int](2, 0, 1, 3)
      UnweightedAdjacencyListGraph.depthFirstSearch(2, AdjacencyListGraphTest.hashmapGraph1) should equal(expected)
    }

    "for graph2 and starting a 6 return the vertices from the start downward" in {
      val expected = List[Int](6)
      UnweightedAdjacencyListGraph.depthFirstSearch(6, AdjacencyListGraphTest.hashmapGraph2) should equal(expected)
    }

    "for graph2 and starting a 1 return the vertices from the start downward" in {
      val expected = List[Int](1, 6)
      UnweightedAdjacencyListGraph.depthFirstSearch(1, AdjacencyListGraphTest.hashmapGraph2) should equal(expected)
    }

    "for graph2 and starting a 0 return the vertices from the start downward" in {
      val expected = List[Int](0, 2, 4, 6, 7, 3)
      UnweightedAdjacencyListGraph.depthFirstSearch(0, AdjacencyListGraphTest.hashmapGraph2) should equal(expected)
    }

    "for graph2 and starting a 0 return only the vetices where x % 2 = 0" in {
      val expected = List[Int](0, 2, 4, 6)
      val fn: Int => Boolean = x => x % 2 == 0
      UnweightedAdjacencyListGraph.dfsFiltered(0, AdjacencyListGraphTest.hashmapGraph2, fn) should equal(expected)
    }

    "for graph3 and starting a 1 return only the vetices where x % 2 = 0" in {
      val expected = List[Int](2, 6, 4)
      val fn: Int => Boolean = x => x % 2 == 0
      UnweightedAdjacencyListGraph.dfsFiltered(1, AdjacencyListGraphTest.hashmapGraph3, fn) should equal(expected)
    }
  }
}

object AdjacencyListGraphTest {
  /**
   * digraph graph1 {
   * 0 -> 1 -> 2 -> 3;
   * 0 -> 2;
   * 2 -> 0;
   * 3 -> 3;
   * }
   */
  val hashmapGraph1: UnweightedAdjacencyListGraph.HashMapGraph[Int] = HashMap[Int, Vector[Int]](
    (0 -> Vector[Int](1, 2)),
    (1 -> Vector[Int](2)),
    (2 -> Vector[Int](0, 3)),
    (3 -> Vector[Int](3)))

  val vectorVectorGraph1: UnweightedAdjacencyListGraph.AdjacencyMatrixGraph[Int] = Vector(
    Vector(0, 1, 1, 0),
    Vector(0, 0, 1, 0),
    Vector(0, 0, 0, 1),
    Vector(0, 0, 0, 1))

  /**
   * digraph graph2 {
   * 0 -> 2 -> 4 -> 7
   * 0 -> 3
   * 1 -> 6
   * 4 -> 6
   * }
   */

  val hashmapGraph2: UnweightedAdjacencyListGraph.HashMapGraph[Int] = HashMap[Int, Vector[Int]](
    (0 -> Vector[Int](2, 3)),
    (1 -> Vector[Int](6)),
    (2 -> Vector[Int](4)),
    (3 -> Vector.empty[Int]),
    (4 -> Vector[Int](6, 7)),
    (5 -> Vector[Int](1)),
    (6 -> Vector.empty[Int]),
    (7 -> Vector.empty[Int]))
  val vectorVectorGraph2: UnweightedAdjacencyListGraph.AdjacencyMatrixGraph[Int] = Vector(
    Vector(2, 3),
    Vector(6),
    Vector(2),
    Vector.empty,
    Vector(6, 7),
    Vector(1),
    Vector.empty,
    Vector.empty)

  val hashmapGraph3: UnweightedAdjacencyListGraph.HashMapGraph[Int] = HashMap[Int, Vector[Int]](
    (1 -> Vector[Int](2, 6)),
    (2 -> Vector[Int](1)),
    (3 -> Vector[Int](1, 5)),
    (4 -> Vector[Int](7)),
    (5 -> Vector[Int](1, 6)),
    (6 -> Vector[Int](2, 4, 5)),
    (7 -> Vector[Int](6)))

  val vectorVectorGraph3: UnweightedAdjacencyListGraph.AdjacencyMatrixGraph[Int] = Vector(
    Vector(0, 1, 0, 0, 0, 1, 0), //0
    Vector(0, 0, 1, 1, 0, 0, 0), //1
    Vector(0, 0, 0, 0, 0, 0, 0), //2
    Vector(0, 0, 0, 0, 1, 0, 0), //3
    Vector(0, 0, 0, 0, 0, 0, 0), //4
    Vector(0, 0, 0, 0, 0, 0, 1), //5
    Vector(0, 0, 0, 0, 0, 0, 0) //6
  )
}
