package jrigney.algorithms

import jrigney.algorithms.Graph.{ WeightedAdjacentVertex, WeightedEdge, WeightedGraph }
import org.scalatest.{ Matchers, WordSpec }

import scala.collection.immutable.HashMap

class GraphFactoryTest extends WordSpec with Matchers {

  val graph = WeightedGraph[Char, Double, WeightedEdge[Char, Double]](
    vertices = ('a' to 'f').toSet,
    edges = Vector(
      WeightedEdge('a', 'b', 7.0),
      WeightedEdge('a', 'c', 9.0),
      WeightedEdge('a', 'f', 14.0),
      WeightedEdge('b', 'c', 10.0),
      WeightedEdge('b', 'd', 15.0),
      WeightedEdge('c', 'd', 11.0),
      WeightedEdge('c', 'f', 2.0),
      WeightedEdge('d', 'e', 6.0),
      WeightedEdge('e', 'f', 9.0)))

  "A GraphFactory" should {
    "build an undirected weighted graph" in {
      val expected = HashMap(
        'a' -> List(WeightedAdjacentVertex('b', 7.0), WeightedAdjacentVertex('c', 9.0), WeightedAdjacentVertex('f', 14.0)),
        'b' -> List(WeightedAdjacentVertex('a', 7.0), WeightedAdjacentVertex('c', 10.0), WeightedAdjacentVertex('d', 15.0)),
        'c' -> List(WeightedAdjacentVertex('a', 9.0), WeightedAdjacentVertex('b', 10.0), WeightedAdjacentVertex('d', 11.0), WeightedAdjacentVertex('f', 2.0)),
        'd' -> List(WeightedAdjacentVertex('b', 15.0), WeightedAdjacentVertex('c', 11.0), WeightedAdjacentVertex('e', 6.0)),
        'e' -> List(WeightedAdjacentVertex('d', 6.0), WeightedAdjacentVertex('f', 9.0)),
        'f' -> List(WeightedAdjacentVertex('a', 14.0), WeightedAdjacentVertex('c', 2.0), WeightedAdjacentVertex('e', 9.0)))
      GraphFactory.buildWeightedAdjacencyList(graph) should equal(expected)
    }
  }
}
