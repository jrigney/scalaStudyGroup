package jrigney.algorithms

import jrigney.algorithms.Graph.{ WeightedAdjacencyList, WeightedAdjacentVertex }
import org.scalatest.{ FunSuite, Matchers }

import scala.collection.immutable.HashMap

class GraphTest extends FunSuite with Matchers {

  import Graph._

  val weightedGraphAlpha = WeightedGraph[Char, Double, WeightedEdge[Char, Double]](
    vertices = ('a' to 'f').toSet,
    edges = Vector(
      WeightedEdge('a', 'b', 7.0),
      WeightedEdge('a', 'c', 9.0),
      WeightedEdge('a', 'f', 14.0),
      WeightedEdge('b', 'c', 10.0),
      WeightedEdge('b', 'd', 15.0),
      WeightedEdge('c', 'd', 11.0),
      WeightedEdge('c', 'f', 2.0),
      WeightedEdge('d', 'e', 6.0),
      WeightedEdge('e', 'f', 9.0)))
  //      1
  //  /   |   \
  //  2   3   4
  //  /   \     |
  //  5   6     7
  //  |       / | \
  //  8      9 10 11

  val weightedGraphCharlie = WeightedGraph[Int, Double, WeightedEdge[Int, Double]](
    vertices = (1 to 11).toSet,
    edges = Vector(
      WeightedEdge(1, 2, 1.0),
      WeightedEdge(1, 3, 1.0),
      WeightedEdge(1, 4, 1.0),
      WeightedEdge(2, 5, 1.0),
      WeightedEdge(3, 6, 1.0),
      WeightedEdge(4, 7, 1.0),
      WeightedEdge(5, 8, 1.0),
      WeightedEdge(7, 9, 1.0),
      WeightedEdge(7, 10, 1.0),
      WeightedEdge(7, 11, 1.0)))

  val undirectedCycles = HashMap(
    "a" -> List(("b", 7.0), ("c", 9.0), ("f", 14.0)),
    "b" -> List(("c", 10.0), ("d", 15.0)),
    "c" -> List(("d", 11.0), ("f", 2.0)),
    "d" -> List(("e", 6.0)),
    "e" -> List(("f", 9.0)),
    "f" -> Nil)
  val weightedGraphBravo = WeightedGraph[Int, Double, WeightedEdge[Int, Double]](
    vertices = (1 to 8).toSet,
    edges = Vector(
      WeightedEdge(1, 4, 6.0),
      WeightedEdge(1, 5, 6.0),
      WeightedEdge(2, 4, 6.0),
      WeightedEdge(4, 6, 6.0),
      WeightedEdge(4, 8, 6.0),
      WeightedEdge(5, 7, 6.0),
      WeightedEdge(6, 8, 6.0),
      WeightedEdge(7, 8, 6.0)))

  test("shortest path dijkstra connected directed graph destination connected vertex") {
    val adjacencyList = GraphFactory.buildWeightedAdjacencyList(weightedGraphAlpha)

    val actual = shortestPath[Char, Double]('a', 'e', adjacencyList)
    assert(Path(20.0, List('a', 'c', 'f', 'e')) == actual)
  }

  test("shortest path dijkstra undirected graph destination unconnected vertex") {

    val adjacencyList = GraphFactory.buildWeightedAdjacencyList(weightedGraphBravo)
    val actual = shortestPath[Int, Double](1, 3, adjacencyList)
    val expected = Path(0.0, Nil)
    assert(actual == expected)
  }

  test("shortest path tree dijkstra connected directed graph destination connected vertex") {

    val start = List(("a", 0.0))
    val actual = minimumCosts[String](start, undirectedCycles, Map(("a" -> 0)))
    assert(Map("e" -> 26.0, "f" -> 11.0, "a" -> 0.0, "b" -> 7.0, "c" -> 9.0, "d" -> 20.0) == actual)
  }

  test("shortest paths") {

    val adjacencyList = GraphFactory.buildWeightedAdjacencyList(weightedGraphAlpha)

    val actual = shortestPaths[Char, Double]('a', 'e', adjacencyList)
    val expected = Map(
      'e' -> Path(20.0, List('e', 'f', 'c', 'a')),
      'f' -> Path(11.0, List('f', 'c', 'a')),
      'a' -> Path(0.0, List('a')),
      'b' -> Path(7.0, List('b', 'a')),
      'c' -> Path(9.0, List('c', 'a')),
      'd' -> Path(20.0, List('d', 'c', 'a')))

    actual should equal(expected)
  }

  test("dfs") {

    val adjacencyList = GraphFactory.buildWeightedAdjacencyList(weightedGraphCharlie)

    import Scratch._
    val actual = dfsRecursiveMutable(adjacencyList, 1)
  }
}

object Scratch {

  sealed trait VectorState
  final case object Undiscovered extends VectorState
  final case object Discovered extends VectorState
  final case object Processed extends VectorState

  def dfsRecursiveMutable(G: WeightedAdjacencyList[Int, Double], u: Int) = {

    import scala.collection.mutable
    var time = 0
    val state = mutable.IndexedSeq.fill(12)(Undiscovered: VectorState)
    val entry = mutable.IndexedSeq.fill(12)(0)
    val processed = mutable.IndexedSeq.fill(12)(0)
    val exit = mutable.IndexedSeq.fill(12)(0)

    def dfsRecursiveMutableLoop(G: WeightedAdjacencyList[Int, Double], u: Int): Unit = {
      //      println(s"$u:u ${time}:time")
      state.update(u, Discovered)
      //process vertex here
      entry.update(u, time)
      time = time + 1
      val adjList = G(u)
      adjList.foreach {
        case WeightedAdjacentVertex(v, w) =>
          //process edge(u,v) here
          if (state(v) == Undiscovered) {
            processed.update(v, u)
            dfsRecursiveMutableLoop(G, v)
          }
      }
      state.update(u, Processed)
      time = time + 1
      exit.update(u, time)
    }

    dfsRecursiveMutableLoop(G, u)
    println(s"${exit.zipWithIndex}:exit") /*REMOVEME*/
    println(s"${entry.zipWithIndex}:entry") /*REMOVEME*/
  }

  def dfsRecursiveImmutable(
    G: WeightedAdjacencyList[Int, Double],
    u: Int,
    time: Int,
    state: Map[Int, VectorState],
    entry: Map[Int, Int],
    exit: Map[Int, Int],
    processed: Map[Int, Int]) = {

    ???
  }

}
