package jrigney.problems

import org.scalatest.FunSuite
import scala.annotation.tailrec

/*
Practice matrix to graph conversion
 */
class NineteenOneTest extends FunSuite {

  import GraphBuilders._

  import Graph._

  val startVertex = Vertex(2, 0)
  val exitVertex = Vertex(0, 2)

  test("path") {
    val theMaze = GraphBuilders.buildGraph(maze)

  }
  //the given maze where true are open areas and false are walls
  val maze: Array[Array[Boolean]] = Array(
    Array(false, true, true),
    Array(false, true, false),
    Array(true, true, false))

  test("convert matrix to graph") {
    val expectedVertices = Set(Vertex(0, 1), Vertex(0, 2), Vertex(1, 1), Vertex(2, 0), Vertex(2, 1))
    val expectedEdges = Set(
      (Vertex(0, 1), Vertex(0, 2)),
      (Vertex(2, 1), Vertex(2, 0)),
      (Vertex(0, 1), Vertex(1, 1)),
      (Vertex(1, 1), Vertex(2, 1)),
      (Vertex(0, 2), Vertex(0, 1)),
      (Vertex(1, 1), Vertex(0, 1)),
      (Vertex(2, 0), Vertex(2, 1)),
      (Vertex(2, 1), Vertex(1, 1)))
    val actualGraph = buildGraph(maze)
    val actualVertices = actualGraph.map(_.vertex)
    val actualEdges = actualGraph.flatMap { adjacencyList =>
      val graphVertex = adjacencyList.vertex
      adjacencyList.edges.map { adjacentVertex => (graphVertex, adjacentVertex) }
    }

    assert(actualVertices.sameElements(expectedVertices))
    assert(actualEdges.sameElements(expectedEdges))
  }
  //  println(s"graph ${GraphBuilders.buildGraph(maze)}") /*REMOVEME*/

  test("breadth first traversal") {
    val actual = breadthFirstSearch(Vertex(2, 0), buildGraph(maze))
    val expected = List(
      List(Vertex(2, 0)),
      List(Vertex(2, 1)),
      List(Vertex(1, 1)),
      List(Vertex(0, 1)),
      List(Vertex(0, 2)))
    assert(actual.sameElements(expected))
  }

  //  println(s"#### bfs ${breadthFirstSearch(Vertex(2, 0), buildGraph(maze))}") /*REMOVEME*/

  test("spanning tree") {
    val actual = Graph.spanningTree(Vertex(2, 0), buildGraph(maze))
    val expected = List(
      WeightedVertex(0, 2, 4, Vertex(0, 1)),
      WeightedVertex(0, 1, 3, Vertex(1, 1)),
      WeightedVertex(1, 1, 2, Vertex(2, 1)),
      WeightedVertex(2, 1, 1, Vertex(2, 0)),
      WeightedVertex(2, 0, 0, Vertex(2, 0)))
    assert(actual.sameElements(expected))
  }
  //  println(s"#### spanning tree \n${Graph.spanningTree(Vertex(2, 0), buildGraph(maze)).mkString("\n")}") /*REMOVEME*/

  test("path find") {
    val actual = Graph.pathFind(Vertex(2, 0), Vertex(0, 2), buildGraph(maze))
    val expected = Some(List(
      WeightedVertex(2, 0, 0, Vertex(2, 0)),
      WeightedVertex(2, 1, 1, Vertex(2, 0)),
      WeightedVertex(1, 1, 2, Vertex(2, 1)),
      WeightedVertex(0, 1, 3, Vertex(1, 1)),
      WeightedVertex(0, 2, 4, Vertex(0, 1))))
    assert(actual.sameElements(expected))
  }
  //  println(s"#### path \n${Graph.pathFind(Vertex(2, 0), Vertex(0, 2), buildGraph(maze))}") /*REMOVEME*/
}

object MazeTest {

}

object Graph {
  type Graph = Set[AdjacencyList]

  def breadthFirstSearch(start: Vertex, g: Graph) = {
    val visited = List(start)
    val result = List(List(start))

    @tailrec
    def BFS0(start: List[Vertex], accumulator: List[List[Vertex]], visited: List[Vertex]): List[List[Vertex]] = {
      val potentialNeighbors = start.flatMap { v =>
        g.find(_.vertex == v).map(_.edges)
      }.flatten
      val newNeighbors = potentialNeighbors.filterNot(visited.contains).distinct
      if (newNeighbors.isEmpty) accumulator
      else BFS0(newNeighbors, newNeighbors :: accumulator, visited ++ newNeighbors)
    }

    BFS0(List(start), result, visited).reverse
  }

  def spanningTree(start: Vertex, g: Graph): List[WeightedVertex] = {
    @tailrec
    def spanningTreeLoop(nodesToLookAt: List[WeightedVertex], currentCost: Int, visited: List[Vertex], acc: List[WeightedVertex]): List[WeightedVertex] = {
      val potentalNeighbors = nodesToLookAt.flatMap { parentVertex =>
        val p = Vertex(parentVertex.row, parentVertex.col)
        g.find(_.vertex == p).map { adjacencyList => adjacencyList.edges.map { v => WeightedVertex(v.row, v.col, currentCost + 1, p) } }
      }.flatten
      val newNeighbors = potentalNeighbors.filterNot { v => visited.contains(Vertex(v.row, v.col)) }.distinct
      if (newNeighbors.isEmpty) nodesToLookAt ++ acc
      else {
        spanningTreeLoop(newNeighbors, currentCost + 1, visited ++ newNeighbors.map { v => Vertex(v.row, v.col) }, nodesToLookAt ++ acc)
      }
    }

    val weightedStart = WeightedVertex(start.row, start.col, 0, start)
    spanningTreeLoop(List(weightedStart), 0, List(start), List.empty[WeightedVertex])
  }

  def pathFind(start: Vertex, end: Vertex, graph: Graph): Option[List[WeightedVertex]] = {
    val spanningTree = Graph.spanningTree(start, graph)

    @tailrec
    def loop(currentVertex: WeightedVertex, end: Vertex, spanningTree: List[WeightedVertex], acc: List[WeightedVertex]): Option[List[WeightedVertex]] = {
      //      println(s"##### currentVertex ${currentVertex} end ${end} acc ${acc}") /*REMOVEME*/
      val foundEnd = currentVertex.row == end.row && currentVertex.col == end.col
      val parentRow = currentVertex.fromVertex.row
      val parentCol = currentVertex.fromVertex.col
      val nextVertex = spanningTree.find(v => v.col == parentCol && v.row == parentRow)
      (foundEnd, nextVertex) match {
        case (true, _) => Some(acc)
        case (false, None) => None
        case (false, Some(vertex)) => loop(vertex, end, spanningTree, vertex +: acc)
      }
    }
    spanningTree.find(v => v.col == end.col && v.row == end.row).flatMap(s => loop(s, start, spanningTree, List(s)))
  }
}

case class WeightedVertex(row: Int, col: Int, cost: Int, fromVertex: Vertex)

object GraphBuilders {

  def getNeighborCoordinates(row: Int, col: Int, rowUpperBoundary: Int, colUpperBoundary: Int): Set[Vertex] = {
    val north = if (col - 1 >= 0) Some(Vertex(row, col - 1)) else None
    val south = if (col + 1 <= colUpperBoundary) Some(Vertex(row, col + 1)) else None
    val east = if (row + 1 <= rowUpperBoundary) Some(Vertex(row + 1, col)) else None
    val west = if (row - 1 >= 0) Some(Vertex(row - 1, col)) else None

    Set(north, south, east, west).collect {
      case Some(n) => n
    }
  }

  def buildGraph(maze: Array[Array[Boolean]]): Set[AdjacencyList] = {
    val rowUpperBoundary = maze.length - 1
    val colUpperBoundary = maze(0).length - 1

    val vertices = for {
      rowI <- 0 until maze.length
      colI <- 0 until maze(0).length
    } yield {
      val vertexId = s"$rowI:$colI"
      if (maze(rowI)(colI) == true) {
        val adjacentVertices = getNeighborCoordinates(rowI, colI, rowUpperBoundary, colUpperBoundary) flatMap { coordinates =>
          if (maze(coordinates.row)(coordinates.col) == true) Some(Vertex(coordinates.row, coordinates.col))
          else None
        }

        Some(AdjacencyList(Vertex(rowI, colI), adjacentVertices))
      } else None
    }

    vertices.collect {
      case Some(a) => a
    }.toSet

  }

}

case class AdjacencyList(vertex: Vertex, edges: Set[Vertex])

case class Vertex(row: Int, col: Int)

//http://codereview.stackexchange.com/questions/29699/bfs-and-dfs-in-scala
//http://cs.stackexchange.com/questions/298/graph-searching-breadth-first-vs-depth-first
//bfs is easy to parallelize

object DevelopmentTest extends App {

  case class Vertex(id: String, adjacentVertices: Set[String])

  case class AdjacentCoordinates(row: Int, col: Int)

}
