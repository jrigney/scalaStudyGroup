package jrigney.problems

/**
 * Write a program that takes as input a set of words and returns groups of anagrams for those words.
 */

import org.scalatest.FunSuite
class ThirteenOne extends FunSuite {

  import Anagram._

  test("anagram") {
    val words = List("debitcard", "elvis", "silent", "badcredit", "lives", "freedom", "listen", "levis")
    val s1 = List("debitcard", "badcredit")
    val s2 = List("elvis", "lives", "levis")
    val s3 = List("silent", "listen")
    val s4 = List("freedom")

    val expected = List(s1, s2, s3, s4)
    val actual = anagram(words)
    assert(actual.size.toInt == expected.size.toInt)
    actual.map { x =>
      assert(true == expected.contains(x))
    }
    ()
    //assert(actual == expected)
  }
}

object Anagram {

  def anagram(words: List[String]): List[List[String]] = {
    val keyed = words.map { w =>
      (w.sorted, w)
    }
    val grouped = keyed.groupBy(x => x._1)
    grouped.map(_._2.map { _._2 }).toList
  }

}
