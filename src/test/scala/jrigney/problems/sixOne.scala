package jrigney.problems

//6.1 Write a program that takes an array A of length n and an index i into A,
//and rearranges the elements such that all elements less than A[i] appear first,
//followed by elements equal to A[i], followed by elements greater than A[i]

import org.scalatest.FunSuite
class SixOneTest extends FunSuite {

  import MyArray._
  test("partition array") {
    val unsorted = Seq(3, 2, 1)
    val expected = Seq(1, 2, 3)
    val pivotIndex = 1
    val actual = partitionArray(unsorted, pivotIndex)
    assert(actual == expected)
  }
}

object MyArray {

  def partitionArray(unsorted: Seq[Int], pivotIndex: Int): Seq[Int] = {

    @scala.annotation.tailrec
    def loop(pivotValue: Int, remaining: Seq[Int], below: Seq[Int], equal: Seq[Int], above: Seq[Int]): Seq[Int] = {
      remaining match {
        case Nil => below ++ equal ++ above
        case v if (v.head < pivotValue) => loop(pivotValue, remaining.tail, v.head +: below, equal, above)
        case v if (v.head == pivotValue) => loop(pivotValue, remaining.tail, below, v.head +: equal, above)
        case v if (v.head > pivotValue) => loop(pivotValue, remaining.tail, below, equal, v.head +: above)
      }
    }

    loop(unsorted(pivotIndex), unsorted, Seq.empty[Int], Seq.empty[Int], Seq.empty[Int])
  }
}

