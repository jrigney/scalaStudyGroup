package jrigney.problems

import org.scalatest.FunSuite
/**
 * edified -> deified
 */
class ThirteenTwo extends FunSuite {

  import Palindrome._

  test("palindrome") {
    val input = List(("edified", true), ("aba", true), ("aaabaaa", true),
      ("aabb", true), ("ab", false), ("aa", true))

    input.foreach {
      case (in, expected) =>
        assert(palindrome_?(in) == expected)
    }
  }
}

object Palindrome {

  def palindrome_?(input: String): Boolean = {
    input.size match {
      case 1 => true
      case 2 if (input(0) == input(1)) => true
      case 2 => false
      case _ =>
        val stringAsList = input.toList.groupBy(x => x)
        val rme = stringAsList.map { case (k, v) => v.size }.toList.sorted.filter(n => n % 2 != 0)
        //        println(s"##### $rme")
        ((input.size % 2 == 0), (rme.size % 2 == 0)) match {
          case (false, true) => false
          case (false, false) => true
          case (true, false) => false
          case (true, true) => true
        }
    }
  }
}
