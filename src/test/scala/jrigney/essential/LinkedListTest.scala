package jrigney.essential

import org.scalatest.WordSpec

class LinkedListSpec extends WordSpec {
  "A LinkedList" when {
    "subtyped as an End" should {
      val unit = End(): LinkedList[Int]
      "return End for map" in {
        val actual = unit.map(a => a.toString)
        assert(actual == End())
      }
      "return the End value for fold" in {
        val fn = (a: Int, b: String) => s"$b WRONG one"
        val actual = unit.fold("")(fn)
        assert(actual == "")
      }
    }
  }
}
