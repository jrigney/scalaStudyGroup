package jrigney.essential.game

import jrigney.essential.game.Application.ActResult
import jrigney.essential.game.Direction.{ North, South }
import org.scalatest.{ FunSuite, Matchers }

class GameTest extends FunSuite with Matchers {

  val initialLocation = Location(0, 0)
  val initialInventory = List.empty[Item]
  val initialPlayerState = PlayerState(initialLocation, initialInventory)
  val cell1 = Cell(List(Sword()), "An empty room")
  val cell2 = Cell(List.empty[Item], "Just like the other rooms")
  val worldMap = WorldMap(Vector(Vector(cell1, cell2)))
  val initialGameState = GameState(initialPlayerState, worldMap)

  test("parse") {
    Application.parse("q") shouldEqual Right(Quit)
  }

  test("act") {
    Application.act(Quit, initialGameState) shouldEqual ActResult(List("quitter"), None)
  }

  test("cellAt of a negative location x") {
    val validLocation = Location(-1, 1)
    worldMap.cellAt(validLocation) shouldEqual None
  }
  test("cellAt of a location x greater than size of vector") {
    val validLocation = Location(0, 1000)
    worldMap.cellAt(validLocation) shouldEqual None
  }
  test("cellAt of a negative location y") {
    val validLocation = Location(0, -1)
    worldMap.cellAt(validLocation) shouldEqual None
  }
  test("cellAt of a location y greater than size of vector") {
    val validLocation = Location(0, 1000)
    worldMap.cellAt(validLocation) shouldEqual None
  }

  test("cellAt of a valid cell") {
    val validLocation = Location(0, 1)
    worldMap.cellAt(validLocation) shouldEqual Some(cell2)
  }

  test("look around") {
    Application.lookAround(initialGameState) shouldEqual ActResult(List("Desc: An empty room Items:  An ordinary metallic sword"), Some(initialGameState))
  }

  test("move North is invalid move") {
    Application.move(initialGameState, North()) shouldEqual ActResult(List("Invalid North"), Some(initialGameState))
  }

  test("move South is valid move") {
    val expectedPlayerState = initialPlayerState.copy(loc = Location(0, 1))
    Application.move(initialGameState, South()) shouldEqual ActResult(List("Just like the other rooms"), Some(initialGameState.copy(expectedPlayerState)))
  }
  test("move South then North is valid move") {
    val southMove = Application.move(initialGameState, South())
    val _ = southMove.state.map { sm =>
      Application.move(sm, North()) shouldEqual ActResult(List("An empty room"), Some(initialGameState))
    }
  }

  test("pickup") {
    val cell1 = Cell(List.empty[Item], "An empty room")
    val cell2 = Cell(List.empty[Item], "Just like the other rooms")
    val expectedWorldMap = WorldMap(Vector(Vector(cell1, cell2)))
    val initialLocation = Location(0, 0)
    val initialInventory = List(Sword())
    val expectedPlayerState = PlayerState(initialLocation, initialInventory)
    val expectedGameState = GameState(expectedPlayerState, expectedWorldMap)
    Application.pickup(initialGameState) shouldEqual ActResult(Nil, Some(expectedGameState))
  }
  test("drop") {
    val cell1 = Cell(List.empty[Item], "An empty room")
    val initialWorldMap = WorldMap(Vector(Vector(cell1)))

    val initialLocation = Location(0, 0)
    val initialInventory = List(Sword())
    val initialPlayerState = PlayerState(initialLocation, initialInventory)
    val initialGameState = GameState(initialPlayerState, initialWorldMap)

    val modifiedCell = Cell(List(Sword()), "An empty room")
    val expectedWorldMap = WorldMap(Vector(Vector(modifiedCell)))

    val expectedPlayerState = initialPlayerState.copy(inv = List.empty[Item])
    val expectedGameState = GameState(expectedPlayerState, expectedWorldMap)

    Application.drop(initialGameState) shouldEqual ActResult(Nil, Some(expectedGameState))
  }

  test("updateAt") {
    val updateLocation = Location(0, 1)
    val expectedCell = Cell(List.empty[Item], "updated")
    val expectedWorldMap = WorldMap(Vector(Vector(cell1, expectedCell)))
    worldMap.updateAt(updateLocation, (c: Cell) => c.copy(baseDesc = "updated")) shouldEqual Some(expectedWorldMap)
  }

}

object ITest extends App {

  //val io = Application.interactConsole("command")
  //val input = io.unsafePerformIO()
  //println(s"input $input")
}
