package jrigney.essential

import org.scalatest.WordSpec

class SumSpec extends WordSpec {
  "A Sum type" when {
    "subtyped as a Left" should {
      val s: Sum[Int, String] = Left(1)
      "return the same Left for map" in {
        val actual = s.map(_ + "Not Correct")
        assert(actual == Left(1))
      }
      "execute the failure function for fold" in {
        val failureFn = (a: Int) => a + 1
        val successFn = (b: String) => s"$b WRONG one"
        val actual = s.fold(failureFn, successFn)
        assert(actual == 2)
      }
      "return the same Left for flatMap" in {
        val rightFn = (b: String) => Right[Int, String](s"$b WRONG one")
        val actual = s.flatMap(rightFn)
        assert(actual == Left(1))
      }
    }
  }
}
