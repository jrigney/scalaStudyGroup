package jrigney.epi

import org.scalatest.FunSuite
import org.scalatest.Matchers

class SixDotSeventeenSuite extends FunSuite with Matchers {

  val fiveBy = Vector(
    Vector(1, 2, 3, 4, 5),
    Vector(6, 7, 8, 9, 10),
    Vector(11, 12, 13, 14, 15),
    Vector(16, 17, 18, 19, 20),
    Vector(21, 22, 23, 24, 25))

  test("placeholder") {
    val expected = Vector(1, 2, 3, 4, 5, 10, 15, 20, 25, 24, 23, 22, 21, 16, 11, 6, 7, 8, 9, 14, 19, 18, 17, 12, 13)
    SixDotSeventeen.sequence(fiveBy) shouldBe expected
  }
}
