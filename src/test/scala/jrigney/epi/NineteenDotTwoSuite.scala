package jrigney.epi

import org.scalatest.{ Matchers, WordSpec }

class NineteenDotTwoSuite extends WordSpec with Matchers {

  import NineteenDotTwo._

  "Given a 2D array af black and white entries and a start (x,y) flip the color" +
    "associated with the start" should {

      """for graph
      |  012
      |0 xoo
      |1 xox
      |2 oox
      |with start=(1,1) change locations (1,1),(2,1),(2,0),(0,1),(0,2) from white to black
    """.stripMargin in {
        val noCycles: Grid = Vector(
          Vector(Color.Black, Color.White, Color.White),
          Vector(Color.Black, Color.White, Color.Black),
          Vector(Color.White, Color.White, Color.Black))
        val maze = noCycles
        val start = Vertex(1, 1)

        val expected = List(Vertex(1, 1), Vertex(2, 1), Vertex(2, 0), Vertex(0, 1), Vertex(0, 2)).sortBy { case Vertex(r, c) => (r, c) }
        val actual = NineteenDotTwo.locationsToFlip(maze, start).sortBy { case Vertex(r, c) => (r, c) }
        actual shouldBe expected
      }

      //      """for graph
      //      |  0123
      //      |0 xoxo
      //      |1 xooo
      //      |2 xoxo
      //      |3 oooo
      //      |with entrance=(3,0) exit=(0,3) have a path (3,0),(3,1),(3,2),(3,3),(2,3),(1,3),(0,3)
      //    """.stripMargin in {
      //
      //        val cycles = Vector(
      //          Vector(Color.Black, Color.White, Color.Black, Color.White),
      //          Vector(Color.Black, Color.White, Color.White, Color.White),
      //          Vector(Color.Black, Color.White, Color.Black, Color.White),
      //          Vector(Color.White, Color.White, Color.White, Color.White))
      //        val maze = cycles
      //        val start = Vertex(3, 0)
      //        val end = Vertex(0, 3)
      //
      //        NineteenDotOne.path2(maze, start, end) shouldBe (Some(Path(7, List(Vertex(3, 0), Vertex(3, 1), Vertex(2, 1), Vertex(1, 1), Vertex(1, 2), Vertex(1, 3), Vertex(0, 3)))))
      //      }
      //
    }
}

