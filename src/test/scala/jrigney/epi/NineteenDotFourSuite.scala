package jrigney.epi

import org.scalatest.{ Matchers, WordSpec }

import scala.collection.immutable.HashMap

class NineteenDotFourSuite extends WordSpec with Matchers {

  import NineteenDotFour._
  "Given a directed graph" should {

    "detect cycles" in {
      val cycles: NineteenDotFour.Graph = HashMap[Int, Vector[Int]](
        (1 -> Vector(2, 3)),
        (2 -> Vector(5)),
        (3 -> Vector(4)),
        (4 -> Vector.empty),
        (5 -> Vector(3)))

      detectCycle(1, cycles) shouldBe (true)
    }

    "detect cycles circular" in {
      val cycles: NineteenDotFour.Graph = HashMap[Int, Vector[Int]](
        (1 -> Vector(2)),
        (2 -> Vector(3)),
        (3 -> Vector(4)),
        (4 -> Vector(1)))

      detectCycle(1, cycles) shouldBe (true)
    }

    "no cycle" in {
      val graph: NineteenDotFour.Graph = HashMap[Int, Vector[Int]](
        (1 -> Vector(2)),
        (2 -> Vector.empty),
        (3 -> Vector(4, 5)),
        (4 -> Vector.empty),
        (5 -> Vector.empty))

      detectCycle(1, graph) shouldBe (false)
    }
  }
}

