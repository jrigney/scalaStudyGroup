package jrigney.epi

import org.scalatest.FunSuite
import org.scalatest.Matchers

class FifteenDotOneSuite extends FunSuite with Matchers {

  /**
   * 2
   *   7
   *     2
   *       leaf
   *       leaf
   *     6
   *       5
   *         leaf
   *         leaf
   *       11
   *         leaf
   *         leaf
   *   5
   *     leaf
   *     9
   *       leaf
   *       4
   */

  test("placeholder") {
    val bTree = FifteenOneNode(
      2,
      FifteenOneNode(
        7,
        FifteenOneNode(
          2,
          FifteenOneLeaf(),
          FifteenOneLeaf()),
        FifteenOneNode(
          6,
          FifteenOneNode(
            5,
            FifteenOneLeaf(),
            FifteenOneLeaf()),
          FifteenOneNode(
            11,
            FifteenOneLeaf(),
            FifteenOneLeaf()))),
      FifteenOneNode(
        5,
        FifteenOneLeaf(),
        FifteenOneNode(
          9,
          FifteenOneNode(
            4,
            FifteenOneLeaf(),
            FifteenOneLeaf()),
          FifteenOneLeaf())))

    bTree.bst_? shouldBe false
  }
}

