package jrigney.epi

import org.scalatest.{ FunSuite, Matchers, WordSpec }

class NineteenDotOneSuite extends WordSpec with Matchers {

  import NineteenDotOne._

  "Given a 2D array af black and white entries representing a maze with designated entrance and " +
    "exit point find path, if one exists" should {

      """for graph
      |  012
      |0 xoo
      |1 xox
      |2 oox
      |with entrance=(2,0) exit=(0,2) have a path (2,0), (2,1), (1,1), (0,1), (0,2)
    """.stripMargin in {
        val noCycles = Vector(
          Vector(Color.Black, Color.White, Color.White),
          Vector(Color.Black, Color.White, Color.Black),
          Vector(Color.White, Color.White, Color.Black))
        val maze = noCycles
        val start = Vertex(2, 0)
        val end = Vertex(0, 2)

        NineteenDotOne.path2(maze, start, end) shouldBe (Some(Path(5, List(Vertex(2, 0), Vertex(2, 1), Vertex(1, 1), Vertex(0, 1), Vertex(0, 2)))))
      }

      """for graph
      |  0123
      |0 xoxo
      |1 xooo
      |2 xoxo
      |3 oooo
      |with entrance=(3,0) exit=(0,3) have a path (3,0),(3,1),(3,2),(3,3),(2,3),(1,3),(0,3)
    """.stripMargin in {

        val cycles = Vector(
          Vector(Color.Black, Color.White, Color.Black, Color.White),
          Vector(Color.Black, Color.White, Color.White, Color.White),
          Vector(Color.Black, Color.White, Color.Black, Color.White),
          Vector(Color.White, Color.White, Color.White, Color.White))
        val maze = cycles
        val start = Vertex(3, 0)
        val end = Vertex(0, 3)

        NineteenDotOne.path2(maze, start, end) shouldBe (Some(Path(7, List(Vertex(3, 0), Vertex(3, 1), Vertex(2, 1), Vertex(1, 1), Vertex(1, 2), Vertex(1, 3), Vertex(0, 3)))))
      }

    }
}

