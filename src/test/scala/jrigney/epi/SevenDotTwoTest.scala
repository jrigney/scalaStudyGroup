package jrigney.epi

import org.scalatest.FunSuite
import org.scalatest.Matchers

class SevenDotTwoTest extends FunSuite with Matchers {

  test("placeholder") {

    SevenDotTwo.baseConversion("615", 7, 13) shouldBe "1A7"
  }
}
