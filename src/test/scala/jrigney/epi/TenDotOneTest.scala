package jrigney.epi

import org.scalatest.FunSuite
class TenDotOneSuite extends FunSuite {

  import jrigney.epi.TenOneTree._

  test("Node only") {
    val tree = TenOneNode(1, TenOneLeaf(), TenOneLeaf())
    assert(isHeightBalanced(tree))
  }
  test("Left node only") {
    val tree = TenOneNode(1, TenOneNode(2, TenOneLeaf(), TenOneLeaf()), TenOneLeaf())
    assert(isHeightBalanced(tree))
  }
  test("Two Left node only which is not height balanced") {
    val tree = TenOneNode(1, TenOneNode(2, TenOneNode(3, TenOneLeaf(), TenOneLeaf()), TenOneLeaf()), TenOneLeaf())
    assert(isHeightBalanced(tree) == false)
  }
}
