package jrigney.epi

import org.scalatest.FunSuite
import org.scalatest.Matchers

class FourteenDotOneSuite extends FunSuite with Matchers {

  //  test("intersection") {
  //    val actual = FourteenDotOne.intersection(Array(2,3,3,5,5,6,7,7,8,12), Array(5,5,6,8,8,9,10,10))
  //    actual shouldBe Array(5,6,8)
  //  }

  //  test("evenIntersection") {
  //    val actual = FourteenDotOne.evenIntersection(Array(2,3,3,5,5,6,7,7,8,12), Array(5,5,6,8,8,9,10,10))
  //    actual shouldBe Array(5,6,8)
  //  }
  //

  test("evenIntersection") {
    val actual = FourteenDotOne.evenIntersection(Array(2), Array(2))
    actual shouldBe Array(2)
  }

}

