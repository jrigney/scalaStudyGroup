package jrigney.epi

import org.scalatest.FunSuite
import org.scalatest.Matchers

class ThirteenDotOneSuite extends FunSuite with Matchers {

  test("level") {
    ThirteenDotOne.palindromeable_?("level") shouldBe true
  }
  test("baa") {
    ThirteenDotOne.palindromeable_?("baa") shouldBe true
  }
  test("a") {
    ThirteenDotOne.palindromeable_?("a") shouldBe true
  }
  test("bca") {
    ThirteenDotOne.palindromeable_?("bca") shouldBe false
  }
}
