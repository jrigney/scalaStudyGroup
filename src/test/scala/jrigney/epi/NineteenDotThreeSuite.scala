package jrigney.epi

import org.scalatest.{ Matchers, WordSpec }

class NineteenDotThreeSuite extends WordSpec with Matchers {

  import NineteenDotThree._

  "Given a 2D array with entries White and Black replace all White that cannot reach the boundary with Black" should {

    """for graph
      |  012345
      |0 bbbbbb
      |1 wbwbbb
      |2 bwwbbb
      |3 bbbbbb
      |4 bwwwbb
      |5 bwbbbb
      |
      | return
      |
      |  012345
      |0 bbbbbb
      |1 wbbbbb
      |2 bbbbbb
      |3 bbbbbb
      |4 bwwwbb
      |5 bwbbbb
    """.stripMargin in {
      val noCycles: Grid = Vector(
        Vector(Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black),
        Vector(Color.White, Color.Black, Color.White, Color.Black, Color.Black, Color.Black),
        Vector(Color.Black, Color.White, Color.White, Color.Black, Color.Black, Color.Black),
        Vector(Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black),
        Vector(Color.Black, Color.White, Color.White, Color.White, Color.Black, Color.Black),
        Vector(Color.Black, Color.White, Color.Black, Color.Black, Color.Black, Color.Black))
      val maze = noCycles

      val expected: Grid = Vector(
        Vector(Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black),
        Vector(Color.White, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black),
        Vector(Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black),
        Vector(Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black),
        Vector(Color.Black, Color.White, Color.White, Color.White, Color.Black, Color.Black),
        Vector(Color.Black, Color.White, Color.Black, Color.Black, Color.Black, Color.Black))

      val actual = NineteenDotThree.boundary(maze)

      val result = for {
        row <- 0 to expected.lastIndexOf(expected.last)
      } yield {
        actual(row) == expected(row)
      }

      result.contains(false) shouldBe false

    }
  }
}

