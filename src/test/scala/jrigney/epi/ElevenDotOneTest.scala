package jrigney.epi

import org.scalatest.FunSuite

class ElevenDotOneSuite extends FunSuite {

  val alpha = List(3, 5, 7)
  val bravo = List(0, 6, 28)
  val charlie = List(0, 6)

  val lists = List(alpha, bravo, charlie)

  val expected = List(0, 0, 3, 5, 6, 6, 7, 28)
  test("Placeholder") {
    assert(ElevenDotOne.merge(lists) == expected)
  }
}
