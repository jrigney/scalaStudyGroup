package jrigney.epi

import org.scalatest.FunSuite
import org.scalatest.Matchers

class EighteenDotOneSuite extends FunSuite with Matchers {

  test("even number of tasks") {
    val taskDurations = List(5, 2, 1, 6, 4, 4)
    val expected = List((1, 6), (2, 5), (4, 4)).sorted
    EighteenDotOne.optimumTaskAssignment(taskDurations: List[Int]).sorted shouldBe expected
  }

  test("odd number of tasks") {
    val taskDurations = List(5, 2, 1, 6, 4)
    val expected = List((6, 0), (1, 5), (2, 4)).sorted
    EighteenDotOne.optimumTaskAssignment(taskDurations: List[Int]).sorted shouldBe expected
  }
}
