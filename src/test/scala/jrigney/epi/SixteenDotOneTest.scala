package jrigney.snippet

import org.scalatest.FunSuite
import org.scalatest.Matchers

object Hanoi {

  def hanoi(source: Int, temp: Int, dest: Int, numDisks: Int, step: Int): Unit = {
    println(s"s $source t $temp d $dest numD $numDisks step $step")
    if (numDisks <= 0) println("")
    else {
      hanoi(source, dest, temp, numDisks - 1, 1)
      println(s"move disk $numDisks to from $source to $dest")
      hanoi(temp, source, dest, numDisks - 1, 3)
    }
  }
}
class HanoiSuite extends FunSuite with Matchers {
  import Hanoi._
  test("placeholder") {
    val source = 1
    val temp = 2
    val dest = 3
    hanoi(source, temp, dest, 2, 0)

    assert(true)
  }
}
