package jrigney.epi

import org.scalatest.FunSuite
import org.scalatest.Matchers

class SeventeenDotOneSuite extends FunSuite with Matchers {

  test("final 12 plays 2, 3, 7") {
    val finalScore = 12
    val listPlayScores = List(2, 3, 7)
    SeventeenDotOne.playCombos(finalScore, listPlayScores) shouldBe 4
  }

  test("final 12 plays 2, 3") {
    val finalScore = 12
    val listPlayScores = List(2, 3)
    SeventeenDotOne.playCombos(finalScore, listPlayScores) shouldBe 3
  }

  test("final 12 plays 2") {
    val finalScore = 12
    val listPlayScores = List(2)
    SeventeenDotOne.playCombos(finalScore, listPlayScores) shouldBe 1
  }
}
