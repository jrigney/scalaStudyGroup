package jrigney.epi

import org.scalatest.FunSuite
import org.scalatest.Matchers
import scala.collection.immutable.Queue
import jrigney.snippet.QueuedRandomSource

class SixDotElevenSuite extends FunSuite with Matchers {

  val input = Vector(3, 7, 5, 11)

  val randomSource = QueuedRandomSource(Queue(2, 2, 1))

  test("subset of three") {
    val unit = SixDotEleven(randomSource)
    unit.subset(input, 3).sorted shouldBe List(5, 7, 11).sorted
  }
}

