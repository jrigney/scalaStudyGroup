package jrigney.fiftyseven

import org.scalatest.FunSuite
import org.scalatest.Matchers

class CountingCharsSuite extends FunSuite with Matchers {
  test("count") {
    CountingChars.count("homer") shouldBe (("homer", 5))
  }
}
