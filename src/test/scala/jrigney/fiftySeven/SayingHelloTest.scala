package jrigney.fiftyseven

import org.scalatest.FunSuite
import org.scalatest.Matchers

class SayingHelloSuite extends FunSuite with Matchers {
  test("saying hello") {
    SayingHello.hello("bob") shouldBe "Hello bob"
  }
}
