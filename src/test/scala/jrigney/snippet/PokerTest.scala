package jrigney.snippet

import org.scalatest.FunSuite
import org.scalatest.Matchers

class PokerSuite extends FunSuite with Matchers {
  test("Card unapply") {
    Card.unapply("8C") shouldBe Some(Club(8))
  }

  //  test("Hand unapply") {
  //    Hand.unapply("8C,9C,10C,JC,QC") shouldBe Some( List(Club(8), Club(9), Club(10), Club(11), Club(12)))
  //  }

  test("sequence_?") {
    Hand.sequence_?(List(Club(8), Club(9), Club(10), Club(11), Club(12))) shouldBe true
    Hand.sequence_?(List(Club(9), Club(10), Club(11), Club(12), Club(8))) shouldBe true
    Hand.sequence_?(List(Diamond(8), Club(9), Club(10), Club(11), Club(12))) shouldBe true
    Hand.sequence_?(List(Diamond(5), Club(9), Club(10), Club(11), Club(12))) shouldBe false
    Hand.sequence_?(List(Club(6), Club(9), Club(10), Club(11), Club(12))) shouldBe false
  }

  test("kinds") {
    Hand.kinds(List(Club(8), Diamond(8), Heart(8), Spade(8), Club(12))) shouldBe FourKind(8)
    Hand.kinds(List(Club(8), Diamond(8), Heart(8), Spade(9), Club(12))) shouldBe ThreeKind(8)
    Hand.kinds(List(Club(8), Diamond(8), Heart(9), Spade(9), Club(12))) shouldBe TwoPair(9)
    Hand.kinds(List(Club(8), Diamond(8), Heart(9), Spade(9), Club(9))) shouldBe FullHouse(9, 8)
    Hand.kinds(List(Club(8), Diamond(8), Heart(2), Spade(9), Club(12))) shouldBe OnePair(8)
  }
  test("hand apply") {
    //    Hand.apply("8C,9C,10C,JC,QC") shouldBe Straight()
    Hand.apply("8C,8H,8D,JC,QC") shouldBe ThreeKind(8)
  }

}
