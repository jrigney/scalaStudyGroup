package jrigney.snippet

object Fib {

  def fib(x: Int): Int = {
    def loop(remainingSumsToCalculate: Int, currentSum: Int, nextSum: Int): Int = {
      remainingSumsToCalculate match {
        case 0 => currentSum
        case _ => loop(remainingSumsToCalculate - 1, nextSum, currentSum + nextSum)
      }
    }
    loop(x, 0, 1)
  }
}

import org.scalatest.FunSuite
class FibSuite extends FunSuite {

  test("Fib 1") {
    assert(Fib.fib(1) == 1)
  }
  test("Fib 2") {
    assert(Fib.fib(2) == 1)
  }
  test("Fib 5") {
    assert(Fib.fib(5) == 5)
  }
  test("Fib 25") {
    assert(Fib.fib(25) == 75025)
  }

}

