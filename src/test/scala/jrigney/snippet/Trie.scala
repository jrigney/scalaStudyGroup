package jrigney.snippet

import scala.collection._

//sealed trait ZZZ {
//
//  def append(key : String): Unit
//  def findByPrefix(prefix: String): scala.collection.Seq[String]
//
//}

class TrieNode(val char: Option[Char] = None, var word: Option[String] = None) {

  val children: mutable.Map[Char, TrieNode] = mutable.Map.empty[Char, TrieNode]

  /**
   * given a key:String
   * while key still has letters
   *   take first letter from key
   *   if children does not contain first char of key
   *     create key
   *     add key to children
   *   else
   *     loop to the child node
   *
   */
  def append(key: String): Unit = {

    @scala.annotation.tailrec
    def loop(node: TrieNode, restOfKey: String): Unit = {
      if (restOfKey.isEmpty)
        node.word = Option(key)
      else {
        val firstChar = restOfKey.head
        val child: TrieNode = node.children.getOrElseUpdate(firstChar, new TrieNode(Option(firstChar)))
        loop(child, restOfKey.tail)
      }
    }
    loop(this, key)
  }

  def findByPrefix(prefix: String): List[String] = {

    @scala.annotation.tailrec
    def loop(restOfPrefix: String, node: TrieNode, wordList: List[String]): List[String] = {
      if (restOfPrefix.isEmpty)
        getWords(node) ++ wordList
      else {
        val firstChar = restOfPrefix.head
        node.children.get(firstChar) match {
          case Some(child) => loop(restOfPrefix.tail, child, wordList)
          case None => wordList
        }
      }
    }

    loop(prefix, this, List.empty[String])
  }

  def getWords(node: TrieNode): List[String] = {

    @scala.annotation.tailrec
    def loop(remainingNodes: List[TrieNode], wordList: List[Option[String]]): List[String] = {
      remainingNodes match {
        case Nil => wordList.flatten
        case (h :: t) => loop(t ++ h.children.values, h.word +: wordList)
      }
    }

    loop(List(node), List.empty[Option[String]])
  }

  def dump(): String = {

    def loop(remainingNodes: List[TrieNode], acc: String): String = {
      remainingNodes match {
        case Nil => acc
        case (h :: t) =>
          val thisDump = if (h.children.isEmpty) s"char ${h.char} children EMPTY\n"
          else s"char ${h.char} children ${h.children.values.flatMap(_.char)}\n"
          loop(t ++ h.children.values, acc + thisDump)
      }
    }
    loop(List(this), "### Dump\n")
  }
}

import org.scalatest.FunSuite
class TrieSuite extends FunSuite {

  val unit = new TrieNode()

  test("An empty Set should have size 0") {
    assert(Set.empty.size == 0)
  }

  test("Trie append") {
    unit.append("abc")
    assert(unit.findByPrefix("abc") == List("abc"))
  }

  test("Trie findByPrefix") {
    unit.append("abc")
    unit.append("abg")
    unit.append("acg")
    unit.append("bcd")
    //    println(s"${unit.dump}") /*REMOVEME*/
    assert(unit.findByPrefix("ab").sorted == List("abg", "abc").sorted)
    assert(unit.findByPrefix("b").sorted == List("bcd").sorted)
    assert(unit.findByPrefix("a").sorted == List("abg", "abc", "acg").sorted)
  }
}

