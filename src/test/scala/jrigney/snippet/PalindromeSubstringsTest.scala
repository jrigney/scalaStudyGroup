package jrigney.snippet

import org.scalatest.FunSuite
import org.scalatest.Matchers

class PalindromeSubstringsSuite extends FunSuite with Matchers {

  test("palindrome substrings") {
    val in = "xxyx"
    val expected = Set("x", "y", "xx", "xyx")

    PalindromeSubstrings.palindromSubstrings(in) shouldBe expected
  }

  test("all substrings") {
    val in = "xxyx"
    val expected = Set("xxyx", "xxy", "xx", "x", "xyx", "xy", "yx", "y")

    PalindromeSubstrings.subStrings(in) shouldBe expected
  }

  test("palindrome_?") {
    PalindromeSubstrings.palindrome_?("a") shouldBe true
    PalindromeSubstrings.palindrome_?("aa") shouldBe true
    PalindromeSubstrings.palindrome_?("aaa") shouldBe true
    PalindromeSubstrings.palindrome_?("aba") shouldBe true
    PalindromeSubstrings.palindrome_?("abba") shouldBe true
    PalindromeSubstrings.palindrome_?("ab") shouldBe false
  }
}
