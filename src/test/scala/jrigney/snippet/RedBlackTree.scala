package jrigney.snippet

object RedBlackTree {
  def makeBlack[K: Ordering, V](tree: RedBlackTree[K, V]) = tree match {
    case Node(false, left, key, value, right) => Node(true, left, key, value, right)
    case t => t
  }

  def balance[K: Ordering, V](isBlack: Boolean, left: RedBlackTree[K, V], key: K, value: V, right: RedBlackTree[K, V]) = {
    (isBlack, left, key, value, right) match {
      case (true, Node(false, Node(false, a, xk, xv, b), yk, yv, c), zk, zv, d) =>
        Node(false, Node(true, a, xk, xv, b), yk, yv, Node(true, c, zk, zv, d))

      case (true, Node(false, a, xk, xv, Node(false, b, yk, yv, c)), zk, zv, d) =>
        Node(false, Node(true, a, xk, xv, b), yk, yv, Node(true, c, zk, zv, d))

      case (true, a, xk, xv, Node(false, Node(false, b, yk, yv, c), zk, zv, d)) =>
        Node(false, Node(true, a, xk, xv, b), yk, yv, Node(true, c, zk, zv, d))

      case (true, a, xk, xv, Node(false, b, yk, yv, Node(false, c, zk, zv, d))) =>
        Node(false, Node(true, a, xk, xv, b), yk, yv, Node(true, c, zk, zv, d))

      case (isBlack, a, xk, xv, b) => Node(isBlack, a, xk, xv, b)
    }
  }

  sealed abstract class RedBlackTree[K: Ordering, +V] {

    val isBlack: Boolean

    def left: RedBlackTree[K, V]

    def key: K
    def value: V

    def right: RedBlackTree[K, V]

    def apply(key: K): Option[V]

    def updated[A >: V](key: K, value: A): RedBlackTree[K, A]

    def -(key: K): RedBlackTree[K, V]

    def removeMin: (RedBlackTree[K, V], K, V)

    def toStream: Stream[(K, V)]

    /**
     * BFS search to list
     */
    def valuesByBreadth(): List[RedBlackTree[K, V]] = {
      import scala.collection.immutable.Queue
      def loop(q: Queue[RedBlackTree[K, V]]): List[RedBlackTree[K, V]] = {
        q match {
          case Leaf() +: xs => loop(xs)
          case x +: xs => x :: loop(xs :+ x.left :+ x.right)
          case _ => Nil
        }
      }

      loop(Queue(this))
    }

    /**
     * Depth first pre-order traversal
     */
    def prettyPrint(): List[String] = {
      def loop(stack: List[RedBlackTree[K, V]], depth: Int): List[String] = {
        stack match {
          case s if s.isEmpty => List.empty[String]
          case Leaf() :: t => loop(t, depth - 1)
          case Node(isBlack, left, key, value, right) :: t =>
            val leftSide = loop(left :: t, depth + 1)
            val rightSide = loop(right :: t, depth + 1)
            List(s"${" " * depth}$key $isBlack") ++: leftSide ++: rightSide
        }
      }

      loop(List(this), 0)

    }
  }

  case class Node[K: Ordering, +V](isBlack: Boolean, left: RedBlackTree[K, V], key: K, value: V, right: RedBlackTree[K, V]) extends RedBlackTree[K, V] {
    import Ordered._

    def apply(key: K) = {
      if (this.key > key)
        left(key)
      else if (this.key == key)
        Some(value)
      else // (this.key < key)
        right(key)
    }

    def updated[A >: V](key: K, value: A) = {
      if (this.key > key)
        balance(isBlack, left.updated(key, value), this.key, this.value, right)
      else if (this.key == key)
        Node(isBlack, left, key, value, right)
      else //  (this.key < key)
        balance(isBlack, left, this.key, this.value, right.updated(key, value))
    }

    def -(key: K) = {
      if (this.key > key) {
        balance(isBlack, left - key, this.key, this.value, right)
      } else if (this.key == key) {
        (left, right) match {
          case (Node(_, _, _, _, _), Node(_, _, _, _, _)) => {
            val (right2, key2, value2) = right.removeMin
            balance(isBlack, left, key2, value2, right2)
          }

          case (Leaf(), Node(_, _, _, _, _)) => right

          case (Node(_, _, _, _, _), Leaf()) => left

          case (Leaf(), Leaf()) => Leaf()
        }
      } else {
        balance(isBlack, left, this.key, this.value, right - key)
      }
    }

    def removeMin = left match {
      case Node(_, _, _, _, _) => {
        val (left2, key2, value2) = left.removeMin
        (balance(isBlack, left2, key, value, right), key2, value2)
      }

      case Leaf() => (right, key, value)
    }

    def toStream = left.toStream ++ ((key, value) #:: right.toStream)
  }

  /**
   * Represents the Nil case in a Red-Black Tree
   */
  case class Leaf[K: Ordering]() extends RedBlackTree[K, Nothing] {
    val isBlack = true

    def left = sys.error("left of an empty tree")

    def key = sys.error("key of an empty tree")
    def value = sys.error("value of an empty tree")

    def right = sys.error("right of an empty tree")

    def apply(key: K) = None

    def updated[V](key: K, value: V) = Node(false, Leaf(), key, value, Leaf())

    def -(key: K) = this

    def removeMin = sys.error("cannot removeMin of an empty tree")

    val toStream = Stream()
  }

}
import org.scalatest.FunSuite
class RedBlackTreeSuite extends FunSuite {
  import RedBlackTree._

  val unit = new Node[Int, String](false, Leaf(), 8, "eight", Leaf())

  test("An empty Set should have size 0") {
    assert(Set.empty.size == 0)
  }

  test("Initial creation Z == root") {
    val levelOrder = unit.valuesByBreadth
    assert(levelOrder.map(_.value) == List("eight"))
  }

  test("Insert simple") {
    val levelOrder = unit.updated(5, "five").valuesByBreadth
    val root = (unit.value, unit.isBlack) //because the root can be either red or black
    val rest = List(("five", false))
    val expected = root +: rest
    assert(levelOrder.map(x => (x.value, x.isBlack)).sameElements(expected))
  }

  test("Insert Z uncle == Red") {

    def insert(key: (Int, String), node: RedBlackTree[Int, String]): RedBlackTree[Int, String] = {
      val r = node.updated(key._1, key._2)
      //      println(s"### ${key._1} after $r")
      //      println(s"### ${key._1} pp\n${r.prettyPrint.mkString("\n")}")
      r
    }

    val updates = List((5, "five"), (15, "fifteen"), (7, "seven"), (3, "three"))
    val unit: RedBlackTree[Int, String] = new Node[Int, String](true, Leaf(), 8, "eight", Leaf())
    val noCare = updates.foldLeft(unit) { (acc, next) => insert(next, acc) }

  }

}
