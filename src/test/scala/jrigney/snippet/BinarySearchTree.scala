package jrigney.snippet

sealed trait BinarySearchTree {
  def key: Int
  def value: String
  def left: BinarySearchTree
  def right: BinarySearchTree
  def apply(key: Int): Option[String]
  def isEmpty(): Boolean

  def add(key: Int, value: String): BinarySearchTree
  def delete(key: Int): BinarySearchTree
  def min(): (Int, String)
  def max(): (Int, String)

  def valuesByLevelOrder(): List[String]
  def valuesByInorder(): List[String]
}

final case class Leaf() extends BinarySearchTree {
  def key = sys.error("key of an empty tree")
  def value = sys.error("value of an empty tree")
  def left = sys.error("left of an empty tree")
  def right = sys.error("right of an empty tree")
  def apply(key: Int) = None
  def isEmpty() = true

  def add(key: Int, value: String) = Node(Leaf(), key, value, Leaf())
  def delete(key: Int) = sys.error("delete of an empty tree")
  def min() = sys.error("min of an empty tree")
  def max() = sys.error("max of an empty tree")
  def valuesByLevelOrder() = List.empty[String]
  def valuesByInorder() = List.empty[String]
}

/**
 * 8
 * /    \
 * 3      10
 * / \       \
 * 1   6       14
 * /
 * 4
 */
final case class Node(left: BinarySearchTree, key: Int, value: String, right: BinarySearchTree) extends BinarySearchTree {
  def apply(key: Int) = {
    if (this.key > key)
      left(key)
    else if (this.key == key)
      Some(value)
    else //this.key < key
      right(key)
  }

  def isEmpty(): Boolean = false

  def add(key: Int, value: String): BinarySearchTree = {
    if (this.key > key)
      Node(left.add(key, value), this.key, this.value, right)
    else if (this.key < key)
      Node(left, this.key, this.value, right.add(key, value))
    else
      Node(Leaf(), key, value, Leaf())
  }

  def delete(key: Int): BinarySearchTree = {
    println(s"dk $key")
    if (this.key > key)
      Node(left.delete(key), this.key, this.value, right)
    else if (this.key < key)
      Node(left, this.key, this.value, right.delete(key))
    else if (left.isEmpty && right.isEmpty) Leaf()
    else if (left.isEmpty) right
    else if (right.isEmpty) left
    else {
      val (skey, svalue) = right.min
      println(s"sk $skey $svalue")
      Node(left, skey, svalue, right.delete(skey))
    }
  }

  def min(): (Int, String) = {
    def loop(t: BinarySearchTree, key: Int, value: String): (Int, String) = {
      if (t.isEmpty) (key, value) else loop(t.left, t.key, t.value)
    }

    if (isEmpty) sys.error("Empty tree")
    else loop(left, key, value)
  }

  def max(): (Int, String) = {
    def loop(t: BinarySearchTree, key: Int, value: String): (Int, String) = {
      if (t.isEmpty) (key, value) else loop(t.right, t.key, t.value)
    }

    if (isEmpty) sys.error("Empty tree")
    else loop(right, key, value)
  }

  /**
   * BFS search to list
   */
  def valuesByLevelOrder(): List[String] = {
    import scala.collection.immutable.Queue
    def loop(q: Queue[BinarySearchTree]): List[String] = {
      if (q.isEmpty) Nil
      else if (q.head.isEmpty) loop(q.tail)
      else q.head.value :: loop(q.tail :+ q.head.left :+ q.head.right)
    }

    loop(Queue(this))
  }

  /**
   * def traverse_binary_tree(node, callback):
   *     if node is None:
   *         return
   *     traverse_binary_tree(node.leftChild, callback)
   *     callback(node.value)
   *     traverse_binary_tree(node.rightChild, callback)
   *
   * DFS
   */
  def valuesByInorder(): List[String] = {
    def loop(stack: List[BinarySearchTree]): List[String] = {
      if (stack.isEmpty) List.empty[String]
      else if (stack.head.isEmpty) loop(stack.tail)
      else {
        val leftSide = loop(stack.head.left :: stack.tail)
        val rightSide = loop(stack.head.right :: stack.tail)
        leftSide ::: List(stack.head.value) ::: rightSide
      }
      //else stack.head.value :: loop(stack.head.right :: stack.head.left :: stack.tail)
    }

    loop(List(this))

  }
}

import org.scalatest.FunSuite
class BinarySearchTreeSuite extends FunSuite {

  def buildTree(elems: List[(Int, String)], tree: BinarySearchTree): BinarySearchTree = {
    def loop(remainingElems: List[(Int, String)], tree: BinarySearchTree): BinarySearchTree = {
      remainingElems match {
        case Nil => tree
        case h :: rest => loop(rest, tree.add(h._1, h._2))
      }
    }
    loop(elems, tree)
  }

  def valuesOf(elems: List[Int]): List[String] = {
    elems.map { x => Values(x)._2 }
  }
  val Values = Map[Int, (Int, String)](
    (1, (1, "")),
    (3, (3, "three")),
    (4, (4, "four")),
    (6, (6, "six")),
    (8, (8, "eight")),
    (10, (10, "ten")),
    (14, (14, "fourteen")))

  val start = Node(Leaf(), 8, "eight", Leaf())

  val elems: List[(Int, String)] = List(
    Values(3),
    Values(1),
    Values(6),
    Values(4),
    Values(10),
    Values(14))
  val unit = buildTree(elems, start)

  test("apply") {
    assert(unit(3) == Some(Values(3)._2))
  }

  test("breadth") {
    assert(unit.valuesByLevelOrder == valuesOf(List(8, 3, 10, 1, 6, 14, 4)))
  }

  test("inorder") {
    assert(unit.valuesByInorder == valuesOf(List(1, 3, 4, 6, 8, 10, 14)))
  }

}
object BinarySearchTreeApp extends App {

  val a = Node(Leaf(), 8, "eight", Leaf())
  val b = a.add(3, "three")
  val c = b.add(1, "one")
  val d = c.add(6, "six")
  val e = d.add(4, "four")
  val f = e.add(10, "ten")
  val g = f.add(14, "fourteen")

  println(s"breadth ${g.valuesByLevelOrder}")
  println(s"apply 3 ${g(3)}")
  println(s"delete 3 ${g.delete(3).valuesByLevelOrder}")
  println(s"inorder ${g.valuesByInorder}")
  println(s"max ${g.max}")

  val z = Node(Leaf(), 5, "five", Leaf())
  val y = z.add(2, "two")
  val x = y.add(1, "one")
  val w = x.add(3, "three")
  val q = w.add(7, "seven")
  val r = q.add(8, "eight")
  println(s"breadth ${r.valuesByLevelOrder}")
  println(s"delete 2 ${r.delete(2).valuesByLevelOrder}")
  //println(s"inorder ${r.valuesByInorder}")

  val aa = Node(Leaf(), 4, "four", Leaf())
  val bb = aa.add(1, "one")
  val cc = bb.add(6, "six")
  println(s"inorder ${cc.valuesByInorder}")
}
