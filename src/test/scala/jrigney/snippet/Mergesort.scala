package jrigney.snippet

object Mergesort {
  def mergesort[A](list: List[A])(implicit ev: A => Ordered[A]): List[A] = {
    def sort(p: (List[A], List[A])): List[A] = p match {
      case (Nil, Nil) => Nil
      case (ha :: Nil, Nil) => ha :: Nil
      case (Nil, hb :: Nil) => hb :: Nil
      case (as, bs) => merge(halfifyAndSort(as), halfifyAndSort(bs))
    }

    def halfify(as: List[A]): (List[A], List[A]) = {
      as match {
        case f :: s :: r => as.splitAt(as.size / 2)
        case f :: Nil => (List(f), Nil)
        case Nil => (Nil, Nil)
      }
    }

    def merge(as: List[A], bs: List[A]): List[A] = {
      def loop(cs: List[A], ds: List[A], r: List[A]): List[A] = (cs, ds) match {
        case (ha :: ta, hb :: tb) =>
          if (ha < hb) loop(ta, ds, ha :: r)
          else loop(cs, tb, hb :: r)
        case (ha :: ta, Nil) => loop(ta, Nil, ha :: r)
        case (Nil, hb :: tb) => loop(Nil, tb, hb :: r)
        case (Nil, Nil) => r
      }

      loop(as, bs, Nil).reverse
    }

    def halfifyAndSort(as: List[A]) = sort(halfify(as))

    halfifyAndSort(list)
  }
}

import org.scalatest.FunSuite
class MergesortSuite extends FunSuite {

  val in = List(38, 27, 43, 3, 9, 82, 10)
  val x = List(38, 27, 43)

  test("mergesort") {
    assert(Mergesort.mergesort(in) == List(3, 9, 10, 27, 38, 43, 82))
  }

  test("mergesort 2 elements") {
    assert(Mergesort.mergesort(List(9, 3)) == List(3, 9))
  }

  test("mergesort 1 elements") {
    assert(Mergesort.mergesort(List(9)) == List(9))
  }

  test("mergesort 0 elements") {
    assert(Mergesort.mergesort(List.empty[Int]) == List.empty[Int])
  }
}
