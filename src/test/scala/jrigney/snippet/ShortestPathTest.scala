package jrigney.snippet

import org.scalatest.FunSuite
import org.scalatest.Matchers
import jrigney.snippet.ShortestPath.Path

class ShortestPathSuite extends FunSuite with Matchers {
  val cycleDirected: Map[Int, List[(Int, Int)]] = Map(
    (1 -> List((2, 4), (3, 2))),
    (2 -> List((3, 5), (4, 10))),
    (3 -> List((5, 3))),
    (4 -> List((6, 11))),
    (5 -> List((4, 4))))

  test("cycleDirected") {

    ShortestPath.shortestPath(1, 6, cycleDirected) shouldBe Path(20, List(1, 3, 5, 4, 6))
  }
}
