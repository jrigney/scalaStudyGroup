package jrigney.snippet

import org.scalatest.FunSuite
import org.scalatest.Matchers

class LogEntriesSuite extends FunSuite with Matchers {

  test("placeholder") {
    val input = List((1.2, 4.5), (3.1, 6.7), (8.9, 10.3))
    val expected = List((1.2, 1), (3.1, 2), (4.5, 1), (6.7, 0), (8.9, 1), (10.3, 0))
    LogEntries.counts(input) shouldBe (expected)
  }
}
