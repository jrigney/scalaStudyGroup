package jrigney.epi

import scala.annotation.tailrec
import scala.collection.immutable.HashMap

object NineteenDotFour {
  type Graph = HashMap[Int, Vector[Int]]

  def detectCycle(start: Int, graph: Graph): Boolean = {

    //dfs stack
    @tailrec
    def loop(fringe: Vector[Int], visited: Vector[Int], hasCycle: Boolean): Boolean = {
      fringe match {
        case fringe if fringe.isEmpty => hasCycle
        case head +: tail =>
          val potentialNeighbors = graph(head)
          if (potentialNeighbors.isEmpty) loop(tail, head +: visited, hasCycle)
          else {
            val (visitedVertex, notVisitedVertex) = potentialNeighbors.partition(visited.contains)
            loop(notVisitedVertex ++ tail, head +: visited, !visitedVertex.isEmpty)
          }
      }
    }

    loop(Vector(start), Vector.empty, false)
  }
}
