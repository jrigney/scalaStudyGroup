package jrigney.epi

import jrigney.snippet.RandomSource

case class SixDotEleven(randomSource: RandomSource[Int]) {

  def subset[A](elements: Vector[A], subsetSize: Int): List[A] = {
    val (subElems, _, _) = (1 to subsetSize).foldLeft((List.empty[A], elements, randomSource)) { (acc, _) =>
      val (subset, elements, random) = acc
      val (randomIndex, rs) = random.next
      val (l, r) = elements.view.splitAt(randomIndex)
      (r.head +: subset, (l ++ r.tail).toVector, rs)
    }
    subElems
  }
}
