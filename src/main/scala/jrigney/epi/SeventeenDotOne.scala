package jrigney.epi

object SeventeenDotOne {

  //mutable
  def playCombos(finalScore: Int, playScores: List[Int]): Int = {

    val numCombinationsForScore = Array.ofDim[Int](playScores.size, finalScore + 1)

    for {
      i <- (0 until playScores.size)
      j <- (1 to finalScore)
    } yield {
      numCombinationsForScore(i)(0) = 1 //there is always one way to reach a score of 0
      val withoutThisPlay = if (i - 1 >= 0) numCombinationsForScore(i - 1)(j) else 0
      val withThisPlay = if (j >= playScores(i)) numCombinationsForScore(i)(j - playScores(i)) else 0
      numCombinationsForScore(i)(j) = withoutThisPlay + withThisPlay
      //println(s"\n${numCombinationsForScore.deep.mkString("\n")}")
    }
    numCombinationsForScore(playScores.size - 1)(finalScore)
  }
}
