package jrigney.epi

import scala.annotation.tailrec
import scala.collection.immutable
import scala.collection.immutable.VectorBuilder

object NineteenDotThree {

  type Grid = Vector[Vector[Color.EnumVal]]

  def boundary(grid: Grid): Grid = {

    val colFirstIndex = 0
    val colLastIndex = grid.head.lastIndexOf(grid.head.last)
    val rowFirstIndex = 0
    val rowLastIndex = grid.lastIndexOf(grid.last)

    def boundary(): Set[Vertex] = {

      val boundaryVertices = {

        val northSouth = for {
          row <- Set(rowFirstIndex, rowLastIndex)
          col <- colFirstIndex to colLastIndex
        } yield Vertex(row, col)

        val eastWest = for {
          col <- Set(colFirstIndex, colLastIndex)
          row <- rowFirstIndex to rowLastIndex
        } yield Vertex(row, col)

        (northSouth ++ eastWest)

      }

      boundaryVertices.filter {
        case Vertex(r, c) if grid(r)(c) == Color.White => true
        case _ => false
      }
    }

    @tailrec
    def bfs(fringe: Vector[Vertex], result: Vector[Vertex], visited: Vector[Vertex]): Vector[Vertex] = {

      def adjacentVertices(vertex: NineteenDotThree.Vertex): Vector[Vertex] = {
        val shift = Vector((-1, 0), (1, 0), (0, 1), (0, -1))
        shift.map { case (r, c) => Vertex(vertex.row + r, vertex.col + c) }
      }

      def inGrid_?(v: Vertex): Boolean = {
        val validRow = v.row >= grid(0).indexOf(grid(0).head) && v.row < grid(0).size
        val validCol = v.col >= grid.indexOf(grid.head) && v.col < grid.size

        validRow && validCol
      }

      def isWhite_?(v: Vertex): Boolean = {
        grid(v.row)(v.col) == Color.White
      }

      fringe match {
        case f if f.isEmpty => result
        case h +: t =>
          val potentialNeighbors = adjacentVertices(h).filter(inGrid_?).filter(isWhite_?)
          val newNeighbors = potentialNeighbors.filterNot(visited.contains)

          if (newNeighbors.isEmpty) bfs(t, h +: result, h +: visited)
          else bfs(t ++ newNeighbors, h +: result, h +: visited)

      }
    }

    val whiteAtBoundary: Vector[Vertex] = boundary().toVector

    val allWhiteConnectedToBoundary = bfs(whiteAtBoundary, whiteAtBoundary, whiteAtBoundary)

    //final fill all black and layout white
    val blackGrid: Grid = Vector.fill(grid(0).size, grid.size)(Color.Black)
    allWhiteConnectedToBoundary.foldLeft(blackGrid) { (acc, next) => acc.updated(next.row, acc(next.row).updated(next.col, Color.White)) }
  }

  object Color {

    sealed trait EnumVal

    case object White extends EnumVal

    case object Black extends EnumVal

  }

  case class Vertex(row: Int, col: Int)

}
