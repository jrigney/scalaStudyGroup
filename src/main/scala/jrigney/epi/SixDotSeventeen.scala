package jrigney.epi

/**
 * constraints
 * min/max size matrix
 * value types
 * min/max values
 *
 * example
 * 5x5 matrix
 *
 * brute force
 *
 * diroffset = offset to matrix.size - offset
 *   add matrix(row,matrix.size - diroffset -1) to seq
 *
 * diroffset = offset to matrix.size - offset
 *   add matrix(matrix.size - dirOffset -1, col) to seq
 *
 * diroffset = matrix.size - offset to offset
 *   add matrix(row, matrix.size - diroffset -1 ) to seq
 *
 * diroffset = matrix.size - offset to offset
 *   add matrix(matrix.size - diroffset -1, col ) to seq
 *
 * brute force bigo
 * n^2 because the matrix is an NxN and we need to cover all the elements in the matrix to get the sequence
 *
 * optimized
 *
 * direction keeps track of what to add/subtract to a row or column based on direction headed
 * 0 = north, 1 = east, 2 = south, 3 = west
 *
 * update matrix with 0 for visited coord
 * update seq
 *
 * using direction calculate next step on given direction
 * validate direction making sure next step is in original matrix boundary and is not in the exterior perimeter
 *   as indicated byusing  the coord having a 0 value.
 * if not valid change direction and loop
 * else loop with calculated directions
 *
 */
object SixDotSeventeen {
  def sequence(matrix: Vector[Vector[Int]]): Vector[Int] = {

    //(row addition/substraction, col addition/substraction)
    val direction: Vector[(Int, Int)] = Vector((0, 1), (1, 0), (0, -1), (-1, 0))

    def loop(
      matrix: Vector[Vector[Int]],
      row: Int,
      col: Int,
      heading: Int,
      seq: Vector[Int]): Vector[Int] = {

      def updateMatrix(row: Int, col: Int): Vector[Vector[Int]] = {
        val updatedRow = matrix(row).updated(col, 0)
        matrix.updated(row, updatedRow)
      }

      if (matrix(row)(col) == 0) seq
      else {
        val updatedSeq = seq :+ matrix(row)(col)
        val updatedMatrix = updateMatrix(row, col)

        //take next step on current heading
        val nextRowCandidate = row + direction(heading)._1
        val nextColCandidate = col + direction(heading)._2

        //validate next step on current heading
        if (nextRowCandidate < 0 || nextRowCandidate >= matrix.size ||
          nextColCandidate < 0 || nextColCandidate >= matrix.size ||
          matrix(nextRowCandidate)(nextColCandidate) == 0) {
          //heading is not correct so change direction
          val newHeading = (heading + 1) % 4
          loop(updatedMatrix, row + direction(newHeading)._1, col + direction(newHeading)._2, newHeading, updatedSeq)
        } else {
          //heading is ok so keep going
          loop(updatedMatrix, nextRowCandidate, nextColCandidate, heading, updatedSeq)
        }
      }

    }

    loop(matrix, 0, 0, 0, Vector.empty[Int])
  }
}
