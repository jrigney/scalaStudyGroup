package jrigney.epi

import TenOneTree._

sealed trait TenOneTree {
  def postOrder(f: BalanceFn): (Boolean, Int)
}
final case class TenOneNode(data: Int, left: TenOneTree, right: TenOneTree) extends TenOneTree {
  override def postOrder(f: BalanceFn): (Boolean, Int) = {
    val leftResult = left.postOrder(f)
    if (leftResult._1) f(leftResult, right.postOrder(f)) else leftResult
  }
}
final case class TenOneLeaf() extends TenOneTree {
  override def postOrder(f: BalanceFn): (Boolean, Int) = (true, -1)
}

object TenOneTree {
  type BalancedStatus = (Boolean, Int)
  type BalanceFn = (BalancedStatus, BalancedStatus) => BalancedStatus

  def isHeightBalanced(tree: TenOneTree): Boolean = {
    val result = tree.postOrder { (left: BalancedStatus, right: BalancedStatus) =>
      val isBalanced = Math.abs(left._2 - right._2) <= 1
      val height = Math.max(left._2, right._2) + 1
      (isBalanced, height)
    }
    result._1
  }

}
