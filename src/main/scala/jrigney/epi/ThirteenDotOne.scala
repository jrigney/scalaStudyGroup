package jrigney.epi

/**
 * constraints
 *   palindrome is a word read backward & forward
 *
 * examples
 * a => true
 * bba => true
 * bbcb => false
 * bc => false
 *
 * brute force
 *   get permutation of string and check each is palindrome
 *
 * brute force bigo
 *   permutation n!
 *   check each palindrome n!
 *
 *   n! + n!
 *
 * optimized
 *   for odd lengths strings chars should group in even blocks with one char odd
 *   for even length strings chars should group evenly
 *
 *   go through string and group chars
 *   go through groups count number of odd groups
 *     if number of odd groups > 1 then not palindromeable
 *
 * optimized bigo
 *   go through string and group chars = c
 *   go through groups count number of odd groups = c for worst case
 *
 *   2c ~> c
 */

object ThirteenDotOne {
  def palindromeable_?(in: String): Boolean = {
    val grouped = in.groupBy(c => c)
    val oddCount = grouped.foldLeft(0) { (acc, nextGroup) =>
      val (k, v) = nextGroup
      if ((v.length % 2) != 0) acc + 1 else acc
    }
    !(oddCount > 1)
  }
}
