package jrigney.epi

object ElevenDotOne {
  implicit object AscendingOrder extends Ordering[Int] {
    def compare(x: Int, y: Int) = {
      //      println(s"x $x y $y comp ${y.compare(x)}") /*REMOVEME*/
      y.compare(x)
    }
  }

  val x = scala.collection.mutable.PriorityQueue(7, 3)(AscendingOrder)

  def merge(lists: List[List[Int]]): List[Int] = {
    import scala.collection.mutable.PriorityQueue

    def loop(lists: List[List[Int]], q: PriorityQueue[Int]): PriorityQueue[Int] = {
      lists match {
        case Nil => q
        case Nil :: t => loop(t, q)
        case (h :: Nil) :: remainingLists =>
          q.enqueue(h)
          loop(remainingLists, q)
        //remainingLists :: restHeadList because we want to get the minimums for the rest of the lists
        case (h :: restHeadList) :: remainingLists =>
          q.enqueue(h)
          loop(remainingLists :+ restHeadList, q)
      }
    }

    loop(lists, PriorityQueue.empty[Int](AscendingOrder)).dequeueAll
  }
}
