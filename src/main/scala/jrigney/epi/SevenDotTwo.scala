package jrigney.epi

import scala.annotation.tailrec

object SevenDotTwo {
  def baseConversion(num: String, base1: Int, base2: Int): String = {
    val asBase10 = num.foldLeft(0) { (acc, c) =>
      val encodedDigit = if (c.isDigit) {
        c - '0'
      } else {
        c - 'A' + 10
      }
      acc * base1 + encodedDigit
    }

    @tailrec
    def loop(remaining: Int, acc: List[Char]): List[Char] = {
      def encoded(num: Int): Char = {
        if (num < 10) ('0' + num % base2).toChar
        else ('A' + num - 10 % base2).toChar
      }

      remaining match {
        case 0 => acc
        case _ =>
          val x = remaining % base2
          loop(remaining / base2, encoded(x) +: acc)
      }

    }

    loop(asBase10, List.empty[Char]).mkString
  }
}
