package jrigney.epi

/**
 * constraints
 *   how many tasks
 *   how many tasks per worker
 *   what is optimal
 *
 * bruteforce
 *   sort tasks
 *   take first and last element to make smallest possible
 *   loop reducing size of vector
 *
 * bruteforce bigo
 *   sort tasks nlogn
 *   take firt & last 1
 *   loop n/2
 * n/2 (nlogn)
 *
 */
object EighteenDotOne {

  def optimumTaskAssignment(tasks: List[Int]): List[(Int, Int)] = {
    def loop(sortedTasks: List[Int], optimalTasks: List[(Int, Int)]): List[(Int, Int)] = {
      sortedTasks match {
        case Nil => optimalTasks
        case h :: rest =>
          val last = rest.last
          loop(rest.init, (h, last) +: optimalTasks)
      }
    }

    val sortedTasks = tasks.sorted
    if (sortedTasks.size % 2 == 1) {
      val max = sortedTasks.last
      loop(sortedTasks.init, List((max, 0)))
    } else {
      loop(sortedTasks, List.empty[(Int, Int)])
    }
  }
}
