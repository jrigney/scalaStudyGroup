package jrigney.epi

import scala.collection.immutable.Queue

object NineteenDotTwo extends App {

  type Grid = Vector[Vector[Color.EnumVal]]

  def locationsToFlip(grid: Grid, start: Vertex): List[Vertex] = {

    val startColor = grid(start.row)(start.col)

    def loop(fringe: Queue[Vertex], result: List[Vertex], visited: List[Vertex]): List[Vertex] = {

      def adjacentVertex(vertex: Vertex): Vector[Vertex] = {
        val shift = Vector((1, 0), (-1, 0), (0, 1), (0, -1))
        shift.map(s => Vertex(vertex.row + s._1, vertex.col + s._2))
      }

      def inGrid_?(vertex: Vertex): Boolean = {
        val validRow = vertex.row >= 0 && vertex.row < grid(0).size
        val validCol = vertex.col >= 0 && vertex.col < grid.size

        validRow && validCol
      }

      def sameColor(vertex: Vertex, startColor: Color.EnumVal): Boolean = {
        val locationColor = grid(vertex.row)(vertex.col)
        locationColor == startColor
      }

      //bfs so queue
      fringe match {
        case q if q.isEmpty => result.reverse
        case head +: rest =>

          val potentialNeighbors = adjacentVertex(head).filter(inGrid_?).filter(v => sameColor(v, startColor))

          val newNeighbors = potentialNeighbors.filterNot(visited.contains)

          val newResult = head +: result
          val newVisited = head +: visited

          //          println(s"f $fringe r $result v $visited p $potentialNeighbors n $newNeighbors")
          if (newNeighbors.isEmpty) loop(rest, newResult, newVisited)
          else loop(rest ++ newNeighbors, newResult, newVisited)
      }
    }

    loop(Queue(start), List.empty, List.empty)
  }

  object Color {

    sealed trait EnumVal

    case object White extends EnumVal

    case object Black extends EnumVal

  }

  case class Vertex(row: Int, col: Int)

}

