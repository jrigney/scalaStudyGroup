package jrigney.epi
/**
 * Search sorted array for first occurrence of k
 * -14, -10, 2, 108, 108, 243, 285, 285, 285, 401
 * Write a method that takes a sorted array and a key and returns the index of the first occurrence of
 * that key in the array.  For example, when applied to the array in Figure 8.1 your algorithm should
 * return 3 if the given key is 108; if it is 285, your algorithm should return 6.
 */

object EightDotOne extends App {
  val sortedSeq = Vector(-14, -10, 2, 108, 108, 243, 285, 285, 285, 401)

  import scala.collection.Searching._
  val findThis = 108
  @scala.annotation.tailrec
  def loop(rest: Vector[Int], index: Option[Int]): Option[Int] = {
    println(s"$rest, $index")
    rest match {
      case rest if rest.isEmpty => index
      case rest =>
        rest.search(findThis) match {
          case Found(newIndex) =>
            val (lessThan, greaterThan) = rest.splitAt(newIndex)
            println(s"\t $lessThan $greaterThan")
            loop(lessThan, Some(newIndex))
          case _ => loop(Vector.empty[Int], index)
        }
    }
  }
  val x = loop(sortedSeq, None)
  println(x)
}
