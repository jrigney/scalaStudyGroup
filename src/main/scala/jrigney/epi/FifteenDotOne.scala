package jrigney.epi

sealed trait FifteenOneTree {
  def bst_?(): Boolean
  def isBst(): (Boolean, Int)
}

final case class FifteenOneNode(v: Int, left: FifteenOneTree, right: FifteenOneTree) extends FifteenOneTree {
  override def bst_? : Boolean = {
    isBst._1
  }

  override def isBst(): (Boolean, Int) = {
    (left, right) match {
      case (FifteenOneLeaf(), FifteenOneLeaf()) => (true, v)
      case (left, FifteenOneLeaf()) =>
        val (isBst, leftValue) = left.isBst()
        if (isBst && leftValue <= v) (true, v) else (false, v)
      case (FifteenOneLeaf(), right) =>
        val (isBst, rightValue) = right.isBst()
        if (isBst && v <= rightValue) (true, v) else (false, v)
      case (left, right) =>
        val (leftBst, leftValue) = left.isBst()
        if (leftBst == true && leftValue <= v) {
          val (rightBst, rightValue) = right.isBst()
          if (rightBst == true && v <= rightValue) (true, v) else (false, v)
        } else (false, v)
    }
  }
}
final case class FifteenOneLeaf() extends FifteenOneTree {
  override def bst_?(): Boolean = true
  override def isBst(): (Boolean, Int) = (true, 0)
}

