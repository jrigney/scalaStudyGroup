package jrigney.epi

import scala.annotation.tailrec

object NineteenDotOne extends App {

  def adjacentVertices(map: Vector[Vector[Int]], rowUpperBoundary: Int, colUpperBoundary: Int)(vertex: Vertex): Set[Vertex] = {
    val north = if (vertex.col - 1 >= 0) Some(Vertex(vertex.row, vertex.col - 1)) else None
    val south = if (vertex.col + 1 <= colUpperBoundary) Some(Vertex(vertex.row, vertex.col + 1)) else None
    val east = if (vertex.row + 1 <= rowUpperBoundary) Some(Vertex(vertex.row + 1, vertex.col)) else None
    val west = if (vertex.row - 1 >= 0) Some(Vertex(vertex.row - 1, vertex.col)) else None

    Set(north, south, east, west).collect {
      case Some(n) if map(n.row)(n.col) == 1 => n
    }
  }

  def path(map: Vector[Vector[Int]], start: Vertex, end: Vertex): Option[Path] = {

    @tailrec
    def loop(discoveredCostAscending: List[Path], processed: Set[Vertex],
      end: Vertex, adjacentVertices: Vertex => Set[Vertex]): Option[Path] = {
      discoveredCostAscending match {
        case Nil => None
        case (Path(costToStart, breadcrumbs)) :: restOfPaths =>
          val currentPathEnd = breadcrumbs.head
          if (currentPathEnd == end) Option(Path(costToStart, breadcrumbs.reverse)) //we found a path from start to end
          else {
            val pathsToAdjacentVertexToStart: List[Path] = adjacentVertices(currentPathEnd).flatMap { adjVertex =>
              if (!processed.contains(adjVertex))
                //add the adjacentVertex to the breadcrumbs
                List(Path(costToStart + 1, adjVertex :: breadcrumbs))
              else
                //since we have already processed/seen this adjacentVertex we can ignore it
                List.empty[Path]
            }.toList

            val sortedDiscovered = (pathsToAdjacentVertexToStart ++ restOfPaths).sortWith {
              case (Path(costToStart1, _), Path(costToStart2, _)) => costToStart1 < costToStart2
            }
            loop(sortedDiscovered, processed + currentPathEnd, end, adjacentVertices)
          }
      }
    }

    val rowUpperBoundary = map.length - 1
    val colUpperBoundary = map.length - 1
    val adjVertFn = adjacentVertices(map, rowUpperBoundary, colUpperBoundary) _

    loop(List(Path(0, List(start))), Set.empty[Vertex], end, adjVertFn)
  }

  ////////////////////////////////////////////////////////////////////////
  /**
   * The 2D matrix is no the same as an adjacency matrix.  An adjacency matrix axes are all the vertices.
   * The vertices in the 2D matrix is (row,column)
   *
   * Create the types first to best model the domain
   * Practice value types
   * Practice augmented types to add functions to value types
   * What really is the graph model?  The map is not an adjacency matrix.
   * How do you avoid off by one errors and array out of bounds?
   * If there are no new neighbors then we are at the end of a path.
   * If we are at the end of the path and it is not the end we are looking for
   * remove the last segment.  The stack will keep track how to continue.
   * The stack keeps track of the potential paths by keeping the newNeighbors
   */

  type Map = Vector[Vector[Color.EnumVal]]

  def path2(map: Vector[Vector[Color.EnumVal]], start: Vertex, end: Vertex): Option[Path] = {
    //depth first search
    //not shortest path
    @tailrec
    def loop(fringe: List[Vertex], result: Path, visited: Vector[Vertex]): Path = {

      fringe match {
        case Nil => result
        case head +: rest =>
          if (head == end) {
            val Path(c, vs) = result
            Path(c + 1, vs :+ head)
          } else {
            val potentialNeighbors: Vector[Vertex] = adjacentVertices(head).filter(inMap_?).filter(white_?)
            val newNeighbors: Vector[Vertex] = potentialNeighbors.filterNot(visited.contains)

            val newResult = {
              val Path(c, vs) = result
              Path(c + 1, vs :+ head)
            }
            //            println(s"f $fringe r $result v $visited pN $potentialNeighbors nN $newNeighbors nR $newResult")
            if (newNeighbors.isEmpty) {
              loop(rest, result, head +: visited)
            } else loop(newNeighbors.toList ++ rest, newResult, head +: visited)
          }
      }
    }

    def adjacentVertices(home: Vertex): Vector[Vertex] = {
      val shifts = Vector((-1, 0), (0, -1), (1, 0), (0, 1))
      shifts.map { case (r, c) => Vertex(home.row + r, home.col + c) }
    }

    def white_?(v: Vertex): Boolean = {
      map(v.row)(v.col) == Color.White
    }

    def inMap_?(v: Vertex): Boolean = {
      val Vertex(row, col) = v
      val validRow = row >= 0 && row < map(0).size
      val validCol = col >= 0 && col < map.size
      validRow && validCol
    }

    //depth first search so stack
    val x = loop(List(start), Path(0, List.empty), Vector.empty)
    Some(x)
  }

  object Color {

    sealed trait EnumVal

    case object Black extends EnumVal

    case object White extends EnumVal

  }

  case class Vertex private (row: Int, col: Int)

  case class Path(costToStart: Int, breadcrumbs: List[Vertex])
}

//class Column(val underlying: Int) extends AnyVal {
//  def foo: Column = new Column(underlying)
//  def + (x: Int): Column = new Column(this.underlying + x)
//}
//class Row(val underlying: Int) extends AnyVal {
//  def foo: Row = new Row(underlying)
//  def + (x: Int): Row = new Row(this.underlying + x)
//}

