package jrigney.epi

/**
 * constraints
 * inputs can have duplicates
 * inputs are sorted
 *
 * brute force
 * put both arrays into hashset
 * fold one hashset compare contains against other hash set
 * if equal then put in hashset
 *
 * brute force bigo
 * put both arrays into hashset => s1 + s2
 * fold smaller hashset compare contains against other hash set => s1
 * if equal then put in hashset => s1
 *
 * s1 + s2 + s1 + s1 ~ 3s1 + s2 ~ s1 + s2
 *
 *
 */

object FourteenDotOne {
  def intersection(alpha: Array[Int], bravo: Array[Int]): Array[Int] = {
    import scala.collection.immutable.HashSet

    def intersect(hashed: HashSet[Int], anArray: Array[Int]): HashSet[Int] = {
      anArray.foldLeft(HashSet.empty[Int]) { (acc, next) =>
        if (hashed.contains(next)) acc + next else acc
      }
    }
    val intersected = (alpha.size > bravo.size) match {
      case true => intersect(HashSet(alpha: _*), bravo)
      case false => intersect(HashSet(bravo: _*), alpha)
    }

    intersected.toArray
  }

  def evenIntersection(alpha: Array[Int], bravo: Array[Int]): Array[Int] = {
    @scala.annotation.tailrec
    def loop(alphaRest: Array[Int], bravoRest: Array[Int], intersection: Array[Int]): Array[Int] = {

      println(s"${alphaRest.mkString(":")}, ${bravoRest.mkString(":")} ${intersection.mkString(":")}")
      (alphaRest, bravoRest) match {
        case (a, _) if a.isEmpty => intersection
        case (_, b) if b.isEmpty => intersection
        case (Array(ha, ta @ _*), b @ Array(hb, tb @ _*)) if ha < hb => loop(ta.toArray, b, intersection)
        case (a @ Array(ha, ta @ _*), Array(hb, tb @ _*)) if ha > hb => loop(a, tb.toArray, intersection)
        case (a @ Array(ha, ta @ _*), b @ Array(hb, tb @ _*)) if ha == hb => loop(ta.toArray, tb.toArray, ha +: intersection)
      }
    }
    loop(alpha, bravo, Array.empty[Int])
  }
}
