package jrigney.epi

import scala.collection.immutable.HashMap

//Tower of Hanoi
/**
 *  move stack from t1 to t2
 *
 *     t1              t2              t3
 *     1
 *                                     1
 *   ========================================
 *    1,2
 *    2                                1
 *                     2               1
 *                     1,2
 *   ========================================
 *   1,2,3
 *                                     1,2      -- we already know how to move a stack of two. instead of moving it to t2
 *                                                 move it to a buffer (t3)
 *                     3               1,2      -- similar to a stack of one we move 3 to the destination
 *                     1,2,3                    -- again we know how to move the stack of two. so we move the stack of 2
 *                                                 to the destination (t2)
 *   1,2,3,4
 *                                     1,2,3    -- we already know how to move the stack of 3. so move it to the buffer t3
 *                     4               1,2,3    --similar to a stack of one we move 4 to the destination (t2)
 *                     1,2,3,4                  -- again we know how to move the stack of two. so we move the stack of 3
 *                                                 to the destination
 *
 *   we are starting to see a pattern here were n-1 is moved to the buffer, n is moved to the destination, then
 *   n-1 is moved to the destination.
 */
object SixteenDotOne extends App {

  //show movement from src to sink
  //using standard recusion

  def moveDisks(n: Int, from: Int, to: Int, via: Int): Unit = {
    //    println(s"n $n from $from to $to via $via") /*REMOVEME*/
    if (n == 1) {
      //      Console.println(s"\tMove disk from pole " + from + " to pole " + to) /*REMOVEME*/
    } else {
      moveDisks(n - 1, from, via, to)
      moveDisks(1, from, to, via)
      moveDisks(n - 1, via, to, from)
    }
  }

  moveDisks(3, 1, 2, 3)

}
