package jrigney.algorithms

import scala.annotation.tailrec
import scala.collection.immutable.HashMap

//Create an unweighted AdjacencyListGraph containing
//0 -> 1, 2
//1 -> 2
//2 -> 0, 3
//3 -> 3
//
// use a hashmap
// use a list of list

object UnweightedAdjacencyListGraph extends App {
  type HashMapGraph[Vertex] = HashMap[Vertex, Vector[Vertex]]

  val hashmapGraph: HashMapGraph[Int] = HashMap[Int, Vector[Int]](
    (0 -> Vector[Int](1, 2)),
    (1 -> Vector[Int](2)),
    (2 -> Vector[Int](0, 3)),
    (3 -> Vector[Int](3)))

  type AdjacencyMatrixGraph[Vertex] = Vector[Vector[Vertex]]

  val listListGraph: AdjacencyMatrixGraph[Int] = Vector(
    Vector(1, 2),
    Vector(2),
    Vector(0, 3),
    Vector(3))

  //Write a function to print the breadth first traversal for a graph.

  def breadthFirstSearch[V](startVertex: V, graph: HashMap[V, Vector[V]]): List[V] = {

    def loop(fringe: List[V], result: List[V], visited: List[V]): List[V] = {
      fringe match {
        case Nil => result.reverse
        case head :: rest =>
          val potentialNeighbors = fringe.flatMap(graph)
          val unvisitedNeighbors = potentialNeighbors.filterNot(visited.contains)

          val newResult = if (visited.contains(head)) result else head +: result
          if (unvisitedNeighbors.isEmpty) loop(rest, newResult, head +: visited)
          else loop(rest ++ unvisitedNeighbors, newResult, head +: visited)
      }
    }

    loop(List(startVertex), List.empty, List.empty)
  }

  //Write a function to print out only x % 2 == 0 for a graph.
  def bfsMod2[V](startVertex: V, graph: HashMap[V, Vector[V]], filterFn: V => Boolean): List[V] = {

    def loop(fringe: List[V], result: List[V], visited: List[V], filterFn: V => Boolean): List[V] = {

      val fringeResult = fringe.filter(filterFn)
      val potentialNeighbors = fringe.flatMap(v => graph(v))
      val newNeighbors = potentialNeighbors.filterNot(visited.contains)

      if (newNeighbors.isEmpty) result ++ fringeResult
      else loop(newNeighbors, result ++ fringeResult, newNeighbors ++ visited, filterFn)
    }

    loop(List(startVertex), List.empty, List(startVertex), filterFn)
  }

  // Write a depth first search using adjacency list
  def depthFirstSearch[V](startVertex: V, graph: HashMap[V, Vector[V]]): List[V] = {
    // stack

    @tailrec
    def loop(fringe: List[V], result: List[V], visited: List[V]): List[V] = {
      //      println(s"fringe $fringe, result $result, visited $visited")

      fringe match {
        case Nil => result.reverse
        case current :: rest =>
          val potentialNeighbors = graph(current).toList
          val newNeighbors = potentialNeighbors.filterNot(visited.contains)
          val newResult = if (visited.contains(current)) result else current +: result
          loop(newNeighbors ++ rest, newResult, current +: result)
      }
    }

    loop(List(startVertex), List.empty, List.empty)
  }

  //Write a depth first search where only x % 2 = 0 is returned

  def dfsFiltered[V](startVertex: V, graph: HashMap[V, Vector[V]], fn: V => Boolean): List[V] = {
    // stack

    @tailrec
    def loop(fringe: List[V], result: List[V], visited: List[V], fn: V => Boolean): List[V] = {
      //      println(s"fringe $fringe, result $result, visited $visited")

      fringe match {
        case Nil => result.reverse
        case current :: rest =>
          val potentialNeighbors = graph(current).toList
          val newNeighbors = potentialNeighbors.filterNot(visited.contains)
          val newResult = (if (visited.contains(current)) result else current +: result).filter(fn)
          loop(newNeighbors ++ rest, newResult, current +: result, fn)
      }
    }

    loop(List(startVertex), List.empty, List.empty, fn)
  }

  //Write a breadth first search using adjacency matrix
  def breadthFirstSearch(startVertex: Int, graph: AdjacencyMatrixGraph[Int]): List[Int] = {

    def loop(fringe: List[Int], result: List[Int], visited: List[Int]): List[Int] = {
      fringe match {
        case Nil => result.reverse
        case head :: rest =>
          val potentialNeighbors = graph(head).zipWithIndex.filter { case (v, _) => v == 1 }.map { _._2 }
          val newNeighbors = potentialNeighbors.filterNot(visited.contains)

          val newResult = if (visited.contains(head)) result else head +: result

          //          println(s"f $fringe r $result v $visited pN $potentialNeighbors n $newNeighbors")
          if (newNeighbors.isEmpty) loop(rest, newResult, head +: visited)
          else loop(rest ++ newNeighbors, newResult, head +: visited)
      }
    }

    loop(List(startVertex), List.empty, List.empty)

  }

  //Write a depth first search using an adjacency matrix
  def depthFirstSearch(startVertex: Int, graph: AdjacencyMatrixGraph[Int]): List[Int] = {

    //dfs uses a stack to keep track of where to go next
    @tailrec
    def loop(fringe: List[Int], result: List[Int], visited: List[Int]): List[Int] = {
      fringe match {
        case Nil => result.reverse
        case head :: rest =>
          val potentialNeighbors = graph(head).zipWithIndex.filter { case (v, _) => v == 1 }.map { _._2 }
          val newNeighbors = potentialNeighbors.filterNot(visited.contains)
          if (newNeighbors.isEmpty) loop(rest, head +: result, head +: visited)
          else loop(newNeighbors.toList ++ rest, head +: result, head +: visited)
      }
    }

    loop(List(startVertex), List.empty, List.empty)
  }
}

