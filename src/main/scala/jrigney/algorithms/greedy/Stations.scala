package jrigney.algorithms.greedy

object Stations extends App {

  type Station = String
  type State = String
  val statesNeeded = Set("mt", "wa", "or", "id", "nv", "ut")
  val stations: Map[Station, Set[State]] = Map(
    ("kone", Set("id", "nv", "ut")),
    ("ktwo", Set("wa", "id", "mt")),
    ("kthree", Set("or", "nv", "ca")),
    ("kfour", Set("nv", "ut")),
    ("kfive", Set("ca", "az")))

  def minStations(): Set[Station] = {
    def loop(remainingNeeded: Set[State], stationsIncluded: Set[Station]): Set[Station] = {
      remainingNeeded match {
        case x if x.isEmpty => stationsIncluded
        case x =>
          val (stateAdditions, stationAddition) = stations.foldLeft((Set.empty[State], "")) {
            case ((statesCovered, station), (testStation, testStates)) =>
              val newStatesCovered = remainingNeeded.intersect(testStates)
              if (newStatesCovered.size > statesCovered.size) (newStatesCovered, testStation)
              else (statesCovered, station)
          }

          val rest = remainingNeeded.filterNot(stateAdditions.contains)

          loop(rest, stationsIncluded + stationAddition)
      }
    }

    loop(statesNeeded, Set.empty[Station])
  }

  println(s"minStation ${minStations()}")
}
