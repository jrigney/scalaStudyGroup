package jrigney.algorithms

import jrigney.algorithms.Graph._

object GraphFactory {

  def buildWeightedAdjacencyList[V, W, E <: WeightedEdge[V, W]](graph: WeightedGraph[V, W, E]): WeightedAdjacencyList[V, W] = {
    import scala.collection.breakOut

    val vertices = graph.vertices
    val edges = graph.edges

    val initialMap: WeightedAdjacencyList[V, W] = vertices.map(v => (v -> List.empty[WeightedAdjacentVertex[V, W]]))(breakOut)

    edges.foldLeft(initialMap) { (weightedAdjacencyList, next) =>

      def addAdjacentVetexToAdjacencyMatrix(
        vertex: V,
        adjacentVertex: V)(adjancencyList: WeightedAdjacencyList[V, W]): WeightedAdjacencyList[V, W] = {

        val maybeAdjacentToVertex = adjancencyList.get(vertex)
        maybeAdjacentToVertex.fold(adjancencyList updated (vertex, List(WeightedAdjacentVertex(adjacentVertex, next.weight)))) { es =>
          adjancencyList.updated(vertex, es :+ WeightedAdjacentVertex(adjacentVertex, next.weight))
        }
      }

      val startToEndAdjList = addAdjacentVetexToAdjacencyMatrix(next.startVertex, next.endVertex) _
      val endToStartAdjList = addAdjacentVetexToAdjacencyMatrix(next.endVertex, next.startVertex) _

      val undirectAdjacencyList = endToStartAdjList compose startToEndAdjList
      undirectAdjacencyList(weightedAdjacencyList)
    }
  }
}
