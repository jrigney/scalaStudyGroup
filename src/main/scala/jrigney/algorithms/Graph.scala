package jrigney.algorithms

import scala.annotation.tailrec
import scala.collection.immutable.HashSet
import scala.collection.immutable.HashMap

object Graph {

  def breadthFirstSearch[V](startVertex: V, graph: HashMap[V, List[V]]): Vector[Vector[V]] = {
    val visited = HashSet.empty[V]
    val result = Vector(Vector(startVertex))

    @tailrec
    def bfs(fringe: Vector[V], result: Vector[Vector[V]], visited: HashSet[V]): Vector[Vector[V]] = {
      val potentialNeighbors = fringe.flatMap(graph)
      val newNeighbors = potentialNeighbors.filterNot {
        visited contains
      }
      if (newNeighbors.isEmpty) result //no other nodes so return results
      else bfs(newNeighbors, newNeighbors +: result, visited ++ newNeighbors)
    }

    bfs(Vector(startVertex), result, visited).reverse
  }

  def shortestPath[Vertex, Weight](start: Vertex, end: Vertex, graph: WeightedAdjacencyList[Vertex, Weight])(implicit numeric: Numeric[Weight], ordering: Ordering[Weight]): Path[Vertex, Weight] = {
    //general idea - in a loop grab the vertex(minVertex) that provides the minimum cost path to the start until you reach the destination
    @tailrec
    def shortestPathLoop(
      fringeSortedCostToStart: List[Path[Vertex, Weight]], // all paths we currently have a temp distance for sorted by cost ascending
      graph: WeightedAdjacencyList[Vertex, Weight],
      dest: Vertex,
      visited: Set[Vertex]): Path[Vertex, Weight] =
      fringeSortedCostToStart match {
        case (Path(costFromStart, path)) :: fringe_rest => //choose the vertex that  minimimizes the cost from start to chosen Vertex
          val currentEndOfPath = path.head
          if (currentEndOfPath == dest) Path(costFromStart, path.reverse) // if the chosen vertex == dest then return the path for the chosen vertex
          else {
            val pathsToAdjacentVertexToStart: List[Path[Vertex, Weight]] = {
              val verticesAdjacentToEndOfPath = graph(currentEndOfPath) //else get the adjacent vertices
              verticesAdjacentToEndOfPath.flatMap {
                //loop through the adjacent vertices
                case (WeightedAdjacentVertex(vertexAdjacentToEnd, costEndOfPathVertexToAdjacentVertex)) =>
                  //if the adjacentVertex has not been visited, update the path with the new cost and add the adjacent vertex to the path.
                  //this is the path and cost of the adjacent vertex to the start
                  if (!visited.contains(vertexAdjacentToEnd))
                    List(Path(numeric.plus(costFromStart, costEndOfPathVertexToAdjacentVertex), vertexAdjacentToEnd :: path))
                  else List.empty[Path[Vertex, Weight]]
              }
            }
            val sorted_fringe = (pathsToAdjacentVertexToStart ++ fringe_rest).sortWith {
              //sort the paths in ascending cost
              case (Path(distanceToStart1, _), Path(distanceToStart2, _)) => ordering.lt(distanceToStart1, distanceToStart2)
            }
            shortestPathLoop(sorted_fringe, graph, dest, visited + currentEndOfPath)
          }
        case Nil => Path(numeric.zero, List())
      }

    shortestPathLoop(List(Path(numeric.zero, List(start))), graph, end, Set(start))
  }

  @tailrec
  def minimumCosts[V](
    fringeDiscovered: List[(V, Double)],
    graph: Map[V, List[(V, Double)]],
    currentlyProcessedVertexCosts: Map[V, Double]): Map[V, Double] = {
    if (fringeDiscovered.isEmpty) currentlyProcessedVertexCosts
    else {
      //take a fringe vertex that has a the minimum cost to the start
      val (minCostToStartVertex, costToStart) = fringeDiscovered.minBy { case (vector, cost) if currentlyProcessedVertexCosts.contains(vector) => cost }
      val adjacentToFringe = for {
        (adjacentVertex, costMinVertexToAdjacent) <- graph(minCostToStartVertex) //get adjacent vertices to the minCostVertex
        adjacentVertexCurrentCostToStart = currentlyProcessedVertexCosts.getOrElse(adjacentVertex, Double.MaxValue) //get the cost of the adjacent vertex to the minCostVertex
        if costToStart + costMinVertexToAdjacent < adjacentVertexCurrentCostToStart //filter the vertices that have a cost that is less that the current cost
      } yield adjacentVertex -> (costToStart + costMinVertexToAdjacent)
      val fringeSansMinVertex = fringeDiscovered.filterNot { case (v, _) => v == minCostToStartVertex }
      val newlyProcessedVertices = fringeSansMinVertex ++ adjacentToFringe
      minimumCosts(newlyProcessedVertices, graph, currentlyProcessedVertexCosts ++ newlyProcessedVertices)
    }
  }

  import scala.collection.immutable.HashSet

  def shortestPaths[V, W: Numeric](startVertex: V, destinationVertex: V, graph: WeightedAdjacencyList[V, W]): Map[V, Path[V, W]] = {

    def shortestPathsLoop(fringeVertices: List[Path[V, W]], result: Map[V, Path[V, W]], processed: HashSet[V]): Map[V, Path[V, W]] = {
      //      println(s"\n\n${fringeVertices}:fringeVertices  ${result}:result ${processed}:processed") /*REMOVEME*/
      if (fringeVertices.isEmpty) result
      else {
        val currentMinPath = fringeVertices.head
        currentMinPath match {
          case Path(costToStart, fullPath @ currentVertex :: restOfPath) =>
            val unprocessedAdjacentVertices = graph(currentVertex).filterNot { case WeightedAdjacentVertex(v, w) => processed contains v }
            val newPathsWithAdj = unprocessedAdjacentVertices.map {
              case WeightedAdjacentVertex(adjacentVertex, costFringeVertexToAdjacent) =>
                Path(implicitly[Numeric[W]].plus(costToStart, costFringeVertexToAdjacent), adjacentVertex +: fullPath)
            }
            //            println(s"${newPathsWithAdj}:newPathsWithAdj") /*REMOVEME*/
            val newProcessed = processed + currentVertex

            val newResult = currentMinPath.path.headOption.fold(result) { vertex =>
              val maybeVertex = result get vertex
              maybeVertex.fold(result + (currentVertex -> currentMinPath)) { _ => result }
            }

            val newFringe = (newPathsWithAdj ++ fringeVertices.tail).sortWith {
              case (Path(cost1, _), Path(cost2, _)) => implicitly[Numeric[W]].lt(cost1, cost2)
            }

            shortestPathsLoop(newFringe, newResult, newProcessed)
        }
      }
    }
    val startPath = Path(implicitly[Numeric[W]].zero, List(startVertex))
    shortestPathsLoop(List(startPath), Map((startVertex -> startPath)), HashSet(startVertex))
  }
  // undiscovered - vertex in virgin state
  // discovered - vertex found but not checked out
  // processed - vertex after we have visited all incident edges

  case class Path[V, W](costToStart: W, path: List[V])

  sealed trait Edge[V] {
    def startVertex: V

    def endVertex: V
  }
  final case class UnweightedEdge[V](startVertex: V, endVertex: V) extends Edge[V]
  final case class WeightedEdge[V, W](startVertex: V, endVertex: V, weight: W) extends Edge[V]

  sealed trait AdjacentVertex[V] {
    def vertex: V
  }
  final case class UnweightedAdjacentVertex[V](vertex: V) extends AdjacentVertex[V]
  final case class WeightedAdjacentVertex[V, W](vertex: V, weight: W) extends AdjacentVertex[V]

  sealed trait Graph[V, E] {
    def vertices: Set[V]
    def edges: Vector[E]
  }
  final case class WeightedGraph[V, W, E <: WeightedEdge[V, W]](vertices: Set[V], edges: Vector[E]) extends Graph[V, E]

  type WeightedAdjacencyList[Vertex, Weight] = HashMap[Vertex, List[WeightedAdjacentVertex[Vertex, Weight]]]
  //  type Path[Key] = (Double, List[Key])
}

