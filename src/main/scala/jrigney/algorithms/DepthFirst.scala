package jrigney.algorithms

import scala.annotation.tailrec
import scala.collection.immutable.HashMap

object DepthFirstSearch extends App {

  def dfs(
    graph: HashMap[Int, List[Int]],
    vertex: Int): Vector[Int] = {

    @tailrec
    def loop(remaining: List[Int], visited: Vector[Int]): Vector[Int] = {

      remaining match {
        case Nil => visited
        case h :: t =>
          val neighbors = graph(h).filterNot(visited.contains)
          loop(neighbors ++ t, h +: visited)
      }
    }
    dfs(graph, 1)
  }

  //http://manuel.kiessling.net/2016/02/15/scala-traversing-a-graph-in-a-functional-way/
}

