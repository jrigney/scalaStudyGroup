package jrigney.algorithms

import scala.collection.immutable.HashMap

object Recursion extends App {

  val a1 = Vector(2, 4, 6, 10)

  //given an array of numbers find the number of subsets that sum to a total
  def countSets(s: Vector[Int], theTotal: Int): Int = {

    def withHead(is: Vector[Int], total: Int) = {
      println(s"\t withHead is $is total $total")
      loop(is, total)
    }
    def sansHead(is: Vector[Int], total: Int, head: Int) = {
      println(s"\t sansHead is $is total $total head $head")
      loop(is, total - head)
    }
    def loop(rest: Vector[Int], total: Int): Int = {
      println(s"rest $rest total $total")
      if (total == 0) 1
      else if (total < 0) 0
      else rest match {
        case h +: t if total < h => loop(t, total)
        case h if h.isEmpty => 0
        case h +: t =>
          sansHead(t, total, h) + withHead(t, total)
      }
    }

    loop(s, theTotal)
  }

  println(s" countSets ${countSets(a1, 16)}")

  def countSetsMemoized(s: Vector[Int], theTotal: Int): Int = {

    //memoize the calculations of the number of sets for a total an value in the vector
    def loop(rest: Vector[Int], total: Int, memo: collection.mutable.HashMap[String, Int]): Int = {
      if (total == 0) 1
      else if (total < 0) 0
      else rest match {
        case h if h.isEmpty => 0
        case h +: t if total < h =>
          val k = total.toString + h.toString
          memo.get(k).getOrElse {
            val r = loop(t, total, memo)
            memo.update(k, r)
            r
          }
        case h +: t =>
          val k = total.toString + h.toString
          val r = loop(t, total - h, memo) + loop(t, total, memo)
          memo.update(k, r)
          r
      }
    }

    loop(s, theTotal, collection.mutable.HashMap.empty)
  }

  println(s" countSetsMemoized ${countSetsMemoized(a1, 16)}")

  def countSetsMemoTrait(s: Vector[Int], theTotal: Int): Int = {

    def loop(rest: Vector[Int], total: Int): Int = {
      if (total == 0) 1
      else if (total < 0) 0
      else rest match {
        case h +: t if total < h => loop(t, total)
        case h if h.isEmpty => 0
        case h +: t => loop(t, total - h) + loop(t, total)
      }
    }

    val run = Memoize2(loop _)((x: Vector[Int], y: Int) => s"$x:$y")
    run(s, theTotal)
  }

  println(s" countSetsMemoTrait ${countSetsMemoTrait(a1, 16)}")

  def recMemo() = {
    def facRec(n: BigInt, f: BigInt => BigInt): BigInt = {
      if (n == 0) 1
      else n * f(n - 1)
    }

    val fac = Memoize1.Y(facRec)
    for (k <- 10 to 0 by -1) {
      println(fac(k))
    }
  }

  recMemo()
}

class Memoize2[-T2, -T1, +R, K](f: (T1, T2) => R)(keyFn: (T1, T2) => K) extends ((T1, T2) => R) {

  import scala.collection.mutable

  private[this] val vals = mutable.Map.empty[K, R]

  override def apply(v1: T1, v2: T2): R = {
    val x = keyFn(v1, v2)
    if (vals.contains(x)) {
      vals(x)
    } else {
      val y = f(v1, v2)
      vals += ((x, y))
      y
    }
  }
}

object Memoize2 {
  def apply[T1, T2, R, K](f: (T1, T2) => R)(keyFn: (T1, T2) => K) = new Memoize2(f)(keyFn)

  //  def Y[T1, T2, R](f: (T, T => R) => R) = {
  //    var yf: T => R = null
  //    yf = Memoize2(f(_, yf(_)))
  //    yf
  //  }
}

class Memoize1[-T, +R](f: T => R) extends (T => R) {

  import scala.collection.mutable

  private[this] val vals = mutable.Map.empty[T, R]

  def apply(x: T): R = {
    if (vals.contains(x)) {
      vals(x)
    } else {
      val y = f(x)
      vals += ((x, y))
      y
    }
  }
}

object Memoize1 {
  private def apply[T, R](f: T => R) = new Memoize1(f)

  def Y[T, R](f: (T, T => R) => R): (T) => R = {
    var yf: T => R = null
    yf = Memoize1(f(_, yf(_)))
    yf
  }
}

