package jrigney.fiftyseven

object SayingHello {
  def hello(name: String): String = {
    s"Hello $name"
  }
}
