package jrigney.fiftyseven

object CountingChars {
  def count(s: String): (String, Int) = {
    (s, s.size)
  }
}
