package kitty

trait Printable[A] {
  def format(value: A): String
}

object PrintableDefaults {
  implicit val printableString = new Printable[String] {
    def format(value: String): String = {
      value
    }
  }

  implicit val printableInt = new Printable[Int] {
    def format(value: Int): String = {
      value.toString
    }
  }
}

object PrintableLike {
  implicit class PrintOps[A](value: A) {
    def printIt(implicit printer: Printable[A]): String = {
      printer.format(value)
    }
  }
}

object Printer {
  def print[A](value: A)(implicit printer: Printable[A]): String = {
    printer.format(value)
  }
}

final case class Cat(name: String, age: Int, color: String)
object Cat {
  implicit val printable = new Printable[Cat] {
    def format(value: Cat) = s"${value.name} is a ${value.age} year-old ${value.color} cat"
  }
}
object PrintableApp extends App {
  import PrintableLike._

  val c = Cat("name", 1, "color")
  println(s"using PrintableLike ${c.printIt}")
  println(s"using Printer ${Printer.print(c)}")
}

