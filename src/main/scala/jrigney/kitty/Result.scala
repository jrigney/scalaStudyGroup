package jrigney.kitty

import cats.Functor
import cats.implicits._

object Result {

  implicit val resultFunctor = new Functor[Result] {
    override def map[A, B](fa: Result[A])(f: (A) => B): Result[B] = {
      fa match {
        case Success(value) => Success(f(value))
        case Warning(value, msg) => Warning(f(value), msg)
        case f: Failure => f
      }
    }
  }

  def success[A](value: A): Result[A] = Success(value)
  def warning[A](value: A, msg: String): Result[A] = Warning(value, msg)
  def failure(msg: String): Result[Nothing] = Failure(msg)
}

sealed trait Result[+A] // defined trait Result
final case class Success[A](value: A) extends Result[A] // defined class Success
final case class Warning[A](value: A, message: String) extends Result[A] // defined class Warning
final case class Failure(message: String) extends Result[Nothing] // defined class Failure

object ResultApp extends App {
  import Result._

  success(100).map(_ * 2)

}