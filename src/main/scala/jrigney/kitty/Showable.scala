package jrigney.kitty

import cats.Show
import cats.instances.int._
import cats.instances.string._
import cats.syntax.show._

final case class CatShow(name: String, age: Int, color: String)
object CatShow {
  implicit val catShow = Show.show[CatShow] { c =>
    val name = c.name.show
    val age = c.age.show
    val color = c.color.show
    s"${name} is a ${age} year-old ${color} cat"
  }
}

object ShowableApp extends App {
  val c = CatShow("name", 1, "color")
  println(c.show)
}

