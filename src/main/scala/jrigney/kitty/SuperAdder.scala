package jrigney.kitty

import cats.Monoid
import cats.syntax.semigroup._

object SuperAdder {
  def add[A: Monoid](items: List[A]): A = {
    items.foldLeft(Monoid[A].empty)(_ |+| _)
  }
}

case class Order(totalCost: Double, quantity: Double)
object Order {
  implicit val __orderMonoid__ = new Monoid[Order] {
    override def empty: Order = Order(0, 0)

    override def combine(x: Order, y: Order): Order = {
      Order(x.quantity + y.quantity, x.totalCost + y.totalCost)
    }
  }
}
object SuperAdderApp extends App {

  import cats.instances.int._
  SuperAdder.add(List(1, 2, 3))

  import cats.instances.option._
  SuperAdder.add(List(Some(1), None, Some(3)))

  val order1 = Order(1, 1)
  val order2 = Order(2, 2)

  SuperAdder.add(List(order1, order2))
}
