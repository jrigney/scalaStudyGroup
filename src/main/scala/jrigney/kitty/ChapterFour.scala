package jrigney.kitty

import scala.language.higherKinds
import cats.Monad
import cats.instances.option._
import cats.instances.list._

import scala.concurrent.Future

object ChapterFour {
  import scala.concurrent.ExecutionContext.Implicits.global

  //  val opt1 = Monad[Option].pure(3)
  //  val opt2 = Monad[Option].flatMap(opt1)(a => Some(a + 2))
  //
  //  val list1 = Monad[List].pure(3)
  //  val list2 = List(1, 2, 3)
  //  val list3 = Monad[List].flatMap(list2)(a => List(a, a * 10))
  //
  //  val threeFuture = Future(Option(3))
  //  //need to figure out what implicits are needed
  //  //  val o: Option[Future[Int]] = Monad[Option].sequence(threeFuture)
  //  //  val futureThrees: Future[List[Option[Int]]] = Monad[Future].sequence(List(threeFuture, threeFuture, threeFuture))
  //
  //  def map[F[_], A, B](value: F[A])(fn: A => B): F[B] = {
  //    flatMap(value)(a => pure(fn(a)))
  //  }
  //  def flatMap[F[_], A, B](value: F[A])(func: A => F[B]): F[B] = ???
  //  def pure[F[_], A](value: A): F[A] = ???

  // import cats.Monad

  //4.3.5
  sealed trait Result[+A] // defined trait Result
  final case class Success[A](value: A) extends Result[A] // defined class Success
  final case class Warning[A](value: A, message: String) extends Result[A] // defined class Warning
  final case class Failure(message: String) extends Result[Nothing] // defined class Failure

  implicit val resultMonad = new Monad[Result] {
    override def flatMap[A, B](fa: Result[A])(f: (A) => Result[B]): Result[B] = {
      fa match {
        case Success(x) => f(x)
        case Warning(x, msg) => f(x) match {
          case Success(v) => Warning(v, msg)
          case Warning(v, m) => Warning(v, s"$msg $m")
          case Failure(m) => Failure(s"$msg $m")
        }
        case x @ Failure(_) => x
      }
    }

    override def tailRecM[A, B](a: A)(f: (A) => Result[Either[A, B]]): Result[B] = ???

    override def pure[A](x: A): Result[A] = Success(x)
  }

  def success[A](v: A): Result[A] = Success(v)
  def warning[A](v: A, m: String): Result[A] = Warning(v, m)
  def failure[A](m: String): Result[A] = Failure(m)

  import cats.syntax.functor._
  import cats.syntax.flatMap._

  warning(100, "Message1") flatMap (x => Warning(x * 2, "Message2"))
  warning(10, "Too low") map (_ - 5)

  //id monad
  import cats.Id
  import cats.syntax.flatMap._
  val idThree: Id[Int] = 3
  val idFive: Id[Int] = idThree flatMap (_ + 2)

  //Chapter 4.6 Eval monad
  import cats.Eval
  val someValue = Eval.later { 3 }

  val i = for {
    x <- someValue
  } yield idThree flatMap (_ + x)

}
