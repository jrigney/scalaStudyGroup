package jrigney.interview

object SecretSanta {

  type PairSelection = (Vector[Player], Player) => (SecretPair, Vector[Player])

  def pair(santas: Vector[Player], rcpt: Vector[Player], pairsFn: PairSelection): Vector[SecretPair] = {

    val (_, pairs) = santas.foldLeft((rcpt, Vector.empty[SecretPair])) {
      case ((recipientsRemaining, secretPairs), santa) =>

        val (pair: SecretPair, newRecipients: Vector[Player]) = pairsFn(recipientsRemaining, santa)

        //      println(s"santa [${santa}] recipientsRemaining [${recipientsRemaining}]  validRecipients [${validRecipients}] pair [${pair}] newRecipients [${newRecipients}]")
        (newRecipients, pair +: secretPairs)
    }

    pairs
  }

  def part1Pairing(recipientsRemaining: Vector[Player], santa: Player): (SecretPair, Vector[Player]) = {
    val validRecipients = recipientsRemaining.filterNot(_ == santa)

    val (pair, newRecipients) = validRecipients.headOption.fold((NoRecipient(santa): SecretPair, recipientsRemaining)) { recipient =>

      val pair = Paired(santa, recipient)
      val newRecipients = recipientsRemaining.filterNot(_ == recipient)

      (pair, newRecipients)
    }

    (pair, newRecipients)
  }

  def part2Pairing(recipients: Vector[Player], santa: Player): (SecretPair, Vector[Player]) = {
    val recipientsSansSelf = recipients.filterNot(p => p.name == santa.name)

    def notBefore(recipient: Player): Boolean = !santa.pastRecipients.take(3).contains(recipient)

    val x: Option[(SecretPair, Vector[Player])] = recipientsSansSelf.collectFirst {
      case recipient if notBefore(recipient) =>
        val newRecipients = recipients.filterNot(_ == recipient)
        val newSanta = santa.copy(pastRecipients = recipient +: santa.pastRecipients)
        (Paired(newSanta, recipient), newRecipients)
    }

    //    println(s"santa [${santa}] recipientsSansSelf [${recipientsSansSelf}]")

    x.getOrElse((NoRecipient(santa): SecretPair, recipients))
  }
}

case class Player(name: String, pastRecipients: List[Player])

sealed trait SecretPair

final case class Paired(santa: Player, rx: Player) extends SecretPair

final case class NoRecipient(name: Player) extends SecretPair

object SecretPair {

  implicit class SecretPairOrdered(val secretPair: SecretPair) extends Ordered[SecretPair] {
    override def compare(that: SecretPair): Int = {
      (secretPair, that) match {
        case (Paired(Player(n1, _), _), Paired(Player(n2, _), _)) =>
          if (n1 == n2) 0
          else if (n1 > n2) 1
          else -1
        case (Paired(Player(n1, _), _), NoRecipient(Player(n2, _))) =>
          if (n1 == n2) 0
          else if (n1 > n2) 1
          else -1
        case (NoRecipient(Player(n1, _)), Paired(Player(n2, _), _)) =>
          if (n1 == n2) 0
          else if (n1 > n2) 1
          else -1
        case (NoRecipient(Player(n1, _)), NoRecipient(Player(n2, _))) =>
          if (n1 == n2) 0
          else if (n1 > n2) 1
          else -1
      }
    }
  }

}

