package jrigney.interview

import java.io

object Maps extends App {
  val alphaIn = Seq(Some(3), None, Some(-1), Some(0))
  def filt(input: Seq[Option[Int]]): Seq[Int] = input.flatten.filter(_ > 0)
  println(s"filt ${filt(alphaIn)}")

  val bravoIn = "aaaaabbbccdaa"
  def countLetters(input: String): Map[String, Int] = input.groupBy(_.toString).mapValues(_.size)
  println(s"countLetters ${countLetters(bravoIn)}")

  val charlieIn = Seq(("a", 1), ("d", 4), ("c", 3), ("b", 2))
  def sortTuple(input: Seq[(String, Int)]): Seq[(String, Int)] =
    input.sortBy { case (_, b) => b }
  println(s"sortTuple ${sortTuple(charlieIn)}")

  val in = 1 to 15
  def fizzbuzz1(in: Range): String = {
    def fb1(x: Int): String = {
      x match {
        case x if x % 15 == 0 => "fizzbuzz"
        case x if x % 5 == 0 => "buzz"
        case x if x % 3 == 0 => "fizz"
        case _ => x.toString
      }
    }
    in.map(fb1).mkString(",")
  }
  println(s"fb1 ${fizzbuzz1(in)}")

  def fizzbuzz2(in: Range): String = {
    //num compose buzz compose fizz

    def fizz(n: Int): (Option[String], Int) = {
      if (n % 3 == 0) (Some("Fizz"), n) else (None, n)
    }
    def buzz(x: (Option[String], Int)): (Option[String], Int) = {
      x match {
        case (Some(s), n) if (n % 5 == 0) => (Some(s + "buzz"), n)
        case (None, n) if (n % 5 == 0) => (Some("buzz"), n)
        case (f @ Some(s), n) => (f, n)
        case (_, n) => (None, n)
      }
    }

    val fn = buzz _ compose fizz _
    in.map(fn).collect {
      case (Some(n), _) => n
      case (None, n) => n
    }.mkString(",")
  }

  println(s"fb2 ${fizzbuzz2(in)}")

  def fizzbuzz3(in: Range): String = {

    def fb3(n: Int): String = {
      sealed abstract class FizzBuzzNum(val value: String)
      case object Fizz extends FizzBuzzNum("Fizz")
      case object Buzz extends FizzBuzzNum("Buzz")
      case object FizzBuzz extends FizzBuzzNum("FizzBuzz")
      case object Num extends FizzBuzzNum(n.toString)

      def fizz(fbn: FizzBuzzNum): FizzBuzzNum = {
        fbn match {
          case Num if n % 3 == 0 => Fizz
          case a @ Num => a
          case otherwise => otherwise
        }
      }

      def buzz(fbn: FizzBuzzNum): FizzBuzzNum = {
        fbn match {
          case Num if n % 5 == 0 => Buzz
          case Fizz if n % 5 == 0 => FizzBuzz
          case Buzz => Buzz
          case otherwise => otherwise
        }
      }

      val fn1 = buzz _ compose fizz _

      fn1(Num).value
    }

    in.map(fb3).mkString(",")
  }
  println(s"fb3 ${fizzbuzz3(in)}")
}
