package jrigney.essential

sealed trait Maybe[A] {
  def fold[C](full: A => C, empty: C): C = {
    this match {
      case Empty() => empty
      case Full(x) => full(x)
    }
  }
  def flatMap[B](fn: A => Maybe[B]): Maybe[B] = {
    this match {
      case Empty() => Empty[B]()
      case Full(a) => fn(a)
    }
  }

  def map[B](fn: A => B): Maybe[B] = {
    flatMap[B] { x => Full(fn(x))
    }
  }

}
final case class Empty[A]() extends Maybe[A]
final case class Full[A](value: A) extends Maybe[A]
