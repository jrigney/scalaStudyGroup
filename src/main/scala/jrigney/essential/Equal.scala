package jrigney.essential

trait Equal[A] {
  def equal(v1: A, v2: A): Boolean
}

object Equal {

  implicit object StringEqual extends Equal[String] {
    override def equal(v1: String, v2: String): Boolean = v1.equalsIgnoreCase(v2)
  }

  implicit object IntEqual extends Equal[Int] {
    override def equal(v1: Int, v2: Int): Boolean = v1 == v2
  }

  def apply[A]()(implicit instance: Equal[A]) = instance

  implicit class EqualOps[A](a: A) {
    def ===(v2: A)(implicit e: Equal[A]): Boolean = e.equal(a, v2)
  }

}

object EqualExercise extends App {
  import Equal._
  "abcd".===("ABCD")
  1.===(2)
}

