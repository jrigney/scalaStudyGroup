package jrigney.essential

object FiveFour extends App {
  def intAndString: MyPair[Int, String] = ???

  def booleanAndDouble: MyPair[Boolean, Double] = ???

  val pair = MyPair[String, Int]("hi", 2)
  println(pair.a)
  println(pair.b)

  val left = Left[Int, String](1).value
  val right = Right[Int, String]("foo").value

  val sum: Sum[Int, String] = Right("foo")
  sum match {
    case Left(x) => x.toString
    case Right(x) => x
  }

  val empty: Maybe[Int] = Empty[Int]
  val full: Maybe[Int] = Full(1)
}

case class MyPair[A, B](a: A, b: B)

