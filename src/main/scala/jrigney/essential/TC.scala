package jrigney.essential

final case class Person(name: String, email: String)

trait HtmlWriter[A] {
  def write(in: A): String
}

object HtmlWriter {
  //type class pattern using both implicit parameters and implicit type class instances

  //if the interface defined it the typeclass in the same interface that we want to use we would
  //define something like:
  //object HtmlWriter { //companion object to the typeclass
  def write[A](in: A)(implicit writer: HtmlWriter[A]): String = writer.write(in)

  //but the following would be better
  def apply[A](implicit writer: HtmlWriter[A]): HtmlWriter[A] = writer

}

object ConcreteTypeClassInterface {

  //concrete class for each different situation
  object PersonWriter extends HtmlWriter[Person] {
    override def write(person: Person) = s"${person.name} (${person.email})"
  }

  object ObfuscatedPersonWriter extends HtmlWriter[Person] {
    override def write(person: Person) = s"${person.name} (${person.email.replaceAll("@", " at ")})"
  }

}

object ImplicitTypeClassInterface {

  //implicit parameter list
  object HtmlUtil {
    def htmlify[A](data: A)(implicit writer: HtmlWriter[A]): String = {
      writer.write(data)
    }
  }

  implicit object ApproximationWriter extends HtmlWriter[Int] {
    def write(in: Int): String =
      s"It's definitely less than ${((in / 10) + 1) * 10}"
  }

  implicit val personWriter = new HtmlWriter[Person] {
    override def write(in: Person): String = s"${in.name} (${in.email})"
  }

}

object EnrichedHtml {
  //type enriched with typeclass
  implicit class HtmlOps[T](data: T) {
    def toHtml(implicit writer: HtmlWriter[T]) =
      writer.write(data)
  }

  implicit val enrichedPersonWriter = new HtmlWriter[Person] {
    override def write(in: Person): String = s"${in.name} (${in.email})"
  }
}

object TC extends App {

  //three ways to do typeclasses and interfaces

  def concrete = {
    import ConcreteTypeClassInterface._
    PersonWriter.write(Person("John", "john@example.com")) //concrete class method
  }

  def implicitTypeClass = {
    import ConcreteTypeClassInterface._
    import ImplicitTypeClassInterface._
    HtmlUtil.htmlify(Person("John", "john@example.com"))(PersonWriter) //explicit parameter method
    HtmlUtil.htmlify(2) //implicit parameter using implicit ApproximationWriter

    HtmlWriter[Person].write(Person("Noel", "noel@example.org")) //using the apply in the companion with implicit personWriter
  }

  def enriched = {
    import EnrichedHtml._
    Person("John", "john@example.com").toHtml //using the implicit class HtmlOps and the implicit enrichedPersonWriter
  }

  def typeclassUsage() = {
    def contextBoundAndApply[A: HtmlWriter](x: A): Unit = { //context bound A:HtmlWriter expands into the equivalent implicit parameter list
      val result = HtmlWriter[A].write(x) //uses the apply in HtmlWriter companion object
    }

    import ImplicitTypeClassInterface._
    contextBoundAndApply(Person("john", "john@example.com"))
    contextBoundAndApply(2)
  }
}

