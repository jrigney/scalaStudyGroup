package jrigney.essential

sealed trait MyResult[A]
case class MySuccess[A](result: A) extends MyResult[A]
case class MyFailure[A](reason: String) extends MyResult[A]
