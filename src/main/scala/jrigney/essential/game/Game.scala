package jrigney.essential.game

import scala.io.StdIn
import scala.util.{ Failure, Success, Try }

/**
 * The world's simplest possible implementation of the IO monad.
 * A pure way to represent effectful operations as a thunk.
 */
case class IO[A](unsafePerformIO: () => A) {

  def map[B](ab: A => B): IO[B] =
    IO(() => ab(unsafePerformIO()))

  def flatMap[B](afb: A => IO[B]): IO[B] =
    IO(() => afb(unsafePerformIO()).unsafePerformIO())

  def tryIO(ta: Throwable => A): IO[A] =
    IO(() => {
      IO.tryIO(unsafePerformIO()).unsafePerformIO() match {
        case Left(t) => ta(t)
        case Right(a) => a
      }
    })
}

object IO {
  def point[A](a: => A): IO[A] = IO(() => a)

  def tryIO[A](a: => A): IO[Either[Throwable, A]] =
    IO(() => try {
      Right(a)
    } catch {
      case t: Throwable => Left(t)
    })
}

object Console {
  /**
   * Writes a line of text to the console.
   */
  def putStrLn(line: String): IO[Unit] = IO(unsafePerformIO = () => println(line))

  /**
   * Reads a line of text from the console.
   */
  def getStrLn: IO[String] = IO(unsafePerformIO = () => StdIn.readLine())
}

trait Describable {
  def describe: String
}

sealed trait Item extends Describable

case class Sword() extends Item {
  def describe = "An ordinary metallic sword"
}

case class Location(x: Int, y: Int)

case class Cell(inv: List[Item], baseDesc: String) extends Describable {
  def describe = baseDesc
}

//x[y]
case class WorldMap(private val value: Vector[Vector[Cell]]) {
  def cellAt(loc: Location): Option[Cell] = {
    Try {
      value(loc.x)(loc.y)
    } match {
      case Success(c) => Some(c)
      case Failure(exception) => None
    }
  }

  def updateAt(loc: Location, f: Cell => Cell): Option[WorldMap] = {
    Try {
      value(loc.x)(loc.y)
    } match {
      case Success(c) =>
        Some(WorldMap(value = value.updated(loc.x, value(loc.x).updated(loc.y, f(c)))))
      case Failure(exception) => None
    }
  }
}

case class PlayerState(loc: Location, inv: List[Item])

case class GameState(player: PlayerState, map: WorldMap)

object Direction {

  sealed trait Direction {
    def display: String
  }

  final case class North(display: String = "North") extends Direction

  final case class South(display: String = "South") extends Direction

  final case class East(display: String = "East") extends Direction

  final case class West(display: String = "West") extends Direction

}

sealed trait Command

case object LookAround extends Command

case object MoveNorth extends Command

case object MoveSouth extends Command

case object MoveEast extends Command

case object MoveWest extends Command

case object Pickup extends Command

case object Invalid extends Command

case object Quit extends Command

import Direction._

object Application {
  def parse(text: String): Either[String, Command] = {
    text.trim.toLowerCase match {
      case "q" => Right(Quit)
      case "l" => Right(LookAround)
      case "m s" => Right(MoveSouth)
      case "m n" => Right(MoveNorth)
      case "u" => Right(Pickup)
      case _ => Left(s"unknown command $text")
    }
  }

  //@scala.annotation.tailrec
  def loop(state: GameState): IO[GameState] = {
    import Console._
    for {
      _ <- putStrLn("Command")
      in <- getStrLn
      ActResult(text, newGameState) = parse(in) match {
        case Left(msg) => ActResult(List(msg), Some(state))
        case Right(cmd) => act(cmd, state)
      }
      _ <- text.foldLeft(IO.point(())) {
        case (io, line) => for {
          _ <- io
          _ <- Console.putStrLn(line)
        } yield ()
      }
      finalGameState <- newGameState match {
        case None => IO.point(state)
        case Some(gs) => loop(gs)
      }
    } yield finalGameState
  }

  final case class ActResult(text: List[String], state: Option[GameState])

  def act(command: Command, oldState: GameState): ActResult = {
    import Direction._

    command match {
      case LookAround => lookAround(oldState)
      case Quit => ActResult(List("quitter"), None)
      case Invalid => ActResult(List("Invalid"), Some(oldState))
      case Pickup => pickup(oldState)
      case MoveNorth => move(oldState, North())
      case MoveSouth => move(oldState, South())
      case MoveEast => move(oldState, East())
      case MoveWest => move(oldState, West())
    }
  }

  /**
   * A pure IO value representing the application.
   */
  def start: IO[Unit] = {
    val initialLocation = Location(0, 0)
    val initialInventory = List.empty[Item]
    val initialPlayerState = PlayerState(initialLocation, initialInventory)
    val cell1 = Cell(List(Sword()), "An empty room; route to south")
    val cell2 = Cell(List.empty[Item], "Just like the other rooms; route to the north")
    val worldMap = WorldMap(Vector(Vector(cell1, cell2)))
    val initialGameState = GameState(initialPlayerState, worldMap)
    loop(initialGameState).map(_ => ())
  }

  def move(s: GameState, direction: Direction): ActResult = {
    def newLocation(direction: Direction, l: Location): Location = {
      direction match {
        case North(_) => l.copy(y = l.y - 1)
        case South(_) => l.copy(y = l.y + 1)
        case East(_) => l.copy(y = l.x + 1)
        case West(_) => l.copy(y = l.x - 1)
      }
    }

    s match {
      case GameState(p, m) =>
        val newLoc = newLocation(direction, p.loc)
        m.cellAt(newLocation(direction, p.loc)).fold(ActResult(List(s"Invalid ${direction.display}"), Some(s))) { cell =>
          val newPlayer = p.copy(loc = newLoc)
          ActResult(List(cell.describe), Some(s.copy(player = newPlayer)))
        }
    }
  }

  def lookAround(s: GameState): ActResult = {
    s match {
      case GameState(p, m) =>
        val msg = m.cellAt(p.loc) map (c => s"Desc: ${c.describe} Items: ${c.inv.foldLeft("") { (acc, next) => s"$acc ${next.describe}" }}")
        msg.fold(ActResult(List.empty[String], Some(s))) { m =>
          ActResult(List(m), Some(s))
        }
    }
  }

  def pickup(s: GameState): ActResult = {
    s match {
      case GameState(p, m) =>
        val maybeCell = m.cellAt(p.loc)

        maybeCell.fold(ActResult(List("some how the player is in an invalid location"), Some(s))) { cellUnderInvestigation =>
          cellUnderInvestigation.inv.headOption.fold(ActResult(List("no inventory at location"), Some(s))) { item =>
            val newPlayerState = s.player.copy(inv = item +: s.player.inv)

            val newMap = m.updateAt(p.loc, c => c.copy(inv = cellUnderInvestigation.inv.tail))

            newMap.fold(ActResult(List("map location could not be updated"), Some(s))) { nMap =>
              ActResult(List.empty[String], Some(GameState(newPlayerState, nMap)))
            }
          }
        }
    }
  }

  //attempting something different.
  // I didn't like the copy everywhere.
  // did not need to compose the Lens so it seemed like a waste.
  // is there a naming convention for Lens?  What I have seems Hungarian-ish and not descriptive for its intended use.
  def drop(s: GameState): ActResult = {
    case class Lens[Obj, Value](get: Obj => Value, set: (Obj, Value) => Obj)
    //attempt to contain copy
    val CellInv = Lens[Cell, List[Item]](_.inv, (c, is) => c.copy(inv = is))
    val PlayerStateInv = Lens[PlayerState, List[Item]](_.inv, (p, is) => p.copy(inv = is))
    val GameStateOptionMap = Lens[GameState, Option[WorldMap]](g => Some(g.map), (g, m) => m.fold(g)(aMap => g.copy(map = aMap)))
    val GameStatePlayer = Lens[GameState, PlayerState](_.player, (g, p) => g.copy(player = p))

    s match {
      case GameState(p, m) =>
        val maybeNewGameState = for {
          cell <- m.cellAt(p.loc)
          (item :: rest) = PlayerStateInv.get(p)
        } yield {
          val newPlayerState = PlayerStateInv.set(p, rest)
          val newMap = m.updateAt(p.loc, (c: Cell) => CellInv.set(c, item +: c.inv))

          //I have mixed feelings about this and its readablility vs
          //GameStatePlayer.set(GameStateOptionMap.set(s, newMap), newPlayerState)
          ((GameStatePlayer.set(_: GameState, newPlayerState)) compose (GameStateOptionMap.set(_: GameState, newMap)))(s)
        }

        maybeNewGameState.fold(ActResult(List("could not drop item"), Some(s)))(n => ActResult(List.empty[String], Some(n)))

    }
  }

  def quit(s: GameState): (GameState, Option[String]) = {
    (s, Some("quit"))
  }
}

object Main {
  /**
   * The main function of the application, which performs the effects
   * described by the application's IO monad.
   */
  def main(args: Array[String]): Unit = Application.start.unsafePerformIO()
}
