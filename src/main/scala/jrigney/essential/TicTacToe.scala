import scala.io.StdIn._

object TicTacToe extends App {

  case class Col[A](left: A, center: A, right: A)
  case class Row[A](top: Col[A], middle: Col[A], bottom: Col[A])

  sealed trait PlayerChoice
  case object Xs extends PlayerChoice
  case object Os extends PlayerChoice

  case class Board(matrix: Row[Option[PlayerChoice]])
  case class GameState(board: Board, player: PlayerChoice, occupiedPosition: List[Position])

  sealed trait UserError
  case object InvalidMove extends UserError
  case object GameOverDraw extends UserError
  case object GameOverXs extends UserError
  case object GameOverOs extends UserError

  sealed trait RowPosition
  case object RowTop extends RowPosition
  case object RowMiddle extends RowPosition
  case object RowBottom extends RowPosition

  sealed trait ColPosition
  case object ColLeft extends ColPosition
  case object ColCenter extends ColPosition
  case object ColRight extends ColPosition

  sealed trait UserInput
  case object Quit extends UserInput
  final case class Position(x: ColPosition, y: RowPosition) extends UserInput

  implicit def cToS(c: Option[PlayerChoice]): String = {
    c match {
      case None => " "
      case Some(x: Xs.type) => "x"
      case Some(o: Os.type) => "o"
    }
  }

  def boardCols[A](col: Col[A])(implicit convertToString: A => String): String = {
    s"${convertToString(col.left)} | ${convertToString(col.center)} | ${convertToString(col.right)}"
  }

  def toRowPosition(y: String): Either[String, RowPosition] = {
    y match {
      case "0" => Right(RowTop)
      case "1" => Right(RowMiddle)
      case "2" => Right(RowBottom)
      case otherwise => Left(s"$otherwise in not a valid position")
    }
  }

  def toColPosition(x: String): Either[String, ColPosition] = {
    x match {
      case "0" => Right(ColLeft)
      case "1" => Right(ColCenter)
      case "2" => Right(ColRight)
      case otherwise => Left(s"$otherwise is not a valid position")
    }
  }

  def getInput(): Either[String, UserInput] = {

    val rawInput = readLine(">")
    val positionPattern = "([0-2]),([0-2])".r

    rawInput match {
      case positionPattern(x, y) =>
        for {
          colPos <- toColPosition(x).right
          rowPos <- toRowPosition(y).right
        } yield Position(colPos, rowPos)
      case a if a == "q" => Right(Quit)
      case otherwise => Left(s"$otherwise is not a valid position")
    }
  }

  def showError(err: UserError): Unit = {
    err match {
      case InvalidMove => println(s"Invalid move")
      case GameOverDraw => println(s"Game Over it was a draw")
      case GameOverOs => println(s"Game Over Os win")
      case GameOverXs => println(s"Game Over Xs win")
    }
  }

  def printBoard(board: Row[Option[PlayerChoice]]): Unit = {
    val boardWidth = boardCols(board.top).length
    println(boardCols(board.top))
    println("-" * boardWidth)
    println(boardCols(board.middle))
    println("-" * boardWidth)
    println(boardCols(board.bottom))
  }

  val initialState = {
    val initialBoard: Board = {
      val emptyCol = Col(Option.empty[PlayerChoice], Option.empty[PlayerChoice], Option.empty[PlayerChoice])
      Board(Row(emptyCol, emptyCol, emptyCol))
    }
    GameState(initialBoard, Xs, List.empty[Position])
  }

  def mapBoard(board: Board)(rowPosition: RowPosition, colPosition: ColPosition, playerChoice: PlayerChoice): Board = {
    def rMap[A, B](a: Row[A])(fn: (RowPosition, Col[A]) => Col[B]): Row[B] = {
      val top = fn(RowTop, a.top)
      val middle = fn(RowMiddle, a.middle)
      val bottom = fn(RowBottom, a.bottom)

      Row(top, middle, bottom)
    }

    def cMap[A, B](a: Col[A], p: RowPosition)(fn: (RowPosition, ColPosition, A) => B): Col[B] = {
      val left = fn(p, ColLeft, a.left)
      val center = fn(p, ColCenter, a.center)
      val right = fn(p, ColRight, a.right)
      Col(left, center, right)
    }

    val m = board.matrix

    val updatedMatrix = rMap(m) { (p, col) =>
      cMap(col, p) { (rp, cp, pc) =>
        (rp, cp, pc) match {
          case (`rowPosition`, `colPosition`, _) => Some(playerChoice)
          case (_, _, choice) => choice
        }
      }
    }

    Board(updatedMatrix)
  }

  def rowFold[A, B](a: Col[A], acc: B)(fn: (A, B) => B): B = {
    fn(a.left, fn(a.center, fn(a.right, acc)))
  }

  def rowMap[A, B](a: Row[A])(fn: (Col[A]) => B): List[B] = {
    val top = fn(a.top)
    val middle = fn(a.middle)
    val bottom = fn(a.bottom)
    List(top, middle, bottom)
  }

  def colMap[A, B](m: Row[A])(fn: ((A, A, A) => B)): List[B] = {
    val left = fn(m.top.left, m.middle.left, m.bottom.left)
    val center = fn(m.top.center, m.middle.center, m.bottom.center)
    val right = fn(m.top.right, m.middle.right, m.bottom.right)
    List(left, center, right)
  }

  def checkRowWinners(m: Row[Option[PlayerChoice]]): Option[UserError] = {

    val potentialRowWinner = rowMap(m) { col =>
      col match {
        case Col(Some(Xs), Some(Xs), Some(Xs)) => Some(GameOverXs)
        case Col(Some(Os), Some(Os), Some(Os)) => Some(GameOverOs)
        case _ => None
      }
    }
    potentialRowWinner.flatten.headOption
  }

  def checkColWinners(m: Row[Option[PlayerChoice]]): Option[UserError] = {
    val potentialColWinner = colMap(m) { (l, c, r) =>
      (l, c, r) match {
        case (Some(Xs), Some(Xs), Some(Xs)) => Some(GameOverXs)
        case (Some(Os), Some(Os), Some(Os)) => Some(GameOverOs)
        case _ => None
      }
    }
    potentialColWinner.flatten.headOption
  }

  def endGame(state: GameState): Either[UserError, GameState] = {

    val m = state.board.matrix

    (checkRowWinners(m), checkColWinners(m)) match {
      case (Some(r), _) => Left(r)
      case (_, Some(c)) => Left(c)
      case _ => Right(state)
    }

  }

  def update(userInput: Position, state: GameState): Either[UserError, GameState] = {

    if (state.occupiedPosition.contains(userInput)) Left(InvalidMove)
    else {
      val updatedState = state.copy(occupiedPosition = userInput +: state.occupiedPosition)
      val updatedBoard = mapBoard(updatedState.board)(userInput.y, userInput.x, updatedState.player)
      endGame(updatedState.copy(board = updatedBoard))
    }

  }

  @scala.annotation.tailrec
  def loop(state: GameState): String = {
    println(s"Your turn")
    val userInput = getInput

    userInput match {
      case Left(error) => error
      case Right(Quit) => "quitter"
      case Right(p: Position) =>
        update(p, state) match {
          case Left(error) =>
            error match {
              case InvalidMove =>
                showError(error); loop(state)
              case _ => showError(error); loop(initialState)
            }
          case Right(newState) => printBoard(newState.board.matrix); loop(newState)

        }
    }
  }

  printBoard(initialState.board.matrix)
  println(s"you are Xs")

  val finalStatement = loop(initialState)
  println(s"$finalStatement")
}

