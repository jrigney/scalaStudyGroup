package jrigney.essential

sealed trait LinkedList[A] {
  def length(): Int = {
    this match {
      case Pair(hd, tl) => 1 + tl.length
      case End() => 0
    }
  }

  def contains(item: A): Boolean = {
    this match {
      case Pair(h, t) =>
        if (h == item) true else t.contains(item)
      case End() => false
    }
  }

  def apply(n: Int): Result[A] = {
    this match {
      case Pair(h, t) =>
        if (n == 0) Success(h) else t.apply(n - 1)
      case End() => Failure("Bad things hoppened")
    }
  }

  //fold is a concrete adaption of structural recursion.
  //structural recursion is the generic pattern writing any function that transforms an
  //algebraic data type. So from LinkedList[A] to B
  def fold[B](end: B)(fn: (A, B) => B): B = {
    this match {
      case End() => end
      case Pair(h, t) => fn(h, t.fold(end)(fn))
    }
  }

  def map[B](fn: A => B): LinkedList[B] = {
    this match {
      case End() => End[B]()
      case Pair(h, t) => Pair(fn(h), t.map(fn))
    }
  }
}
final case class End[A]() extends LinkedList[A]
final case class Pair[A](head: A, tail: LinkedList[A]) extends LinkedList[A]

sealed trait Result[A]
case class Success[A](result: A) extends Result[A]
case class Failure[A](reason: String) extends Result[A]

object LinkedListTest extends App {
  val list: LinkedList[Int] = Pair(1, Pair(2, Pair(3, End())))
  val double = list.map(_ * 2)
  assert(double == Pair(2, Pair(4, Pair(6, End()))))

  val addOne = list.map(_ + 1)
  assert(addOne == Pair(2, Pair(3, Pair(4, End()))))

  val divThree = list.map(_ / 3)
  assert(divThree == Pair(1 / 3, Pair(2 / 3, Pair(3 / 3, End()))))
}
