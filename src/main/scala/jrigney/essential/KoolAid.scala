package jrigney.essential

object KoolAid extends App {

  import IntImplicit._
  2.yeah
  3.times(i => println(s"Look - it's the number $i!"))
}

object IntImplicit {
  implicit class IntOps(i: Int) {
    def yeah(): Unit = {
      (1 to i).foreach(_ => println("Oh yeah"))
    }

    def times(fn: Int => Unit) = {
      for { i <- (1 to i) } fn(i)
    }
  }
}

