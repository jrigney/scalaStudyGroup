package jrigney.essential

/**
 * For an algebraic datatype A, fold converts A to B.
 * So leaf/end needs to be a function A => B since Leaf has value:A
 * There are two cases in Tree, Node and Leaf so there are two function parameters
 * in fold, leaf and node which are function
 * There are two cases it Tree, Node and Leaf so there needs to be both cases in the match
 * Node has two parameters left and right so the function for node in the fold parameter
 * list has two parameters.  Since Node has parameters that refer to a recursive field
 * take a parameter B.  So the node function is (B,B) => B
 */
sealed trait Tree[A] {
  def fold[B](leaf: A => B)(node: (B, B) => B): B = {
    this match {
      case Leaf(value) => leaf(value)
      case Node(left, right) => node(left.fold(leaf)(node), right.fold(leaf)(node))
    }
  }
}
final case class Node[A](left: Tree[A], right: Tree[A]) extends Tree[A]
final case class Leaf[A](value: A) extends Tree[A]

object TreeApp extends App {
  val tree: Tree[String] = Node(
    left = Node(Leaf("To"), Leaf("iterate")),
    right = Node(
      left = Node(
        left = Leaf("is"),
        right = Leaf("human,")),
      right = Node(
        left = Leaf("to"),
        right = Node(
          left = Leaf("recurse"),
          right = Leaf("divine")))))

  val sTree: String = tree.fold(x => x) { (left, right) =>
    s"$left $right"
  }

  println(sTree)

}
