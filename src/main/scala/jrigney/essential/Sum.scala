package jrigney.essential

sealed trait Sum[A, B] {
  def fold[C](failure: A => C, success: B => C): C = {
    this match {
      case Right(b) => success(b)
      case Left(a) => failure(a)
    }
  }
  def map[C](fn: B => C): Sum[A, C] = {
    this match {
      case Left(a) => Left(a)
      case Right(b) => Right(fn(b))
    }
  }

  def flatMap[C](fn: B => Sum[A, C]): Sum[A, C] = {
    this match {
      case Left(a) => Left(a)
      case Right(b) => fn(b)
    }
  }
}
final case class Left[A, B](value: A) extends Sum[A, B]
final case class Right[A, B](value: B) extends Sum[A, B]
