package jrigney.dp
//https://community.topcoder.com/stat?c=problem_statement&pm=1259&rd=4493

/**
 * Constraints
 * 1. Sequence up to 50
 * 2. Elements in sequence between 1 and 1000
 * 3. Zigzag sequence does not need to be consecutive
 * 4. A sequence with less than 2 elements is a zigzag sequence
 *
 * Idea1
 * 1. generate all subsequence
 * 2. for each subsequence determine if it is zigzag
 * 3. pick the longest zigzag
 *
 * Idea2
 * a[i] is value in the sequence
 * a[j] is the value of a[i-1]
 * positive[i] <= length of longest zigzag subsequence ending in a[i] and positive
 * negative[i] <= length of longest zigzag subsequence ending at a[i) and negative
 *
 * initial: positive[0] = negative[0] = 1
 * positive[i] = if a[i] - a[j] > 0 max(negative[i] + 1, positive[i])
 * negative[i] = if a[i] - a[j] < 0 max(positive[i] + 1, negative[i])
 * solution: max(positive[i], negative[i])
 */

import java.lang.Math
object ZigZag {
  def longestZigZag(input: Seq[Int]): Int = {
    val values = input.foldLeft((0, 0, input.head)) { (acc, next) =>
      val memoizedValues = memo(acc._1, acc._2, next, acc._3)
      (memoizedValues._1, memoizedValues._2, next)
    }
    Math.max(values._1, values._2)
  }

  def memo(pos: Int, neg: Int, iValue: Int, jValue: Int): (Int, Int) = {
    if (iValue - jValue > 0) {
      val newPos = Math.max(neg + 1, pos)
      (newPos, neg)
    } else {
      val newNeg = Math.max(pos + 1, neg)
      (pos, newNeg)
    }
  }
}
object ZigzagTest extends App {
  val input = Seq(1, 7, 4, 9, 2, 5)
  assert(ZigZag.longestZigZag(input) == 6)
  assert(ZigZag.longestZigZag(Seq(1, 17, 5, 10, 13, 15, 10, 5, 16, 8)) == 7)

}
