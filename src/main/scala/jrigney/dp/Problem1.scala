package jrigney.dp

/**
 * Statement: This is a very standard problem. Given a list of N ordered integers find the
 * longest increasing subsequence in this list.
 * Example: If the list is [16, 3, 5, 19, 10, 14, 12, 0, 15] one possible answer is the
 * subsequence [3, 5, 10, 12, 15], another is [3, 5, 10, 14, 15].
 *
 * Try designing a solution, which will work efficiently for N <= 2,000.
 * Can you think of a solution working fast enough for N <= 2,000,000? You can
 * experiment by generating your own tests and running your solution against them.
 *
 * http://www.hiredintech.com/algorithm-design/learn-algorithms#dynamic
 *
 * Constraints:
 * What to return if only one element list
 * What to return if empty list
 */

object Problem1 extends App {

  val input = List(16, 3, 5, 19, 10, 14, 12, 0, 15)

  type Lists = Map[Int, List[Int]]

  val consecutive = input.foldLeft(Map((input.head -> List(input.head)))) { (acc, next) =>
    val lessMap = acc.filterKeys(_ < next)
    lessMap.size match {
      case 0 => acc + (next -> List(next))
      case 1 => acc + (next -> (next +: lessMap.toList.head._2))
      case _ =>
        val sorted = lessMap.toList.sortWith((a, b) => a._2.size < b._2.size)
        //        println(s"##### next $next acc $acc lessMap $lessMap sorted $sorted") /*REMOVEME*/
        acc + (next -> (next +: sorted.last._2))
    }
  }

  println(s"$consecutive")
}
