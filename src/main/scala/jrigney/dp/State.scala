package jrigney.dp

final case class State[S, A](run: S => (A, S)) {

  def map[B](f: A => B): State[S, B] = {
    val runFn = (s: S) => {
      val (currentValue, newState) = run(s)
      (f(currentValue), newState)
    }

    State(runFn)
  }

  def flatMap[B](f: A => State[S, B]): State[S, B] = {
    val runFn = (s: S) => {
      val (currentValue, newState) = run(s)
      println(s"bind currentValue $currentValue state $s")
      f(currentValue) run (newState)
    }
    State(runFn)
  }

  // Convenience function to drop the resulting state value
  def eval(s: S): A =
    run(s)._1
}

object State {
  //insert value into state
  def insert[S, A](a: A): State[S, A] = State(s => (a, s))

  //Convenience function for taking the current state to a value
  def get[S, A](f: S => A): State[S, A] = State(s => (f(s), s))

  def mod[S](f: S => S): State[S, Unit] = State(s => ((), f(s)))
}

object StateApp extends App {
  type Memo = Map[Int, Int]

  //  def createState(z: Int) = State((memo: Memo) =>
  //    (z + 1, memo)
  //  )
  //
  //  val s = State.insert[Memo, Int](2)
  //  println(s.run(Map()))
  //
  //  val gS = State.get((m: Memo) => m get 1)
  //  println(gS.run(Map()))

  /**
   * State is basically a function that given a state return the current value
   * and a state.  Since State in a function the returned state is a function
   * to get the next value.
   * At least in the example of Fib fibMemoR is building up the function to run.
   * What does that function look like?
   */
  println(FibMemo.fibmemo(5))
}

object FibMemo {
  type Memo = Map[Int, Int]

  def fibmemo(n: Int): Int = {
    def fibmemoR(z: Int): State[Memo, Int] = {
      println(s"z $z")
      if (z <= 1)
        State.insert(z)
      else
        for {
          zValue <- State.get((m: Memo) => m get z)
          v <- {
            val changedStateToZ: Option[State[Memo, Int]] = zValue.map(State.insert[Memo, Int])
            println(s"changedStateToZ $changedStateToZ")
            val y: State[Memo, Int] = changedStateToZ.getOrElse {
              print(s"else of getOrElse")
              val h: State[Memo, Int] = {
                println(s"z1 flatMap")
                fibmemoR(z - 1) flatMap { r =>
                  println(s"z2 flatMap")
                  fibmemoR(z - 2) flatMap { s =>
                    {
                      val t = r + s
                      State.mod((m: Memo) => m + ((z, t))) map (_ => t)
                    }
                  }
                }
              }
              h
            }
            y
          }
        } yield v
    }
    val s: State[Memo, Int] = fibmemoR(n)
    println("after fibmemoR")
    s eval Map()
  }
}
