package jrigney.dp

/**
 * Given a list of N coins, their values (V1, V2, … , VN), and the total sum S.
 * Find the minimum number of coins the sum of which is S (we can use as many coins of
 * one type as we want), or report that it’s not possible to select coins
 * in such a way that they sum up to S.
 *
 * In other words you are given a list of coin values (1, 3, 5) and a total value 10.
 * What are the minimum number of coin needed to equal the sum.  So 5 + 5 == 10 so 2
 * coins.  Or report it is no possible
 *
 * Constraints:
 * How big can N be?
 * How big can S be?
 */

object Beginner extends App {
  val coinValues: List[Int] = List(1, 3, 5)
  val totalValue = 11

  //  val coinCount: Map[Int, Int] = Map((0 -> 0), (1 -> 10), (2 -> 10), (3 -> 10),
  //    (4 -> 10), (5 -> 10))

  val coinCount = {
    (1 to totalValue).foldLeft(Map((0 -> 0))) { (acc, k) => acc + (k -> 10000) }
  }

  val sumValues = (1 to totalValue)

  def coinValueCount(sum: Int, coinValue: Int, coinCount: Map[Int, Int]) = coinCount(sum - coinValue) //number of coins for coinValue to make up the sum

  val counted = sumValues.foldLeft(coinCount) { (count1, sum) =>
    coinValues.foldLeft(count1) { (count, coinValue) =>
      if ((coinValue <= sum) && (coinValueCount(sum, coinValue, count) + 1 < count(sum))) {
        println(s"update sum $sum coinValue $coinValue count $count csc ${count(sum - coinValue)} cs ${count(sum)}")
        count updated (sum, count(sum - coinValue) + 1)
      } else {
        println(s"pass sum $sum coinValue $coinValue count $count")
        count
      }

    }
  }

  println(s"counted $counted")
}

