object Solution {

  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in);
    var n = sc.nextInt();
    var arr = new Array[Int](n);
    for (arr_i <- 0 to n - 1) {
      arr(arr_i) = sc.nextInt();
    }
    val result = arr.sum
    println(result)
  }
}
