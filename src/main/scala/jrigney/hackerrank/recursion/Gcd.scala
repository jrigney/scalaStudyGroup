package jrigney.hackerrank.recursion

import scala.annotation.tailrec

object Gcd extends App {

  @tailrec
  def gcd(x: Int, y: Int): Int = {
    //    println(s"x [${x}] y [${y}]")
    if (y == 0) x else gcd(y, x % y)
  }

  println(gcd(96, 420))
}
