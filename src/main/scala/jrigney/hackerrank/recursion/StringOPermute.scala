package jrigney.hackerrank.recursion

object StringOPermute {

  def main(args: Array[String]): Unit = {
    val numTestCases = scala.io.StdIn.readInt

    val results = (1 to numTestCases).map { _ =>
      val testCase = scala.io.StdIn.readLine()
      val swapped = testCase.grouped(2).map(pair => s"${pair(1).toChar}${pair(0).toChar}")
      swapped.toList.mkString
    }
    results.foreach(println)
  }
}
