package jrigney.hackerrank.recursion

import scala.annotation.tailrec

object Fibonacci extends App {

  def fib(n: Int): Int = {
    def loop(remainingSumsToCalculate: Int, previousSum: Int, currentSum: Int): Int =
      remainingSumsToCalculate match {
        case 0 => previousSum
        case _ => loop(remainingSumsToCalculate - 1, currentSum, previousSum + currentSum)
      }

    loop(n, 0, 1)
  }

  println(s"fib ${fib(35)}")

  class memoize private (val f: Int => Int) {
    import scala.collection.mutable
    private val cache = new mutable.HashMap[Int, Int]()
    def memoized_f(x: Int): Int = {
      cache.getOrElseUpdate(x, f(x))
    }
  }
  object memoize {
    def apply(f: Int => Int): Int => Int = new memoize(f).memoized_f
  }

  val memoizedFib: Int => Int = memoize((n: Int) => {
    if (n <= 1) n else memoizedFib(n - 1) + memoizedFib(n - 2)
  })

  println(s"memoized fib ${fib(35)}")

  //this uses dynamic programming principles in the sense that it calculates the values needed prior to needing them
  //since only the two preceeding are needed then only those need to be kept
  def fibBottomUp(n: Int): Int = {

    @tailrec
    def bottom(n: Int, b: Int, preceedingPreceeding: Int, preceeding: Int): Int = {
      //      println(s"n [${n}] b [${b}] preceedingPreceeding [${preceedingPreceeding}] preceeding [${preceeding}]")
      n match {
        case 2 => b
        case _ => bottom(n - 1, (preceedingPreceeding + preceeding), preceeding, preceeding + preceedingPreceeding)
      }
    }

    n match {
      case 1 | 2 => 1
      case _ => bottom(n, 1, 1, 1)
    }
  }

  println(s"fibBottomUp ${fibBottomUp(35)}")

  //the regular recursive definition way
  def fibRecursive(n: Int): Int = {
    if (n == 1 || n == 2) 1
    else {
      fib(n - 1) + fib(n - 2)
    }
  }

  println(s"fibRecursive ${fibRecursive(35)}")
}
