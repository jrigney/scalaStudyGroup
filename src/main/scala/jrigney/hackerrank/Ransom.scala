
/**
 * examples
 * magazine - give me one grand today night
 * note - give me one grand today
 *
 * magazine - give me one grand today night
 * note - give me one grand today one
 *
 * brute force
 * magazine is an list of strings
 * note is a list of strings
 * loop over the note
 *   if word found in magazine
 *     remove the word from magazine and note for duplicates/multiples
 *   else
 *     remove word from the note
 *
 * bigo brute force
 *   loop over note N
 *    loop over magazine M
 *     remove the word from magazine M
 *     remove the word from note N
 *
 * N * (M * (M + N))
 *
 * optimize
 *   loop over magazine is a search - optimize search with hashmap
 *   remove words from magazine and notes can be optimized if it were a count instead
 *     so keep count for magazine in hashmap
 *     same for notes keep count in hashmap
 *   if magazine.count !>= note.count then fail
 *
 * optimized idea
 *   magazine in hashmap(word,count)
 *   note in hashmap(word,count)
 *   fold over note comparing word count to magazine
 *     if failure then terminate with failure
 *     otherwise recurse(note - word)
 *
 */

import scala.collection.immutable.HashMap

object RansomNote extends App {

  val scanner = new java.util.Scanner(System.in)
  val magazineWordCount = scanner.nextInt
  val noteWordCount = scanner.nextInt

  def asHashmap(wordCount: Int): HashMap[String, Int] = {
    (1 to wordCount).foldLeft(HashMap.empty[String, Int]) { (hashmap, _) =>
      val word = scanner.next
      val wordCount = hashmap.getOrElse(word, 0)
      hashmap updated (word, wordCount + 1)
    }
  }
  val magazine: HashMap[String, Int] = asHashmap(magazineWordCount)

  val note: HashMap[String, Int] = asHashmap(noteWordCount)

  val success_? = note.foldLeft(true) { (acc, noteWordKV) =>
    val (noteWord, noteWordCount) = noteWordKV
    magazine get noteWord match {
      case Some(magazineWordCount) =>
        if (magazineWordCount >= noteWordCount) acc
        else false
      case None => false
    }
  }

  if (success_?) println("Yes") else println("No")
}
