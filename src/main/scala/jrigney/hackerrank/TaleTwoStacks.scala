package jrigney.hackerrank
/**
 *
 * 1 x: Enqueue element into the end of the queue.
 * 2: Dequeue the element at the front of the queue.
 * 3: Print the element at the front of the queue.
 *
 * 10
 * 1 42
 * 2
 * 1 14
 * 3
 * 1 28
 * 3
 * 1 60
 * 1 78
 * 2
 * 2
 *
 * 14
 * 14
 *
 * constraints
 * number of queries
 * number of types
 * value range of element
 * what to do if queue is empty and queue
 * guaranteed valid answer exists for 3
 *
 * examples
 * 4
 * 1 42
 * 2
 * 1 27
 * 3
 * = 27
 *
 * 5
 * 1 42
 * 2
 * 2
 * 1 43
 * 3
 * = 43
 *
 * brute force
 * in stack
 * out stack
 * enqueue to in stack
 * dequeue from out stack
 * on dequeue if out stack is empty reverse in stack; make in stack empty
 *
 * bigo brute force
 * enqueue 1
 * dequeue 1 or N + 1
 *
 * optimize
 *
 * walk through
 *
 * implement
 *
 * test
 */
import java.util.Scanner
import java.io.File
import scala.collection.mutable.ListBuffer

class HackQueue(in: ListBuffer[Int] = ListBuffer.empty, out: List[Int] = Nil) {
  def enqueue(x: Int): HackQueue = new HackQueue(x +=: in, out)
  def dequeue(): (Option[Int], HackQueue) = out match {
    case (h :: tl) => (Option(h), new HackQueue(in, tl))
    case Nil =>
      in.result.reverse match {
        case (h :: tl) => (Option(h), new HackQueue(ListBuffer.empty[Int], tl))
        case Nil => (Option.empty, new HackQueue(ListBuffer.empty[Int], Nil))
      }
  }

  def front: Option[Int] = dequeue match { case (a, _) => a }
  def rear: HackQueue = dequeue match { case (_, q) => q }
}

object TaleTwoStacks extends App {

  //val scan = new Scanner(System.in)
  val scan = new Scanner(new File("/media/smock/project/scalaStudyGroup/data/taleTwoStacksIn08.txt"))
  val numQueries = scan.nextInt
  (1 to numQueries).foldLeft(new HackQueue()) { (q, _) =>
    val queryType = scan.nextInt
    queryType match {
      case 1 =>
        val element = scan.nextInt
        q enqueue element
      case 2 =>
        val (_, newHackQueue) = q.dequeue
        newHackQueue
      case 3 =>
        println(q.front.getOrElse(""))
        q
    }
  }

}

/**
 * need scanner when reading individual values instead of lines
 * do the walk through using hand trace
 * focus on good examples
 *
 */
