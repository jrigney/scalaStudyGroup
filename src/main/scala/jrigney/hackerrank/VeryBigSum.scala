package jrigney.hackerrank

// https://www.hackerrank.com/challenges/a-very-big-sum

object VeryBigSum {

  def main(args: Array[String]): Unit = {
    val elementCount = scala.io.StdIn.readLine
    val elements = scala.io.StdIn.readLine
    val values = elements.split(" ").map(_.toLong)
    val result: Long = values.sum
    println(result)
  }
}

