package jrigney.hackerrank.fuctionalStructure

import jrigney.hackerrank.fuctionalStructure.ListsGcd.PowerOf

object ListsGcd {
  def main(args: Array[String]): Unit = {
    gcd()
  }

  def gcd(): Unit = {
    val listSize = scala.io.StdIn.readInt

    val listGcd = (1 until listSize).toList.foldLeft {
      {
        val inputLine = scala.io.StdIn.readLine()
        finiteRepresentation(inputLine)
      }
    } { (acc, _) =>

      val finiteRep = finiteRepresentation(scala.io.StdIn.readLine())

      (acc.keySet & finiteRep.keySet).map { k =>
        val newPwr: PowerOf = math.min(acc(k), finiteRep(k))
        k -> newPwr
      }.toMap

    }

    val output = listGcd.toList.sortBy(_._1).flatten { case (a, b) => List(a, b) }.mkString(" ")
    println(output)

  }

  type PowerOf = Int
  type Prime = Int

  def finiteRepresentation(in: String): Map[Prime, PowerOf] = {
    val primePowerPairs = in.split(" ").map(_.toInt).grouped(2)
    primePowerPairs.map(pairs => (pairs.head, pairs.last)).toMap
  }
}
