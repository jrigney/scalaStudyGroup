package jrigney.hackerrank

case class Node(data: Int, left: Node, right: Node)

/**
 * constraints
 *
 * brute force
 * traverse tree in order returning all values in tree
 * go through list comparing pairs to see that (a < b)
 *
 * bigo brute force
 * traverse in N
 * go through list in N
 *
 * N + N = 2N = N
 */
object IsBinarySearchTreeApp extends App {

  def isBst(tree: Node): Boolean = {

    def loop(s: List[Node]): List[Int] = {
      if (s.isEmpty) Nil
      else if (s.head == null) loop(s.tail)
      else {
        val leftSide = loop(s.head.left :: s.tail)
        val rightSide = loop(s.head.right :: s.tail)
        leftSide ::: List(s.head.data) ::: rightSide
      }
    }

    val inOrderList = loop(List(tree))
    val (result, _) = inOrderList.foldLeft((true, 0)) { (acc, next) =>
      val (result, previous) = acc
      if (result == false) (false, next)
      else (previous < next, next)
    }
    result
  }

  //  val noExample = Node(3, Node(5,Node(1, null, null), Node(4, null, null)), Node(2, Node(6, null, null), null))
  //  if (isBst(noExample)) println("YES") else println("NO")

  val yesExample = Node(4, Node(1, null, Node(2, null, null)), Node(7, Node(5, null, null), Node(8, null, null)))
  if (isBst(yesExample)) println("YES") else println("NO")
}
