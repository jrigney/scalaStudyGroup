package jrigney.hackerrank

import scala.util.Random

object BubbleSort extends App {
  val arr = List.fill(5)(Random.nextInt)
  val numElements = arr.size

  @scala.annotation.tailrec
  def percolate(sublist: List[Int], acc: List[Int], swaps: Int): (List[Int], Int) = {
    sublist match {
      case Nil => (acc.reverse, swaps)
      case f :: s :: tail if f > s => percolate(f :: tail, s :: acc, swaps + 1)
      case f :: s :: tail => percolate(s :: tail, f :: acc, swaps)
      case f :: Nil => percolate(Nil, f :: acc, swaps)
    }
  }

  @scala.annotation.tailrec
  def bSort(remaining: List[Int], acc: List[Int], swaps: Int): (List[Int], Int) = {
    remaining match {
      case Nil => (acc, swaps)
      case h +: tail =>
        val ps = percolate(acc, List.empty[Int], swaps)
        //        println(s"ps ${ps._1} swaps ${ps._2}") //REMOVEME
        bSort(tail, ps._1, ps._2)

    }
  }

  def bubbleSort[T](inputList: List[T])(implicit ev: T => Ordered[T]): List[T] = {
    def sort(source: List[T], result: List[T]) = {
      bubble(source, Nil, result)
    }

    def bubble(source: List[T], tempList: List[T], result: List[T]): List[T] = source match {
      case h1 :: h2 :: t =>
        if (h1 > h2) bubble(h1 :: t, h2 :: tempList, result)
        else bubble(h2 :: t, h1 :: tempList, result)
      case h1 :: t => sort(tempList, h1 :: result)
      case Nil => result
    }
    sort(inputList, Nil)
  }

  val result = bubbleSort(arr)
  //  val result = bSort((0 until numElements).toList, arr, 0)
  //  println(s"Array is sorted in ${result._2} swaps.")
  //  println(s"First Element: ${result._1.head}")
  //  println(s"Last Element: ${result._1.last}")

  println(s"arr ${arr} sorted ${result}") //REMOVEME
  assert(result == arr.sorted) //REMOVEME
}
