package jrigney.hackerrank

/**
 * https://www.hackerrank.com/challenges/ctci-balanced-brackets
 *
 * sample input
 * 3
 * {[()]}
 * {[(])}
 * {{[[(())]]}}]}
 *
 * sample output
 * YES
 * NO
 * YES
 *
 * constraints
 * min number of strings
 * max number of strings
 * min string length
 * max string length
 * characters in sequence
 *
 * example
 * {[()]([])}
 * (((
 * ]
 * {[())([])}
 *
 * brute force
 * compare each input
 * if it is {[( then push on stack
 * else pop stack if popped is closed associate of open continue
 *   else fail
 *
 * bigo brute force
 * loop over sequence N
 *  compare 1
 *   then push 1
 *   else pop 1
 *     compare 1
 * N + 1 + 1 + 1 + 1
 *
 * optimize
 *
 * walkthrough
 *
 */

import scala.io.StdIn
object BalancedBrackets extends App {

  val numSeq = StdIn.readInt
  (1 to numSeq).foreach { _ =>
    val seq = StdIn.readLine
    val (stack, balanced_?): (List[Char], Boolean) = seq.foldLeft((List.empty[Char], true)) { (acc, bracket) =>
      val (stack, result) = acc
      if (result) {
        bracket match {
          case b @ ('{' | '(' | '[') => (b +: stack, result)
          case '}' | ')' | ']' if stack.isEmpty => (stack, false)
          case b =>
            val (h :: rest) = stack
            h match {
              case '{' => (rest, b == '}')
              case '(' => (rest, b == ')')
              case '[' => (rest, b == ']')
            }
        }
      } else acc
    }
    if (balanced_? && stack.isEmpty) println("YES") else println("NO")
  }
}

/**
 * Found a better way to do this to study
 *
 * object Solution {
 *
 * def main(args: Array[String]) {
 * val sc = new java.util.Scanner (System.in);
 * var t = sc.nextInt();
 * var a0: Int = 0;
 * while(a0 < t){
 * var expression = sc.next();
 * a0+=1;
 *
 * val result = expression.foldLeft(Some(List[Char]()): Option[List[Char]]) {
 * case (None, x) => None
 * case (Some(list), x) if Set('(', '{', '[')(x) => Some(x :: list)
 * case (Some('(' :: tail), ')') => Some(tail)
 * case (Some('[' :: tail), ']') => Some(tail)
 * case (Some('{' :: tail), '}') => Some(tail)
 * case (Some(_), nonMatching) => None
 * }.filter(_.isEmpty).map(_ =>"YES").getOrElse("NO")
 * println(result)
 *
 *
 * }
 * }
 * }
 */
