package jrigney.hackerrank

//https://www.hackerrank.com/challenges/staircase

/**
 * Constraints:
 * input is a single integer
 * last line has zero spaces in it
 * base and height equal n
 * Ideas:
 *
 * Tests:
 * zero as the input
 */
object StairCase {
  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner(System.in)
    var n = sc.nextInt()
    (n to 1).by(-1).foreach { x =>
      val space = ' '.toString * (x - 1)
      val splat = '#'.toString * (n - (x - 1))
      println(space + splat)
    }
  }

}
