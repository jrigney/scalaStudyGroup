package jrigney.hackerrank

// https://www.hackerrank.com/challenges/ctci-bfs-shortest-reach

import scala.annotation.tailrec
import scala.collection.immutable.HashMap
import scala.io.StdIn._

object ShortestReach {

  def main(args: Array[String]): Unit = {
    val numQueries = readInt()

    val queries = (1 to numQueries).foreach { _ =>
      val (numVertices, graph) = inputQuery()
      val startVertex = readLine.trim.toInt
      val vertices = 1 to numVertices
      generateResult(startVertex, vertices.toVector, graph)
    }

  }

  def generateResult(startVertex: Int, vertices: Vector[Int], graph: HashMap[Int, List[Int]]) = {

    val shortestPaths = shortestPathsDijkstra(List((startVertex, 0)), graph, HashMap((startVertex, 6)))
    //    val shortestPaths = dijkstra1(graph)(startVertex)
    println(s"shortestPaths ${shortestPaths}") /*REMOVEME*/
    val result = vertices.foldLeft("") { (acc, next) =>
      if (next == startVertex) acc
      else {
        shortestPaths get next match {
          case None => acc + " -1"
          case Some(v) => acc + s" ${v.toString}"
        }
      }
    }
    println(result.trim)

  }

  def inputQuery(): (Int, HashMap[Int, List[Int]]) = {

    def toTuple[A](l: List[A]): (A, A) = {
      (l(0), l(1))
    }

    @tailrec
    def loop(remaining: List[Int], acc: HashMap[Int, List[Int]]): HashMap[Int, List[Int]] = {
      remaining match {
        case Nil => acc
        case _ :: t =>
          val x = readLine.split(" ").map(_.toInt)
          val forward = acc get x(0) getOrElse List.empty[Int]
          val backward = acc get x(1) getOrElse List.empty[Int]
          val newAcc = acc + ((x(0), (x(1) +: forward)), (x(1), (x(0) +: backward)))
          println(s"newAcc ${newAcc}") /*REMOVEME*/
          loop(t, newAcc)
      }
    }

    val (numVertices, numEdges) = toTuple(readLine().split(" ").map(_.toInt).toList)

    val edges = (1 to numEdges).toList

    val graph = loop(edges, HashMap.empty[Int, List[Int]])
    (numVertices, graph)
  }

  def shortestPathsDijkstra[V](
    fringe: List[(V, Int)],
    graph: HashMap[V, List[V]],
    shortestPathTree: HashMap[V, Int]): HashMap[V, Int] = {
    if (fringe.isEmpty) shortestPathTree
    else {
      val (node, costToStart) = fringe.minBy {
        case (vector, cost) if shortestPathTree.contains(vector) => cost
      }
      val adjacentToFringe = for {
        n <- graph(node)
        if costToStart + 6 < shortestPathTree.getOrElse(n, Int.MaxValue)
      } yield n -> (costToStart + 6)
      val newFringe = (fringe.filterNot {
        case (v, _) => v == node
      } ++ adjacentToFringe).sortBy {
        case (v, c) => c
      }
      shortestPathsDijkstra(newFringe, graph, shortestPathTree ++ newFringe)
    }
  }

  import scala.collection.mutable

  def dijkstra1[N](g: HashMap[N, List[N]])(source: N): (Map[N, Int]) = {
    val fringe = mutable.Set(source)
    val res = mutable.HashMap(source -> 0)
    while (fringe.nonEmpty) {
      val node = fringe.minBy(res)
      fringe -= node
      val cost = res(node)
      for (n <- g(node)) {
        val cost1 = cost + 6
        if (cost1 < res.getOrElse(n, Int.MaxValue)) {
          fringe += n
          res += (n -> cost1)
        }
      }
    }
    (res.toMap)
  }

}

