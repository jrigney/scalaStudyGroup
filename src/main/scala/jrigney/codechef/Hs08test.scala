package jrigney.codechef

import scala.io.StdIn

object Hs08Test extends App {

  def defined(line: String) = {
    line != null && line.nonEmpty
  }
  Iterator.continually(StdIn.readLine).takeWhile(defined(_)).foreach { input =>

    val splitInput = input.split(' ')
    val bankCharge = .50
    val withDraw = splitInput(0).toDouble
    val initialBalance = splitInput(1).toDouble

    def withDrawAmount: Option[Double] = {
      if (0 < withDraw && withDraw <= 2000 && (withDraw % 5 == 0))
        Option(withDraw + bankCharge)
      else
        None
    }

    def finalBalance(withDrawAmount: Double): Option[Double] = {
      val newBalance = initialBalance - withDrawAmount
      if (0 <= initialBalance && initialBalance <= 2000 && newBalance >= 0)
        Option(newBalance)
      else
        None

    }

    val balance = for {
      withDraw <- withDrawAmount
      balance <- finalBalance(withDraw)
    } yield balance

    println(f"${balance.getOrElse(initialBalance)}%.2f")
  }
}
