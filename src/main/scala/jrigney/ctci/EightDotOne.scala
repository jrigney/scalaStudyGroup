package jrigney.ctci

object EightDotOne extends App {

  def countWaysMemo(n: Int): Int = {
    val memo = scala.collection.mutable.HashMap.empty[Int, Int]
    def loop(n: Int): Int = {
//      println(s"n [${n}] memo [${memo}]")
      n match {
        case n if n < 0 => 0
        case n if n == 0 => 1
        case n =>
          memo.get(n) match {
            case Some(x) => x
            case None =>
              val x = loop(n - 1) + loop(n - 2) + loop(n - 3)
              memo.update(n, x)
              x
          }
      }
    }
    loop(n)
  }

  println(s"countWaysMemo [${countWaysMemo(5)}]")

  def countWaysSimple(n: Int): Int = {
    if (n < 0) 0
    else if (n == 0) 1
    else {
      countWaysSimple(n - 1) + countWaysSimple(n - 2) + countWaysSimple(n - 3)
    }
  }

  println(s"countWays ${countWaysSimple(5)}")

  val anyFalse = (1 to 20).map{i => countWaysSimple(i) == countWaysMemo(i)}.filter(_==false).size
  println(s"anyFalse ${anyFalse > 0}")
}
