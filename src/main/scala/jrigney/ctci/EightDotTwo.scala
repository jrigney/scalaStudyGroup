package jrigney.ctci

import scala.collection.immutable.VectorBuilder

object EightDotTwo extends App {

  type Maze = Vector[Vector[Boolean]]
  type Pos = (Int, Int)
  type Path = Vector[Pos]

  //a backtracking recursive algorithm
  def getPath[A](maze: Maze): Option[Path] = {
    val path = new VectorBuilder[Pos]

    //extend solution
    def path_?(pos: Pos): Boolean = {

      if (invalid(pos)) false //we can already tell solution will fail
      else if (target(pos)) true //this is a full solution
      else if (path_?((pos._1, pos._2 - 1)) || path_?((pos._1 - 1, pos._2))) { //see if this extension will lead to a full solution
        path += pos
        true // this solution extension will lead to a full solution
      } else false //this solution extension will NOT lead to a full solution
    }

    def invalid(pos: Pos): Boolean = {
      pos._1 < 0 || pos._2 < 0 || !maze(pos._1)(pos._2)
    }

    def target(pos: Pos): Boolean = pos._1 == 0 && pos._2 == 0

    if (path_?((2,2))) Some(path.result) else None
  }


  val aMaze = Vector(
    Vector(true, true, true),
    Vector(false, false, true),
    Vector(true, true, true)
  )

  println(s"getPath ${getPath(aMaze)}")

}
