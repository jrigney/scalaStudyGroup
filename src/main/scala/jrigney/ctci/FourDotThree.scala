package jrigney.ctci

object FourDotThree extends App {

  sealed trait MyTree[+T]

  case class Leaf[T](v: T) extends MyTree[T]

  case class Branch[T](left: MyTree[T], value: T, right: MyTree[T]) extends MyTree[T]

  case object NoLeaf extends MyTree[Nothing]

  def asInOrderNodes[T](t: MyTree[T]): List[List[T]] = {
    def loop(ts: List[MyTree[T]], acc: List[List[T]]): List[List[T]] = {
      ts match {
        case Nil => acc
        case _ =>
          val nList = ts.flatMap {
            case Branch(l, v, r) => List(l, r)
            case _ => List.empty
          }
          val level = ts.flatMap {
            case Branch(l, v, r) => Some(v)
            case Leaf(v) => Some(v)
            case NoLeaf => None
          }
          loop(nList, acc :+ level)
      }
    }

    loop(List(t), List.empty)
  }

  val testTree = Branch(
    Branch(
      Branch(
        Leaf(0),
        1,
        NoLeaf),
      2,
      Leaf(3)),
    4, Branch(
      Branch(
        Leaf(5),
        6,
        NoLeaf),
      7,
      Branch(
        Leaf(8),
        9,
        NoLeaf)))

  println(s"asInOrderNodes ${asInOrderNodes(testTree)}")
}

