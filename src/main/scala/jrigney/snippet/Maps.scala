package jrigney.snippet

import scala.collection.immutable.HashMap

object Maps extends App {

  val g = HashMap(("a" -> 1), ("b" -> 2))
  val k = g mapValues (_ => Int.MaxValue)
  println(k)

  //a way to collect all the Somes
  val oAlpha = Option("alpha")
  val oBravo = Option.empty[String]
  val oCharlie = Option("charlie")

  val somes: Map[String, String] = Map(
    "alpha" -> oAlpha,
    "bravo" -> oBravo,
    "charlie" -> oCharlie).collect {
      case (k, Some(v)) => (k, v)
    }

  val assembledMap = {
    val builder = Map.newBuilder[String, String]
    oAlpha.foreach(i => builder += "alpha" -> i)
    oCharlie.foreach(i => builder += "charlie" -> i)
    oBravo.foreach(i => builder += "bravo" -> i)
    builder.result()
  }
}
