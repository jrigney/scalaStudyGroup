package jrigney.snippet

import scala.annotation.tailrec

object Factorial extends App {

  def fac(n: Int): Int = {

    @tailrec
    def loop(n: Int, acc: Int): Int = {
      println(s"n [${n}] acc [${acc}]")
      n match {
        case 1 => 1
        case _ => loop(n - 1, n * acc)
      }
    }

    loop(n, 1)
  }

  println(s"fac ${fac(4)}")
}
