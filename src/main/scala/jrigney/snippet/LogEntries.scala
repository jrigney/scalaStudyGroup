package jrigney.snippet

class Time(val underlying: Double) extends AnyVal
class Occupancy(val underlying: Int) extends AnyVal {
  def +(other: Int) = { new Occupancy(underlying + other) }
  def -(other: Int) = { new Occupancy(underlying - other) }
}
sealed trait Direction
case class In() extends Direction
case class Out() extends Direction

object LogEntries extends App {
  def counts(times: List[(Double, Double)]): List[(Double, Int)] = {

    val sortedTimes: List[(Time, Direction)] = {
      val inOutTimes: List[(Time, Direction)] = times.foldLeft(List.empty[(Time, Direction)]) { (inOutTimes, times) =>
        val (inTime, outTime) = times
        val in = (new Time(inTime), In())
        val out = (new Time(outTime), Out())

        List(in, out) ++: inOutTimes
      }

      inOutTimes.sortBy { timeDirection ⇒
        val (time, _) = timeDirection
        time.underlying
      }
    }

    val occupiedTimes = {
      val head = (sortedTimes.head._1, new Occupancy(1))
      sortedTimes.tail.foldLeft(List(head)) { (acc, inOutTime) ⇒

        val (time, direction) = inOutTime
        val (resultTime, occupants) = acc.head
        direction match {
          case x: In ⇒ (time, occupants + 1) +: acc
          case x: Out ⇒ (time, occupants - 1) +: acc
        }
      }
    }

    occupiedTimes.reverse.map {
      case (time, occupancy) => (time.underlying, occupancy.underlying)
    }
  }

}

