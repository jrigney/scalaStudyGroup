package jrigney.snippet

object PermutationString {
  def permutations(s: String): List[String] = {

    //    println(s"##### s ${s}")

    def merge(ins: String, c: Char): Seq[String] = {
      val xx = for (i <- 0 to ins.length) yield {
        val yy = ins.substring(0, i) + c + ins.substring(i, ins.length)
        println(s"yy ${yy}")
        yy
      }
      xx
    }

    if (s.length() == 1)
      List(s)
    else
      permutations(s.substring(0, s.length - 1)).flatMap { p =>
        //        println(s"##### p ${p}  c ${s.charAt(s.length - 1)}") /*REMOVEME*/
        val xx = merge(p, s.charAt(s.length - 1))
        //        println(s"##### merged ${xx} sL ${s.length - 1}") /*REMOVEME*/
        xx
      }
  }

  def permutations[T](lst: List[T]): List[List[T]] = {
    //    println(s"##### lst ${lst}") /*REMOVEME*/
    lst match {
      case Nil => List(Nil)
      case x :: xs => permutations(xs) flatMap { perm =>
        val yy = (0 to xs.length) map { num =>
          val xx = (perm take num) ++ List(x) ++ (perm drop num)
          //          println(s"  ##### perm ${perm.mkString} pT ${(perm take num).mkString} x ${x} pD ${(perm drop num).mkString} num ${num}") /*REMOVEME*/
          xx
        }
        //        println(s"yy ${yy}")
        yy
      }
    }
  }
}

object PermutationStringApp extends App {
  //  println(PermutationString.permutations("dogs").size)
  println(PermutationString.permutations("dogs".toList).map(_.mkString))
}