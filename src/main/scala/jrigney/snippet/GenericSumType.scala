package jrigney.snippet

sealed trait Sum[A, B] {
  def fold[C](error: A => C, success: B => C): C = this match {
    case Failure(v) => error(v)
    case Success(v) => success(v)
  }

  def map[C](bc: B => C): Sum[A, C] = this match {
    case Failure(a) => Failure(a) //we can't use f@Failure => f here because f@Failure is Sum[A,B] and we need a Sum[A,C]
    case Success(b) => Success(bc(b))
  }

  def flatMap[C](f: B => Sum[A, C]) = this match {
    case Failure(a) => Failure(a) //we can't use f@Failure => f here because f@Failure is Sum[A,B] and we need a Sum[A,C]
    case Success(b) => f(b)
  }
}

final case class Failure[A, B](value: A) extends Sum[A, B]

final case class Success[A, B](value: B) extends Sum[A, B]

object GenericSumType extends App {

  val x: Sum[Int, String] = Success("bob")
  val xr = x fold (i => i.toString, s => s"$s lives")
  println(xr)

  val y: Sum[Int, String] = Failure(1)
  val yr = y fold (i => i.toString, s => s)
  println(yr)

  val xm = x map (s => s"$s was here")
  println(xm)

  val ym = y map (s => s"$s was here")
  println(ym)

  val xfm = x.flatMap(s => y)
  println(xfm)

  val xfms = x.flatMap(s => Success(s"$s eats at joes"))
  println(xfms)

  val yfm = y.flatMap(s => Success(s"$s eats at joes"))
  println(yfm)
}
