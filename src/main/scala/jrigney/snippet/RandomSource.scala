package jrigney.snippet

import scala.collection.immutable.Queue

trait RandomSource[A] {
  def next(): (A, RandomSource[A])
}

case class QueuedRandomSource[A](randoms: Queue[A]) extends RandomSource[A] {
  override def next(): (A, RandomSource[A]) = {
    val (top, qRest) = randoms.dequeue
    (top, QueuedRandomSource(qRest))
  }
}
