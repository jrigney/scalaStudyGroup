package jrigney.snippet

object ConcreteTypeClassInterface {

  //concrete class for each different situation
  object PersonWriter extends HtmlWriter2[Person] {
    override def write(person: Person) = s"${person.name} (${person.email})"
  }
}

object ImplicitTypeClassInterface {

  //implicit parameter list
  object HtmlUtil {
    def htmlify[A](data: A)(implicit writer: HtmlWriter2[A]): String = {
      writer.write(data)
    }
  }

  implicit object ApproximationWriter extends HtmlWriter2[Int] {
    def write(in: Int): String =
      s"It's definitely less than ${((in / 10) + 1) * 10}"
  }

  implicit val personWriter = new HtmlWriter2[Person] {
    override def write(in: Person): String = s"${in.name} (${in.email})"
  }

}

object HtmlWriter2 {
  //type class pattern using both implicit parameters and implicit type class instances

  //if the interface defined it the typeclass in the same interface that we want to use we would
  //define something like:
  //  def write[A](in: A)(implicit writer: HtmlWriter2[A]): String = writer.write(in)

  //but the following would be better
  def apply[A](implicit writer: HtmlWriter2[A]): HtmlWriter2[A] = writer

}

trait HtmlWriter2[A] {
  def write(in: A): String
}

object ImplicitTypeclass {

  import ConcreteTypeClassInterface._
  import ImplicitTypeClassInterface._

  //notice that HtmlUtil is able to take either an Int or a person because it is able to find the correct implicit to
  //use for the given type
  HtmlUtil.htmlify(Person("John", "john@example.com"))(PersonWriter) //explicit parameter method
  HtmlUtil.htmlify(2) //implicit parameter using implicit ApproximationWriter

  //the method below uses the apply in HtmlWriter2 and is still able to find the correct implicit to use in the apply
  HtmlWriter2[Person].write(Person("Noel", "noel@example.org")) //using the apply in the companion with implicit personWriter
  HtmlWriter2[Int].write(2) //using the apply in the companion with implicit personWriter
}
