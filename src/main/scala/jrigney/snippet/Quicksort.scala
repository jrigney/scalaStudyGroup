package jrigney.snippet

/**
 * general idea of quicksort
 * partition a list
 *   pick a pivot point
 *   split the list 2 subarrays where elements < pivot are one array and elements > pivot are the other array
 * a list is sorted when the elements less than the pivot are sorted combined with the pivot combined with the elements greater than the pivot are sorted
 * call quicksort recursively on 2 subarrays to get the elements less than and greater than the pivot sorted
 */
object Quicksort {

  def quicksort[A](list: List[A])(implicit ev: A => Ordered[A]): List[A] = {

    //a sorted list is naturally the sorted list of the elements less than the pivot
    //combined with the pivot
    //comibned with sorted list of the elements greater than the pivot
    def sort(partitions: (List[A], A, List[A])): List[A] = partitions match {
      case (Nil, p, Nil) => List(p)
      case (lt, p, gt) => partitionAndSort(lt) ++: (p +: partitionAndSort(gt))
    }

    /**
     * pick a pivot point
     * put elements < pivot on l(ess) side
     * put elements >= pivot on g(reater) side
     */
    def partition(as: List[A]): (List[A], A, List[A]) = {
      @scala.annotation.tailrec
      def loop(p: A, as: List[A], lt: List[A], gt: List[A]): (List[A], A, List[A]) =
        as match {
          case h +: t => if (h < p) loop(p, t, h +: lt, gt) else loop(p, t, lt, h +: gt)
          case Nil => (lt, p, gt)
        }

      //the head is being chosen as the pivot
      loop(as.head, as.tail, Nil, Nil)
    }

    def partitionAndSort(as: List[A]): List[A] =
      if (as.isEmpty) Nil
      else sort(partition(as))

    partitionAndSort(list)
  }
}

object QuicksortApp extends App {

  val a = List(9, 7, 5, 11, 12, 2, 14, 3, 10, 6)
  println(s"quicksort ${Quicksort.quicksort(a)}")
}
