package jrigney.snippet

trait HtmlWriter[A] {
  def write(in: A): String
}

object EnrichedHtml {
  //type enriched with typeclass
  implicit class HtmlOps[T](data: T) {
    def toHtml(implicit writer: HtmlWriter[T]) =
      writer.write(data)
  }

  implicit val enrichedPersonWriter = new HtmlWriter[Person] {
    override def write(in: Person): String = s"${in.name} (${in.email})"
  }
}

object TypeEnrichment {
  import EnrichedHtml._
  Person("John", "john@example.com").toHtml //using the implicit class HtmlOps and the implicit enrichedPersonWriter
}
