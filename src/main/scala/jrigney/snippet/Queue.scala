package jrigney.hackerrank
/**
 *
 * 1 x: Enqueue element into the end of the queue.
 * 2: Dequeue the element at the front of the queue.
 * 3: Print the element at the front of the queue.
 *
 * 10
 * 1 42
 * 2
 * 1 14
 * 3
 * 1 28
 * 3
 * 1 60
 * 1 78
 * 2
 * 2
 *
 * 14
 * 14
 *
 * constraints
 * number of queries
 * number of types
 * value range of element
 * what to do if queue is empty and queue
 * guaranteed valid answer exists for 3
 *
 * examples
 * 4
 * 1 42
 * 2
 * 1 27
 * 3
 * = 27
 *
 * 5
 * 1 42
 * 2
 * 2
 * 1 43
 * 3
 * = 43
 *
 * brute force
 * in stack
 * out stack
 * enqueue to in stack
 * dequeue from out stack
 * on dequeue if out stack is empty reverse in stack; make in stack empty
 *
 * bigo brute force
 * enqueue 1
 * dequeue 1 or N + 1
 *
 * optimize
 *
 * walk through
 *
 * implement
 *
 * test
 */
import java.util.Scanner
//import java.io.File

class Queue(val in: List[Int] = Nil, val out: List[Int] = Nil) {
  def enqueue(x: Int): Queue = new Queue(x +: in, out)
  def dequeue(): (Option[Int], Queue) = out match {
    case (h :: tl) => (Option(h), new Queue(in, tl))
    case Nil => in.reverse match {
      case (h :: tl) => (Option(h), new Queue(Nil, tl))
      case Nil => (Option.empty, new Queue(Nil, Nil))
    }
  }

  def front: Option[Int] = dequeue match { case (a, _) => a }
  def rear: Queue = dequeue match { case (_, q) => q }
  def isEmpty: Boolean = in.isEmpty && out.isEmpty
}

object Queue extends App {

  val scan = new Scanner(System.in)
  //val scan = new Scanner(new File("/media/smock/project/scalaStudyGroup/data/taleTwoStacksIn.txt"))
  val numQueries = scan.nextInt
  (1 to numQueries).foldLeft(new Queue()) { (q, _) =>
    val queryType = scan.nextInt
    queryType match {
      case 1 =>
        val element = scan.nextInt
        q enqueue element
      case 2 =>
        val (_, newQueue) = q.dequeue
        newQueue
      case 3 =>
        println(q.front.getOrElse(""))
        q
    }
  }

}

/**
 * need scanner when reading individual values instead of lines
 * do the walk through using hand trace
 * focus on good examples
 *
 */
