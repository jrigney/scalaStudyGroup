package jrigney.snippet

/**
 *
 * http://www.shadabahmed.com/blog/2013/04/08/transform-one-string-to-another/
 *
 * Transform One String to Another
 * Let S and T be strings and D a set of strings. You can say that S produces T if there exists a sequence of strings, SEQ=[s1, s2, …, sn-1] which meets these criteria:
 *
 * s0 = S
 * sn-1 = T
 *
 * All members of SEQ belong to set D
 *
 * Adjacent strings have the same length and differ in exactly one character.
 *
 * For example, given the set {“cat”, “bar”, “bat”}, you can say that “cat” produces “bar” by
 *
 * [“cat”, “bat”, “bar”]
 *
 * Or, given the set {“cat”, “red”, “bat”}, you can say that “cat” does not produce “red”.
 *
 * Given a set D and two strings S and T, write a function to determine if S produces T. Assume that all > characters are lowercase letters. If S does produce T, output the length of a shortest production sequence; otherwise, output -1.
 */
object TransformString extends App {
  val startWord = "simple"
  val endWord = "fickle"
  val d = Set("simple", "dimple", "pimple", "fickle", "sickle", "simkle", "kettle", "settle")

  def matchesAlmost(s1: String, s2: String): Boolean = {
    if (s1.length != s2.length)
      return false
    else {
      val differences = s1 diff s2
      differences.size == 1
    }
  }

  val graph: Map[String, Set[String]] = d.map { v =>
    val edges = d.flatMap { endPoint =>
      if (matchesAlmost(v, endPoint))
        Some(endPoint)
      else None
    }
    (v, edges)
  }.toMap

  def bfs(start: String) = {
    import scala.collection.immutable.Queue
    @scala.annotation.tailrec
    def bfsLoop(remaining: Queue[String], visited: Set[String], result: Boolean, graph: Map[String, Set[String]]): Option[Boolean] = {
      (result, remaining) match {
        case (false, q) if q.isEmpty => Some(false)
        case (true, _) => Some(true)
        case (false, remaining) =>
          val (node, rest) = remaining.dequeue
          if (visited.contains(node)) bfsLoop(rest, visited, false, graph)
          else {
            val remainingNodes = graph.get(node).fold(rest)(rest.enqueue(_))
            if (node == endWord) bfsLoop(remainingNodes, visited + node, true, graph)
            else bfsLoop(remainingNodes, visited + node, false, graph)
          }
      }
    }

    bfsLoop(Queue(start), Set.empty[String], false, graph)
  }

  println(bfs(startWord))
}

//bool matchesAlmost(String str1, String str2) {
//  if (str1.length != str2.length)
//  return false;
//  int same = 0;
//  for (int i = 0; i < str1.length; ++i) {
//  if (str1.charAt(i) == str2.charAt(i))
//  same++;
//}
//  return same == str1.length - 1;
//}

