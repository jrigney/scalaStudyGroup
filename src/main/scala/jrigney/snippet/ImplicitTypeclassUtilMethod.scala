package jrigney.snippet

object Person6 {

  //concrete class for each different situation
  object Person6Writer extends HtmlWriter6[Person6] {
    override def write(person: Person6) = s"${person.name} (${person.email})"
  }

  implicit val personWriter = new HtmlWriter6[Person6] {
    override def write(in: Person6): String = s"${in.name} (${in.email})"
  }
}

object HtmlWriter6 {
  //implicit parameter list
  object HtmlUtil {
    def htmlify[A](data: A)(implicit writer: HtmlWriter6[A]): String = {
      writer.write(data)
    }
  }

  implicit object ApproximationWriter extends HtmlWriter6[Int] {
    def write(in: Int): String =
      s"It's definitely less than ${((in / 10) + 1) * 10}"
  }

  //type class pattern using both implicit parameters and implicit type class instances

  //if the interface defined it the typeclass in the same interface that we want to use we would
  //define something like:
  //  def write[A](in: A)(implicit writer: HtmlWriter6[A]): String = writer.write(in)

  //but the following would be better
  def apply[A](implicit writer: HtmlWriter6[A]): HtmlWriter6[A] = writer

}

case class Person6(name: String, email: String)

trait HtmlWriter6[A] {
  def write(in: A): String
}

object ImplicitTypeclass6 {

  import Person6._
  import HtmlWriter6._

  //notice that HtmlUtil is able to take either an Int or a person because it is able to find the correct implicit to
  //use for the given type
  HtmlUtil.htmlify(Person6("John", "john@example.com"))(Person6Writer) //explicit parameter method
  HtmlUtil.htmlify(2) //implicit parameter using implicit ApproximationWriter

  //the method below uses the apply in HtmlWriter6 and is still able to find the correct implicit to use in the apply
  HtmlWriter6[Person6].write(Person6("Noel", "noel@example.org")) //using the apply in the companion with implicit personWriter
  HtmlWriter6[Int].write(2) //using the apply in the companion with implicit personWriter
}
