package jrigney.snippet

object Permutation extends App {
  def permutations[T]: List[T] => Traversable[List[T]] = {
    case ts if (ts.isEmpty) =>
      println(s"ts $ts")
      List(Nil)
    case ts => {
      println(s"ts $ts")
      for {
        (t, i) <- ts.zipWithIndex
        ys <- permutations(ts.take(i) ++ ts.drop(1 + i))
        _ = println(s"ys $ys")
      } yield {
        t :: ys
      }
    }
  }

  println(permutations(List("a", "b", "c")))
}
