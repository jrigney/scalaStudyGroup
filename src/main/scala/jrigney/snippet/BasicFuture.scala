package jrigney.snippet

object BasicFuture extends App {
  import scala.concurrent.Await
  import scala.concurrent.duration._
  import scala.concurrent.Future
  //import scala.concurrent.forkjoin._
  import scala.concurrent.ExecutionContext
  import scala.util.{ Success, Failure }
  //you could use import ExecutionContext.Implicits.global instead of
  //implicit val ec = ExecutionContext.global

  implicit val ec = ExecutionContext.global

  val f: Future[List[String]] = Future {
    //what you do here will be async
    //because as soon as the future is applied it can start running
    //on a different thread
    List("one", "two", "three")
  }

  //callbacks are how you get computation done by the Future
  f onComplete {
    case Success(posts) => for (post <- posts) println(post)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }

  //you want to use Await only for testing because it blocks the thread.
  //it is recommended that you avoid Await when possible in favor of callbacks and
  //combinators like onComplete and use in for comprehensions
  Await.result(f, 0 nanos)
}
