package jrigney.snippet

import scala.collection.immutable.Queue
import scala.collection.immutable.HashSet

object Dijktra {
  /**
   * case class Vertex(id, distance)
   *
   * discovered - queue of vertex not processed yet
   * distances - map of vertex to distance of vertex to start
   * graph - map of vertex to list of Vertex where the distance in Vertex is form the map vertex to adjacent vertex
   *
   * if discovered.isEmpty then return distances
   * else
   *   dequeue source from discovered
   *   if source processed then skip and loop with tail of discovered
   *   else
   *     get distance from start to source from distances
   *     foreach adjacent vertices to source
   *       if adjacent is processed then skip
   *       else
   *         if distance from start to source plus distance from source to adjacent < distanceAdjacentToStart then replace with shorter distance
   *         else don't change distance
   *     loop with discovered appended with adjacent vertices, updated distances and source added to processed
   *
   *
   */
  def dijkstra(source: Vertex, graph: Map[Int, List[Vertex]]): Map[Int, Int] = {

    def loop(discovered: Queue[Vertex], distances: Map[Int, Int], processed: HashSet[Int]): Map[Int, Int] = {
      println(s"discovered $discovered distances $distances processed $processed")
      discovered.isEmpty match {
        case true => distances
        case false =>
          val (source, rest) = discovered.dequeue
          if (!processed.contains(source.id)) {

            val distanceToStart = distances(source.id)

            val adjacent = graph(source.id)
            val updatedDistances = adjacent.foldLeft(distances) { (updatedDistancesAcc, vertex) =>
              if (!processed.contains(vertex.id)) {
                val distanceAdjacentToStart = updatedDistancesAcc.get(vertex.id).getOrElse(Int.MaxValue)
                if (distanceToStart + vertex.distance < distanceAdjacentToStart)
                  updatedDistancesAcc.updated(vertex.id, distanceToStart + vertex.distance)
                else
                  updatedDistancesAcc
              } else
                updatedDistancesAcc

            }
            val newQueue: Queue[Vertex] = rest ++ adjacent
            val sortedQ = newQueue.sortWith((v1, v2) => v1.distance < v2.distance)
            loop(sortedQ, updatedDistances, processed + source.id)
          } else {
            println(s"skip $source")
            loop(rest, distances, processed)
          }
      }
    }

    loop(Queue(source), Map((1 -> 0)), HashSet.empty[Int])
  }
}

case class Vertex(id: Int, distance: Int)

object DijkstraApp extends App {
  val graph: Map[Int, List[Vertex]] = Map(
    (1 -> List(Vertex(2, 2), Vertex(3, 3))),
    (2 -> List(Vertex(4, 3), Vertex(1, 2))),
    (3 -> List(Vertex(4, 1), Vertex(1, 3))),
    (4 -> List(Vertex(2, 3), Vertex(3, 1))))
  println(s"===dijkstra ${Dijktra.dijkstra(Vertex(1, 0), graph)}")
}
