package jrigney.snippet

import java.util.Scanner

/**
 * constraints
 * directed undirected
 * max number of nodes
 * max number of edges
 * multi paths / self loops
 * all connected or some not connected
 *
 * brute force
 * visited: HashMap(distance)
 *
 *
 *
 */

import scala.collection.immutable.HashSet
import scala.collection.immutable.HashMap
import scala.collection.immutable.Queue

object Graph {
  def graph(edges: Vector[(Int, Int)], initalMap: Map[Int, List[Int]]): Map[Int, List[Int]] = {

    edges.foldLeft(initalMap) { (g, edge) =>
      val (startNode, endNode) = edge
      val updatedGraph = for {
        adjacentNodes <- g.get(startNode)
        updatedGraph <- Option(g.updated(startNode, endNode +: adjacentNodes))
      } yield updatedGraph

      updatedGraph.get
    }
  }

  /**
   * DFS
   * discovered: List[Vector] //put vectors that are known but not yet processed such as the start vector or the adjacent vectors that need to be processed
   * processed: HashSet[Vector] //put vectors that have been processed and can be skipped if seen again(eg in a cycle in the graph).
   * what does processed mean?  a processed vector is a vector that you have evaluated and does not need to be evaluated again.
   *   In this case the distance from the vector to its adjacent vertices have been recorded in distances.  If we see the vector again we don't need to enter the distances again into distances
   *
   * source is the root node of the current tree
   *
   * if discoverd.isEmpty
   *   return with result
   * else
   *   if(processed.contains(source))
   *     //skip over vertex and continue with rest of the discovered vertices
   *     loop with discovered.tail and processed
   *   else
   *     //process the vertex
   *     loop with the adjacent prepended to discovered.tail and source added to processed
   */
  def dfs(start: Int, graph: Map[Int, List[Int]]) = {

    @scala.annotation.tailrec
    def loop(discovered: List[Int], distances: Map[Int, Int], processed: HashSet[Int]): Map[Int, Int] = {
      discovered match {
        case Nil => distances
        case (source :: tail) =>
          if (processed.contains(source)) loop(tail, distances, processed)
          else {
            //do stuff with source vertex
            println(s"### loop source ${source}")
            val ds = updatedDistances(source, distances, graph)
            println(s"ds ${ds}")
            loop(graph(source) ++: tail, ds, processed + source)
          }
      }
    }

    loop(List(start), Map.empty[Int, Int], HashSet.empty[Int])
  }

  def updatedDistances(source: Int, distances: Map[Int, Int], graph: Map[Int, List[Int]]): Map[Int, Int] = {
    val adjacent = graph(source)
    println(s"source ${source} adjacent ${adjacent} distances ${distances} ")
    adjacent.foldLeft(distances) { (d, vertex) =>
      val sourceDistance = d.getOrElse(source, 0)
      val updatedDistance = d.getOrElse(vertex, 0) + 6
      d.updated(vertex, sourceDistance + updatedDistance)
    }
  }

  //dfs that takes a function to process the source
  def dfsfns(start: Int, graph: Map[Int, List[Int]]) = {

    @scala.annotation.tailrec
    def loop[A](discoverd: List[Int], result: A, process: (Int, A) => A, processed: HashSet[Int]): A = {
      discoverd match {
        case Nil => result
        case (source :: tail) =>
          if (processed.contains(source))
            loop(tail, result, process, processed)
          else {
            val newResult = process(source, result)
            loop(graph(source) ++: tail, newResult, process, processed + source)
          }
      }
    }

    val fn = updatedDistances(_: Int, _: Map[Int, Int], graph)
    loop(List(start), HashMap.empty[Int, Int], fn, HashSet.empty[Int])
  }

  /**
   * BFS
   * use a queue to keep track of discovered vertices
   *
   * processed = HashSet
   * source = dequeue from the queue
   * if source is in hashset then skip and loop to process rest of queue
   * else
   *   process the source
   *   add source to the processed hashset
   *   loop to process rest of queue
   */
  def bfs(start: Int, graph: Map[Int, List[Int]]) = {
    @scala.annotation.tailrec
    def loop(discovered: Queue[Int], distances: Map[Int, Int], processed: HashSet[Int]): Map[Int, Int] = {
      discovered.isEmpty match {
        case true => distances
        case false =>
          val (source, rest) = discovered.dequeue
          //do something with source
          val ds = updatedDistances(source, distances, graph)
          loop(rest.enqueue(graph(source)), ds, processed + source)
      }
    }

    loop(Queue(start), Map.empty[Int, Int], HashSet.empty[Int])
  }
}

object GraphAdjacencyApp extends App {

  val four =
    """
    1
    4 3
    1 2
    1 3
    3 4
    """
  val scanner = new Scanner(four)

  //    val scannerSource = System.in
  //    val scanner = new Scanner(scannerSource)

  val queries = scanner.nextInt
  (1 to queries).foreach { _ =>
    val numNodes = scanner.nextInt
    val numEdges = scanner.nextInt

    val edges: Vector[(Int, Int)] = getEdges(numEdges).toVector
    val initalMap = (1 to numNodes).map((_, List.empty[Int])).toMap
    val graph = Graph.graph(edges, initalMap)

    println(s"==r ${Graph.bfs(1, graph)}")

  }

  def getEdges(numEdges: Int): IndexedSeq[(Int, Int)] = {
    (1 to numEdges).map { _ =>
      val startNode = scanner.nextInt
      val endNode = scanner.nextInt
      (startNode, endNode)
    }
  }

}
