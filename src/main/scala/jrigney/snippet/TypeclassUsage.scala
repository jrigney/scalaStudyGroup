package jrigney.snippet

object HtmlWriter5 {

  //if the interface defined it the typeclass in the same interface that we want to use we would
  //define something like:
  def apply[A](implicit writer: HtmlWriter5[A]): HtmlWriter5[A] = writer

  //implicit parameter list
  object HtmlUtil {
    def htmlify[A](data: A)(implicit writer: HtmlWriter5[A]): String = {
      writer.write(data)
    }
  }

  //another way to define a typeclass instance as an object vs a val
  //because we don't have access to the src for Int we define the instance in the typeclass companion
  implicit object ApproximationWriter extends HtmlWriter5[Int] {
    def write(in: Int): String =
      s"It's definitely less than ${((in / 10) + 1) * 10}"
  }
}

object Person5 {

  //another way to define a typeclass instance as a val vs an object
  //because we have access to the src for Person we define the instance in the class companion
  implicit val personWriter = new HtmlWriter5[Person5] {
    override def write(in: Person5): String = s"${in.name} (${in.email})"
  }
}

case class Person5(name: String, email: String)
trait HtmlWriter5[A] {
  def write(in: A): String
}

object TypeclassUsage {

  /*
  Many times you will need to push an implicit parameter down into a function
  because something needs to use it.  Here is one way to do it.
   */
  def parameterListAndApply[A](x: A)(implicit writer: HtmlWriter5[A]): Unit = {
    val result = HtmlWriter5[A].write(x) //uses the apply in HtmlWriter5 companion object
  }

  /*
  Another way to push an implicit parameter down into a function would be to use a
  Context Bound type parameter.
  A: HtmlWriter5 expands into the equivalent implicit parameter list (implicit writer: HtmlWriter5
   */
  def contextBoundAndApply[A: HtmlWriter5](x: A): Unit = {
    //context bound A:HtmlWriter5 expands into the equivalent implicit parameter list
    val result = HtmlWriter5[A].write(x) //uses the apply in HtmlWriter5 companion object
  }

  import Person5._
  import HtmlWriter5._
  //both parameter list and context bound work the same
  contextBoundAndApply(Person5("john", "john@example.com"))
  contextBoundAndApply(2)

  parameterListAndApply(Person5("john", "john@example.com"))
  parameterListAndApply(2)
}
