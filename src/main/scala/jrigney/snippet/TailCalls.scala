package jrigney.snippet

import scala.annotation.tailrec
import scala.util.control.TailCalls._

object TailCalls extends App {

  def factorial(n: Int): Int = n match {
    case 0 => 1
    case _ => n * factorial(n - 1)
  }

  @tailrec
  def factorial(n: Int, accumulator: Long = 1): Long = {
    if (n == 0) accumulator else factorial(n - 1, (accumulator * n))
  }


  //Using scala TailCalls and TailRec
  def factorialT(n: Int): TailRec[Int] = {
    if (n == 1) done(1) else tailcall(factorialT(n - 1))
  }

  //Using scala TailCalls and TailRec
  def fib(n: Int): TailRec[Int] =
    if (n < 2) done(n) else for {
      x <- tailcall(fib(n - 1))
      y <- tailcall(fib(n - 2))
    } yield (x + y)

  def permsCool(s: List[Int]): List[List[Int]] = {
    if (s.size == 1) List(s)
    else s.flatMap { c =>
      val permOtherDigits = permsCool(s.filterNot(_ == c))
      permOtherDigits.map(c +: _)
    }
  }

//  println(s"premsCool ${permsCool(List(1, 2, 3, 4))}")

  def permsT(s: List[Int]): TailRec[List[List[Int]]] = {
    ???
  }

  println(s"permsT ${permsT(List(1, 2, 3, 4)).result}")

}


