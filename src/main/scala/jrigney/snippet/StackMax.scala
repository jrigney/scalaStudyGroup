package jrigney.snippet

object StackMax extends App {

  //give max in vector
  val input = Vector(1, 2, 3, 4, 5)
  val expectedMax = 5
  val expectedStack = List(5, 4, 3, 2, 1)

  val (max, stack) = input.foldLeft((Option.empty[Int], List.empty[Int])) { (acc, next) =>
    acc match {
      case (None, stack) => (Some(next), next :: stack)
      case (Some(currentMax), stack) if (currentMax < next) => (Some(next), next :: stack)
      case (max, stack) => (max, next :: stack)
    }
  }

  assert(max == Some(expectedMax))
  assert(stack.sameElements(expectedStack))
}
