package jrigney.snippet

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

object MapR extends App {
  val words = List("Red Blue Red Blue Green",
    "White Black",
    "Red White Black",
    "Orange Green",
    "Red Blue Red",
    "Blue Green Blue Red",
    "Green White BlackRed Blue Red",
    "Blue Green Blue Red",
    "Green White Black Red Blue Red",
    "Blue Green Blue Red",
    "Green White Black")


  type Key = String
  type Value = Int
  type Word = String

  def mapReduce(lines: List[String]) = {

    def words(s: String): List[Word] = s.split(" ").filterNot(_ == "").toList

    def reduce(shuffled: List[(Key, Value)]): List[(Key, Value)] = {
      val reduced = shuffled.foldLeft(Map.empty[Key, Value]) { case (acc, (k, v)) =>
        acc.get(k) match {
          case None => acc.updated(k, v)
          case Some(x) => acc.updated(k, v + x)
        }
      }
      reduced.toList
    }

    val (l, r) = lines.splitAt(lines.length / 2)

    def mapWords(lines: List[String]): List[(Key, Value)] = {
      lines.flatMap(words).groupBy(identity).mapValues(_.size).toList
    }

    val leftFuture = Future {
      mapWords(l)
    }
    val rightFuture = Future {
      mapWords(r)
    }

    for {
      left <- leftFuture
      right <- rightFuture
    } yield {
      reduce(left ++ right)
    }

  }

  println(s"mapReduce ${Await.result(mapReduce(words), 500 milliseconds)}")


}
