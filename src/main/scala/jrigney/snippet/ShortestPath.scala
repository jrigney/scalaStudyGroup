package jrigney.snippet

object ShortestPath {

  def shortestPath[V, Cost: Numeric](start: V, end: V, graph: Map[V, List[(V, Cost)]]): Path[V, Cost] = {
    //general idea - in a loop grab the vertex(minVertex) that provides the minimum cost path to the start until you reach the destination
    @scala.annotation.tailrec
    def shortestPathLoop[Vtx, Cst: Numeric](
      discoveredAscendingPaths: List[Path[Vtx, Cst]], // all paths we currently have a temp distance for sorted by cost ascending
      graph: Map[Vtx, List[(Vtx, Cst)]],
      dest: Vtx,
      processed: Set[Vtx]): Path[Vtx, Cst] =
      discoveredAscendingPaths match {
        case (Path(costFromStart, breadCrumbs)) :: remainingPaths => //choose the vertex that minimimizes the cost from start to chosen Vertex
          val currentEndOfPath = breadCrumbs.head
          if (currentEndOfPath == dest) Path(costFromStart, breadCrumbs.reverse) // if the chosen vertex == dest then return the path for the chosen vertex
          else {
            val pathsToAdjacentVertexToStart: List[Path[Vtx, Cst]] = {
              val verticesAdjacentToEndOfPath = graph(currentEndOfPath) //else get the adjacent vertices
              verticesAdjacentToEndOfPath.flatMap {
                //loop through the adjacent vertices
                case (vertexAdjacentToEnd, costEndOfPathVertexToAdjacentVertex) =>
                  //if the adjacentVertex has not been visited, update the path with the new cost and add the adjacent vertex to the path.
                  //this is the path and cost of the adjacent vertex to the start
                  if (!processed.contains(vertexAdjacentToEnd))
                    List(Path(implicitly[Numeric[Cst]] plus (costFromStart, costEndOfPathVertexToAdjacentVertex), vertexAdjacentToEnd :: breadCrumbs))
                  else List.empty[Path[Vtx, Cst]]
              }
            }
            val ascendingPaths = (pathsToAdjacentVertexToStart ++ remainingPaths).sortWith {
              //sort the paths in ascending cost
              case (Path(distanceToStart1, _), Path(distanceToStart2, _)) => implicitly[Numeric[Cst]] lt (distanceToStart1, distanceToStart2)
            }
            shortestPathLoop(ascendingPaths, graph, dest, processed + currentEndOfPath)
          }
        case Nil => Path(implicitly[Numeric[Cst]] zero, List.empty[Vtx])
      }

    shortestPathLoop(List(Path(implicitly[Numeric[Cost]] zero, List(start))), graph, end, Set(start))
  }

  case class Path[V, W](costToStart: W, breadCrumbs: List[V])

}
