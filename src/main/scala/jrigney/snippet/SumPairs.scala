package jrigney.snippet

import scala.collection.immutable.HashSet

/*Given a sorted array a[] find the pairs of number that add up to x.
  brute force
  filter out all numbers > x
  for each number see if the numbers > sum to x

  optimized
  put item in hashmap
  if (number is list - x) in the hashmap then sum
*/
object SumPairs extends App {

  def sumsBrute(sortedList: List[Int], sumWanted: Int): List[(Int, Int)] = {

    def loop(remainingList: List[Int], sums: List[(Int, Int)]): List[(Int, Int)] = {
      if (remainingList.isEmpty) sums
      else {
        val (x :: xs) = remainingList
        val theseSums = xs.flatMap { sumWith =>
          if (x + sumWith == sumWanted) Some((x, sumWith)) else None
        }
        loop(xs, sums ++ theseSums)
      }
    }

    loop(sortedList, List.empty[(Int, Int)])
  }

  def sumsOpimized(sortedList: List[Int], sumWanted: Int): List[(Int, Int)] = {
    val set = HashSet.empty[Int] ++ sortedList
    sortedList.flatMap { elem =>
      val sumWith = sumWanted - elem
      if (set.contains(sumWith)) Some((elem, sumWith)) else None
    }
  }

  val list = List(1, 2, 4, 5, 8, 15, 18, 22).sorted

  println(sumsBrute(list, 20)) //List((2,18), (5,15))
  println(sumsOpimized(list, 20)) //List((2,18), (5,15))
}
