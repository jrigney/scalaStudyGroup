package jrigney.snippet

object Poker extends App {

  /*
  * Given a set of 5 playing card identifiers such as 2H, 7C, QS, 10D, 2D;
  * determine if this hand is better than some other hand, according to the rules of poker.
  *
  * Hands will be a string with 5 cards comma separated,
  * each card will have 1-2 digits or JQKA and a suit indicator C,D,S,H (i.e. 10C, KH)
  *
  * Possible Hand Types Below:
  *   Straight flush - same suit in sequence
  *   Four of a kind - 4 cards same rank
  *   Full house - 3 of a kind with a pair
  *   Flush - all same suit
  *   Straight - 5 in sequence
  *   Three of a kind - 3 same rank
  *   Two pair - 2 different pairs
  *   One pair - 2 cards same rank
  *
  * The goal of this is to compare between the hand types.
  * Comparing 2 of the same type (i.e. 2 straights) to determine a winner is outside the scope
  * and will not be tested.
  *
  * Implement hand1WinsOverHand2 method and return whether or not the first hand wins over the second hand.
  */

  //  class SeqExtractor[A, B](f: A => B) {
  //    def unapplySeq(s: Seq[A]): Option[Seq[A]] =
  //      if (s map f sliding 2 forall { case Seq(a, b) => a == b }) Some(s)
  //      else None
  //  }
  //
  //  val StraightExtract = new SeqExtractor((_: Card).rank)

  def hand1WinsOverHand2(hand1Str: String, hand2Str: String): Boolean = {
    //Hand(hand1Str) > Hand(hand2Str)
    ???
  }

  implicit class CompareTwoPokerHands(hand1: String) {
    def winsOver(hand2: String): Unit = {
      val result = if (hand1WinsOverHand2(hand1, hand2)) "Correct" else "Incorrect"
      println(s"$result, hand [$hand1] wins over [$hand2]")
    }
  }

  println("Poker Hand comparison")
  "8C,9C,10C,JC,QC" winsOver "6S,7H,8D,9H,10D" // straight flush
  "4H,4D,4C,4S,JS" winsOver "6C,6S,KH,AS,AD" // four of a kind
  "5C,3C,10C,KC,7C" winsOver "6C,6D,6H,9C,KD" // flush
  "4H,4D,4C,KC,KD" winsOver "9D,6S,KH,AS,AD" // full house
  "2C,3C,4S,5S,6S" winsOver "6C,6D,6H,9C,KD" // straight
  "7C,7D,7S,3H,4D" winsOver "9S,6S,10D,AS,AD" // three of a kind
  "8C,8H,10S,KH,KS" winsOver "2S,2D,JH,7S,AC" // two pair
  "AC,AH,3C,QH,10C" winsOver "3S,2D,KH,JS,AD" // one pair
  System.exit(0)
}

object Hand {
  def apply(asString: String): Hand = {
    val cards = asString.split(',').flatMap {
      case Card(b) => Some(b)
      case _ => None
    }.toList

    implicit val memo = new Memo
    cards match {
      case Straight(h) => h
      case FourKind(h) => h
      case ThreeKind(h) => h
    }
  }

  def unapply(asString: String): Option[Hand] = {

    val cards = asString.split(',').flatMap {
      case Card(b) => Some(b)
      case _ => None
    }
    ???
  }

  def empty: Hand = Garbage()

  def sequence_?(cards: List[Card]): Boolean = {
    @scala.annotation.tailrec
    def loop(ranks: List[Int]): Boolean = ranks match {
      case head :: Nil => true
      case head :: tail if (head + 1) == tail.head => loop(tail)
      case _ => false
    }

    loop(cards.map(_.rank).sorted)
  }

  def kinds(cards: List[Card]): Hand = {
    val grouped = cards.groupBy(c => c.rank)
    grouped.foldLeft(Hand.empty) { (acc, next) =>
      (next, acc) match {
        case ((rank, cards), _) if cards.size == 4 => FourKind(rank)
        case ((rank, cards), h: OnePair) if cards.size == 3 => FullHouse(rank, h.rank)
        case ((rank, cards), _) if cards.size == 3 => ThreeKind(rank)
        case ((rank, cards), h: ThreeKind) if cards.size == 2 => FullHouse(h.rank, rank)
        case ((rank, cards), h: OnePair) if cards.size == 2 => if (rank > h.rank) TwoPair(rank) else TwoPair(h.rank)
        case ((rank, cards), _) if cards.size == 2 => OnePair(rank)
        case _ => acc
      }
    }
  }
}

trait Hand {
  def value: Int
}

object Straight {
  def unapply(cards: List[Card]): Option[Straight] = {
    if (Hand.sequence_?(cards)) Some(Straight()) else None
  }
}

case class StraightFlush() extends Hand {
  val value = 8
}

object FourKind {
  def unapply(cards: List[Card])(implicit memo: Memo): Option[FourKind] = {
    memo(Hand.kinds(cards)) match {
      case x: FourKind => Some(x)
      case _ => None
    }
  }
}
case class FourKind(highestRank: Int) extends Hand {
  val value = 7
}

case class FullHouse(threes: Int, twos: Int) extends Hand {
  val value = 6
}

case class Flush() extends Hand {
  val value = 5
}

case class Straight() extends Hand {
  val value = 4
}

object ThreeKind {
  def unapply(cards: List[Card])(implicit memo: Memo): Option[ThreeKind] = {
    memo(Hand.kinds(cards)) match {
      case x: ThreeKind => Some(x)
      case _ => None
    }
  }
}

case class ThreeKind(rank: Int) extends Hand {
  val value = 3
}

case class TwoPair(rank: Int) extends Hand {
  val value = 2
}

case class OnePair(rank: Int) extends Hand {
  val value = 1
}

case class Garbage() extends Hand {
  val value = 0
}

class Slot() {

  import java.util.concurrent.ArrayBlockingQueue

  val slot = new ArrayBlockingQueue[Hand](1)

  def getElseSet(hand: Hand): Hand = {
    if (slot.size == 0) {
      slot.add(hand)
      hand
    } else slot.peek()
  }
}

class Memo() {
  val slot = new Slot()

  def apply[A](fn: => Hand): Hand = {
    slot.getElseSet(fn)
  }
}

trait Card {
  def rank: Int
}

case class Diamond(rank: Int) extends Card

case class Heart(rank: Int) extends Card

case class Club(rank: Int) extends Card

case class Spade(rank: Int) extends Card

object Card {
  val CardPattern = """([JQKA\d]{1,2}+)([CDHS]{1})""".r

  def unapply(asString: String): Option[Card] = {
    asString match {
      case CardPattern(rank, suit) =>
        val rankNumeric = rank match {
          case "J" => 11
          case "Q" => 12
          case "K" => 13
          case "A" => 14
          case x => x.toInt
        }
        suit match {
          case "C" => Some(Club(rankNumeric))
          case "D" => Some(Diamond(rankNumeric))
          case "H" => Some(Heart(rankNumeric))
          case "S" => Some(Spade(rankNumeric))
        }
    }
  }
}

