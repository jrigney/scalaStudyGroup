package jrigney.snippet

/*
  not using equalIgnoreCase to make the point
  http://degoes.net/articles/destroy-all-ifs
 */
object AntiIf {

  //typical boolean implementation
  def booleanMatch(s: String, ignoreCase: Boolean, target: String): Boolean = {
    if (ignoreCase) s.toLowerCase == target.toLowerCase
    else s == target
  }

  //replace boolean with sum type
  sealed trait Case
  final case object CaseInsensitive extends Case
  final case object CaseSensitive extends Case
  def sumMatch(s: String, alphaCase: Case, target: String): Boolean = {
    val (pattern, targetPrime) = alphaCase match {
      case CaseInsensitive => (s.toLowerCase, target.toLowerCase)
      case CaseSensitive => (s, target)
    }
    pattern == targetPrime
  }

  //replace conditional with lambda
  type LambdaCase = String => String
  def lambdaMatch(s: String, lc: LambdaCase, target: String): Boolean = {
    val (pattern, targetPrime) = (lc(s), lc(target))
    pattern == targetPrime
  }

  //match combinator
  type Matcher = (String, String) => Boolean
  def caseInsensitive: Matcher => Matcher = {
    (m: Matcher) =>
      {
        (s1: String, s2: String) =>
          m(s1.toLowerCase, s2.toLowerCase)
      }
  }
  def myMatch: Matcher = {
    (s: String, t: String) =>
      s == t
  }
  val x: Boolean = myMatch("bob", "BoB")
  val y: Boolean = (caseInsensitive(myMatch))("bob", "Bob")

}
