package jrigney.snippet

trait HtmlWriter3[A] {
  def write(in: A): String
}
object ConcreteTypeClassInterface2 {

  //concrete class for each different situation
  object PersonWriter extends HtmlWriter3[Person] {
    override def write(person: Person) = s"${person.name} (${person.email})"
  }

  object ObfuscatedPersonWriter extends HtmlWriter3[Person] {
    override def write(person: Person) = s"${person.name} (${person.email.replaceAll("@", " at ")})"
  }

}

object ConcreteTypeclass {

  import ConcreteTypeClassInterface2._
  PersonWriter.write(Person("John", "john@example.com")) //concrete class method
}
