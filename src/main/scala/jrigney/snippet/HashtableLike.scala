

trait HashtableLike[K, V] {
  def add(key: K, value: V): HashtableLike[K, V]
  def get(key: K): V
}

object HashtableLike {

  object HashUtil {
    def add[K, V](key: K, value: V)(implicit hashtable: HashtableLike[K, V]): HashtableLike[K, V] = {
      hashtable.add(key, value)
    }
    def get[K, V](key: K)(implicit hashtable: HashtableLike[K, V]): V = {
      hashtable.get(key)
    }
  }
}

