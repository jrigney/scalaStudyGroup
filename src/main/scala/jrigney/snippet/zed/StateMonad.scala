package jrigney.snippet.zed

import scalaz._
import Scalaz._

object StateMonad extends App {
  val s = State[Int, String](i => (i + 1, "str"))

  println(s.eval(1))
  println(s.exec(1))
  println(s.run(1))

}
