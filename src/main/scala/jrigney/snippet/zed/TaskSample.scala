package jrigney.snippet.zed

import scalaz.concurrent.Task

object TaskSample extends App {
  val x = Task { println("foo") }

  x.unsafePerformSync
}
