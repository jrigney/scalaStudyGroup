package jrigney.snippet

/**
 * given a string s return all the substrings that are palindromes
 *
 * examples
 * val in = "xxyx"
 * val expected = Set("x", "y", "xx", "xyx")
 *
 * determine all the substrings
 * filter substrings for palindromes
 */
object PalindromeSubstrings extends App {
  def palindromSubstrings(s: String): Set[String] = {
    val subs = subStrings(s)
    subs.filter(palindrome_?)
  }

  def palindrome_?(s: String): Boolean = {
    val len = s.length

    def loop(remaining: String, ispalindrome: Boolean): Boolean = {
      remaining.toList match {
        case x if (x.isEmpty || ispalindrome == false) => ispalindrome
        case h :: Nil => true
        case _ =>
          val head = remaining.head
          val last = remaining.last

          if (head == last) loop(remaining.tail.init, ispalindrome) else loop(remaining.tail.init, false)
      }
    }
    loop(s, true)
  }

  def subStrings(s: String): Set[String] = {
    (1 to s.size).foldLeft(Set.empty[String]) { (acc, size) =>
      s.sliding(size).toSet ++: acc
    }
  }
}
