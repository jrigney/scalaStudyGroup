package jrigney.snippet

case class Person(name: String, email: String)
trait Equal[A] {
  def equal(v1: A, v2: A): Boolean
}
object Equal {
  def apply[A](implicit instance: Equal[A]) = instance
}

object EmailImplicit { //package implicit in own object so you can control the implicit selection by changing which object to import
  implicit object EmailEqual extends Equal[Person] {
    def equal(v1: Person, v2: Person): Boolean =
      v1.email == v2.email
  }
}
object NameAndEmailImplicit { //package implicit in own object so you can control the implicit selection by changing which object to import
  implicit object NameEmailEqual extends Equal[Person] {
    def equal(v1: Person, v2: Person): Boolean =
      v1.email == v2.email && v1.name == v2.name
  }
}

object Eq {
  def apply[A](x: A, y: A)(implicit equal: Equal[A]): Boolean = {
    equal.equal(x, y)
  }
}
//you need to have the above objects defined before they are imported below othewise the implicits will no be found

object TypeClass extends App {
  def byNameAndEmail = {
    import NameAndEmailImplicit._
    Eq(Person("Noel", "noel@example.com"), Person("Noel", "noel@example.com"))
  }
  def byEmail = {
    import EmailImplicit._
    Eq(Person("Noel", "noel@example.com"), Person("Dave", "noel@example.com"))
  }
  def convenientInterface = { // uses the object Equal.apply
    import NameAndEmailImplicit._
    Equal[Person].equal(Person("Noel", "noel@example.com"), Person("Noel", "noel@example.com"))
  }
}
