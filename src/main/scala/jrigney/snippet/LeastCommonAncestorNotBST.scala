package jrigney.snippet

/*
Given a binary tree (not a binary search tree) and two values say n1 and n2,
write a program to find the least common ancestor.



 */
object LeastCommonAncestorNotBST {
  def findLCA(tree: Tree, x: Int, y: Int): Option[Tree] = {
    if (tree.value == x || tree.value == y) Some(tree)
    else (if (tree.left != None) findLCA(tree.left.get, x, y) else None, if (tree.right != None) findLCA(tree.right.get, x, y) else None) match {
      case (Some(r), Some(l)) => Some(tree)
      case (Some(r), None) => Some(r)
      case (None, Some(l)) => Some(l)
      case (None, None) => None
    }
  }
}

case class Tree(value: Int, left: Option[Tree], right: Option[Tree])

object LeastCommonAncestorNotBSTApp extends App {
  val tree = Tree(
    1,
    Some(Tree(
      2,
      Some(Tree(4, None, None)),
      Some(Tree(5, None, None)))),
    Some(Tree(
      3,
      Some(Tree(6, None, None)),
      Some(Tree(7, None, None)))))

  println(LeastCommonAncestorNotBST.findLCA(tree, 4, 5))
}
