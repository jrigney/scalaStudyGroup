

class Hashtable[K, V](size: Int, hashFn: (K) => Int) {
  val hashtable = Array.fill(size)(Vector.empty[(Int, V)])

  def hash(key: K) = hashFn(key)
  def index(key: K) = hash(key) % size

  def add(key: K, value: V) = {
    val vec = hashtable(index(key))
    hashtable(index(key)) = (hash(key), value) +: vec
  }

  def dump(): Array[Vector[(Int, V)]] = {
    hashtable
  }

  def get(key: K): Option[V] = {
    val vec = hashtable(index(key))
    vec.find { case (h, v) => h == hash(key) }.map { case (_, v) => v }
  }
}

object HashtableApp extends App {
  val hashtable = new Hashtable[String, Int](5, (key: String) => key.hashCode)
  hashtable.add("bob", 100)

  hashtable.dump.foreach(println)

  println(s"${hashtable.get("bob")}")
}
