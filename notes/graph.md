# Graph
A graph is a set of vertices and a set of edges connecting those vertices.



## Representation
There are three ways to represent a graph

## Edge list
An array of edges where an edge is a tuple of the verticies that make
up the edge.  If a weight is needed for the edge then a three tuple
could be used.

```
[ (0,1), (0,2), (1,2), (2,0), (2,3), (3,3) ]
```

## Adjacency Matrix
For |V| verticies the matrix would be |V| x |V| of 0 or 1 where 1
indicates there is an edge and 0 indicates there is no edge.  If a
weight is needed the the vaule should be the weight and a special
value should be used instead of 0 to indicate there is no edge.

Advantage is a we can find out if an edge exists in constant time by
looking up the indicies of the two verticies.

```
X 0 1 2 3
0 0 1 0 0
1 0 0 1 0
2 1 0 0 1
3 0 0 0 1
```

## Adjacency List

For each vertex i store an array of adjacent verticies

```
0 -> 1, 2
1 -> 2
2 -> 0, 3
3 -> 3
```

## Breadth first search
Uses a queue to keep track of what verticies to do next.

## Depth first search
Uses a stack to keep track of what verticies to do next.